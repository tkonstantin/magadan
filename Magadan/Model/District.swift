//
//  District.swift
//  Magadan
//
//  Created by Константин on 13.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation

class District: SimplePickerObject {
    var degrees = "?"
    var iconName = "clear"
    
     init?(_ key: String, _ _degrees: String="?", _ _iconName: String="clear") {
        super.init(with: [:])
        guard let _name = key.nameFromKey() else { return nil }
        title = _name
        degrees = _degrees
        iconName = _iconName
    }
    
   override init(with json: NSDictionary) {
        super.init(with: json)
    }
    
    
}

extension String {
    fileprivate func nameFromKey() -> String? {
        switch self {
        case "5a1820a50dc43a0a0f7ac5b2": return "Магадан"
        case "5a1820a60dc43a0a0f7ac5b6": return "Ольский"
        case "5a1820a60dc43a0a0f7ac5ba": return "Омсукчанский"
        case "5a1820a70dc43a0a0f7ac5bf": return "Северо-Эвенский"
        case "5a1820a70dc43a0a0f7ac5c2": return "Среднеканский"
        case "5a1820a80dc43a0a0f7ac5c4": return "Сусуманский"
        case "5a1820a80dc43a0a0f7ac5c6": return "Тенькинский"
        case "5a1820a80dc43a0a0f7ac5ca": return "Хасынский"
        case "5a1820a90dc43a0a0f7ac5e4": return "Ягоднинский"
        default: return nil
        }
    }
}



