//
//  SubMenuItem.swift
//  Magadan
//
//  Created by Константин on 26.01.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation

class SubMenuItem {
    var id = ""
    var title = ""
    var body = ""
    var attributedBody: NSAttributedString!
    var cover = ""
    var photos = [String]()
    var date: Date?
    var endDate: Date?
    var videos: [NewsVideoItem] = []

    
    var allImages: [String] {
        get {
            return ([cover] + photos).filter({!$0.isEmpty})
        }
    }
    
    
    var isSingle = false
    
    
    init(with dict: NSDictionary) {
        id = dict["id"] as? String ?? String(dict["id"] as? Int ?? 0)
        title = dict["title"] as? String ?? ""
        body = dict["body"] as? String ?? ""
        attributedBody = body.htmlToAttributedString()

        cover = Server.url_paths.imagesPathPreffix + (dict["cover"] as? String ?? "")
        for photosDict in (dict["photos"] as? [NSDictionary] ?? []) {
            if let img = photosDict["image"] as? String {
                photos.append(Server.url_paths.imagesPathPreffix + img)
            }
        }
        if let dateValue = dict["date"] as? String  {
            self.date = Date.init(dateFromVeryStrangeString: dateValue)
        }
        
        if let endDateValue = dict["date_end"] as? String {
            self.endDate = Date.init(dateFromVeryStrangeString: endDateValue)
        }
        
        for video in dict["videos"] as? [NSDictionary] ?? [] {
            self.videos.append(NewsVideoItem(with: video))
        }
        
    }
}


