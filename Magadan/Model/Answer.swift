//
//  Answer.swift
//  Magadan
//
//  Created by Константин on 07.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation


class Answer {
    
    enum answerKeys {
        static let order = "ANSWER_ORDER"
        static let title = "TITLE"
        static let type = "QUESTION_VOTE_TYPE"
        static let id = "VOTE_ANSWER_ID"
        static let question_id = "VOTE_QUESTION_ID"
        static let previousVote = "USER_VOTE"
        static let votesCount = "ANSWER_COUNT"
        static let votesPercentage = "ANSWER_PERCENTAGE"
    }
    var id = ""
    var question_id = ""
    var order = ""
    var title = ""
    var type = ""
    var indexPath: IndexPath!
    var previousVote: Bool = false
    var votesCount = 0
    var votesPercentage: Double = 0
    
    
    init(with dict: NSDictionary) {
        id = dict[answerKeys.id] as? String ?? String(dict[answerKeys.id] as? Int ?? 0)
        question_id = dict[answerKeys.question_id] as? String ?? String(dict[answerKeys.question_id] as? Int ?? 0)
        order = dict[answerKeys.order] as? String ?? String(dict[answerKeys.order] as? Int ?? 0)
        title = dict[answerKeys.title] as? String ?? ""
        type = dict[answerKeys.type] as? String ?? ""
        previousVote = dict[answerKeys.previousVote] as? Bool ?? false
        votesCount = dict[answerKeys.votesCount] as? Int ?? Int(dict[answerKeys.votesCount] as? String ?? "0") ?? 0
        votesPercentage = dict[answerKeys.votesPercentage] as? Double ?? 0

    }
}

