//
//  News.swift
//  Magadan
//
//  Created by Константин on 19.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

let loadingText = "Загрузка..."

class NewsVideoItem {
    
    var title: String?
    var cover: String?
    var link: String?
    
    init(with dict: NSDictionary) {
        title = dict["TITLE"] as? String
        cover = dict["COVER"] as? String
        link = dict["VIDEO"] as? String
        
        if title != nil {
            title = Server.url_paths.imagesPathPreffix + title!
        }
        if cover != nil {
            cover = Server.url_paths.imagesPathPreffix + cover!
        }
        if link != nil {
            link = Server.url_paths.imagesPathPreffix + link!
        }
    }
}

class News {

    
    enum keys {
        static let announce = "announce"
        static let date = "date"
        static let isBanner = "isBanner"
        static let title = "title"
        static let imageDict = "image"
        static let image_large = "large"
        static let image_small = "small"
        static let id = "id"
        static let text = "text"
        static let photos = "photos"
        static let likesCount = "likes"
        static let commentsCount = "comments"
        static let isLiked = "isLiked"
        static let ogv = "ogv"
        static let share_link = "share_link"
        static let videos = "videos"

    }
    
    var announce = ""
    var date: Date?
    var image_large: String?
    var image_small: String?
    var isBanner = false
    var title = ""
    var id: Int = 0
    var text: String = ""
    var photos: [String] = []
    var likesCount = 0
    var commentsCount = 0
    var isLiked = false
    var ogv = ""
    var share_link: String?
    var videos: [NewsVideoItem] = []
    
    
    var allImages: [String] {
        get {
            return [largestImage].filter({$0 != Server.url_paths.imagesPathPreffix}) + photos
        }
    }
    
    
    var idString: String {
        get {
            return String(id)
        }
    }
    
    var attributedAnnounce: NSAttributedString?
    var attributedText: NSAttributedString?
    var originalHTML: String?
    
    var currentDisplayingTextAttributed: NSAttributedString {
        get {
            if attributedText!.string.isEmpty {
                return NSAttributedString(string: loadingText)
            } else {
                return attributedText!
            }
        }
    }
    
    var currentDisplayingText: String {
        get {
            if text.isEmpty {
                return loadingText
            } else {
                return text
            }
        }
    }
    
    var largestImage: String {
        get {
            return Server.url_paths.imagesPathPreffix + (image_large ?? image_small ?? "")
        }
    }
    
    var smallestImage: String {
        get {
            return Server.url_paths.imagesPathPreffix + (image_small ?? image_large ?? "")
        }
    }
    
    
    init(with json: NSDictionary) {

        self.id = json[keys.id] as? Int ?? Int(json[keys.id] as? String ?? "0") ?? 0
        
        self.announce = (json[keys.announce] as? String ?? "").htmlToString()
        self.text = (json[keys.text] as? String ?? "").htmlToString()
        
        self.attributedAnnounce = (json[keys.announce] as? String ?? "").htmlToAttributedString()
        self.attributedText = (json[keys.text] as? String ?? "").htmlToAttributedString()
        self.originalHTML = json[keys.text] as? String
        self.likesCount = json[keys.likesCount] as? Int ?? Int(json[keys.likesCount] as? String ?? "0") ?? 0
        self.commentsCount = json[keys.commentsCount] as? Int ?? Int(json[keys.commentsCount] as? String ?? "0") ?? 0
        self.isLiked = json[keys.isLiked] as? Bool ?? false

        self.title = (json[keys.title] as? String ?? "").htmlToString()
        self.ogv = json[keys.ogv] as? String ?? ""
        self.date = Date.init(dateFromVeryStrangeString: json[keys.date] as? String ?? "00000000000000")
        self.share_link = json[keys.share_link] as? String
        if let images = json[keys.imageDict] as? NSDictionary {
            self.image_small = images[keys.image_small] as? String
            self.image_large = images[keys.image_large] as? String
        }
        
        for photo in json[keys.photos] as? [NSDictionary] ?? [] {
            self.photos.append(Server.url_paths.imagesPathPreffix + (photo["image"] as? String ?? ""))
        }
        
        for video in json[keys.videos] as? [NSDictionary] ?? [] {
            self.videos.append(NewsVideoItem(with: video))
        }

    }
}
