//
//  Event.swift
//  Magadan
//
//  Created by Константин on 19.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import  UIKit

class Event {
    
    enum keys {
        static let text = "body"
        static let announce = "announce"
        static let endTime = "endTime"
        static let id = "id"
        static let startTime = "startTime"
        static let title = "title"
        static let image = "image"
        static let photos = "photos"
        static let share_link = "share_link"
        static let commentsCount = "comments"
    }
    
    enum liveKeys {
        static let id = "ONLINE_TRANSLATION_ID"
        static let text = "DESCRIPTION"
        static let title = "TITLE"
        static let startTime = "DATE_START"
        static let endTime = "DATE_END"
        static let source = "SOURCE_STREAM_ios"
        static let share_link = "share_link"
    }
    
    var endTime: Date?
    var id = 0
    var startTime: Date?
    var title = ""
    var image: String = ""
    var photos: [String] = []
    var share_link: String?
    var commentsCount = 0
    
    var maxDate: Date {
        get {
            return max(startTime ?? Date(), endTime ?? Date())
        }
    }
    
    var isActual: Bool {
        get {
            return maxDate >= Date()
        }
    }
    
    var allImages: [String] {
        get {
            return [image].filter({$0 != Server.url_paths.imagesPathPreffix}) + photos
        }
    }
    
    var rawAnnounce = ""
    
    lazy var announce: String = {
        return rawAnnounce.htmlToString()
    }()
    
    lazy var text: String = {
        return rawText.htmlToString()
    }()

    var source = ""
    var sourceURL: URL? {
        get  {
            return URL(string: source) ?? URL(string: source.trimmingCharacters(in: .whitespacesAndNewlines))
        }
    }
    
    var isLive: Bool {
        get {
            return sourceURL != nil
        }
    }
    
    var idString: String {
        get {
            return String(id)
        }
    }
    
    func isInRange(month: Int, year: Int) -> Bool {
        guard let start = startTime, let end = endTime else { return false }
        
        let startYear = start.dateToComponents().year
        let endYear = end.dateToComponents().year
        
        if (startYear == year || endYear == year) && month == 13 {
            return true
        }
        
        let conformsToLowerbound = startYear < year || (startYear == year && start.dateToComponents().month <= month)
        let conformsToUpperbound = endYear > year || (endYear == year && end.dateToComponents().month >= month)
        
        return conformsToLowerbound && conformsToUpperbound
    }
    
    
    
    var attributedAnnounce: NSAttributedString?
    
    var rawText: String = ""
    lazy var attributedText: NSAttributedString? = {
        return rawText.htmlToAttributedString()
    }()
    

    
    var currentDisplayingTextAttributed: NSAttributedString {
        get {
            if attributedText!.string.isEmpty {
                return NSAttributedString(string: loadingText)
            } else {
                return attributedText!
            }
        }
    }
    
    var currentDisplayingText: String {
        get {
            if text.isEmpty {
                return loadingText
            } else {
                return text
            }
        }
    }
    
    
    
    init(with json: NSDictionary) {
                
        self.id = json[keys.id] as? Int ?? Int(json[keys.id] as? String ?? "0") ?? 0
        
        self.rawAnnounce = json[keys.announce] as? String ?? ""
        
        self.attributedAnnounce = self.rawAnnounce.htmlToAttributedString()
        self.rawText = json[keys.text] as? String ?? ""
        
        self.commentsCount = json[keys.commentsCount] as? Int ?? Int(json[keys.commentsCount] as? String ?? "0") ?? 0
        self.title = json[keys.title] as? String ?? ""
        self.image = Server.url_paths.imagesPathPreffix + (json[keys.image] as? String ?? "")
        self.startTime = Date.init(dateFromVeryStrangeString: json[keys.startTime] as? String ?? "00000000000000")
        self.endTime = Date.init(dateFromVeryStrangeString: json[keys.endTime] as? String ?? "00000000000000")
        self.share_link = json[keys.share_link] as? String
        
        for photo in json[keys.photos] as? [NSDictionary] ?? [] {
            self.photos.append(Server.url_paths.imagesPathPreffix + (photo["image"] as? String ?? ""))
        }
        
        
    }
    
    init(withLive json: NSDictionary) {
        
        self.id = json[liveKeys.id] as? Int ?? Int(json[liveKeys.id] as? String ?? "0") ?? 0
        
        self.rawText = json[liveKeys.text] as? String ?? ""
        
        self.title = json[liveKeys.title] as? String ?? ""
        self.startTime = Date.init(dateFromVeryStrangeString: json[liveKeys.startTime] as? String ?? "00000000000000")
        self.endTime = Date.init(dateFromVeryStrangeString: json[liveKeys.endTime] as? String ?? "00000000000000")
        self.share_link = json[keys.share_link] as? String
        self.commentsCount = json[keys.commentsCount] as? Int ?? Int(json[keys.commentsCount] as? String ?? "0") ?? 0
        
        if let rawSource = json[liveKeys.source] as? String {
            self.source = rawSource
        }
        
        
        
    }
}
