//
//  User.swift
//  Magadan
//
//  Created by Константин on 13.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation


extension Optional where Wrapped: LosslessStringConvertible {
    
    var string: String {
        get {
            if self == nil {
                return  ""
            } else {
                return String(self!)
            }
        }
    }
}


class User: NSObject, NSCoding {
    
    enum keys {
        static let ESIA_USERS_ID = "ESIA_USERS_ID"
        static let ESIA_USERS_SESSIONS_ID = "ESIA_USERS_SESSIONS_ID"
        static let address = "address"
        static let createdAt = "createdAt"
        static let district = "district"
        static let custom_district = "custom_district"
        static let email = "email"
        static let expire_at = "expire_at"
        static let firstName = "firstName"
        static let isMobile = "isMobile"
        static let is_mobile = "is_mobile"
        static let lastAuthorized = "lastAuthorized"
        static let lastName = "lastName"
        static let middleName = "middleName"
        static let votes = "votes"
        
        enum parsedAddress {
            static let main = "parsedAddress"
            static let city = "city"
            static let cityOwner = "cityOwner"
            static let region = "region"
            static let street = "street"
            static let building = "building"
            static let block = "building_block"
            static let flat = "flat"
            static let index = "postal_index"
        }
        enum questionsAndIdes {
            static let questions = "questions"
            static let ideas = "suggestions"
            static let answered = "answered"
            static let not_answered = "not_answered"
        }
        static let treatments = "treatments"
        static let phone = "phone"
        static let snils = "snils"
        static let user_id = "user_id"

    }
    var ESIA_USERS_ID = ""
    var ESIA_USERS_SESSIONS_ID = ""
    var address = ""
    var createdAt = ""
    var district: String?
    var custom_district_string: String?
    var email = ""
    var expire_at = ""
    var firstName = ""
    var isMobile = false
    var is_mobile = false
    var lastAuthorized = ""
    var lastName = ""
    var middleName = ""
    var city = ""
    var cityOwner = ""
    var region = ""
    var street = ""
    var building = ""
    var block = ""
    var flat = ""
    var phone = ""
    var snils = ""
    var user_id = ""
    var questions_answered = 0
    var questions_not_answered = 0
    var ideas_answered = 0
    var ideas_not_answered = 0
    var treatments = 0
    var index = ""
    var votes = 0
    

        
    init(with json: NSDictionary) {
        
        ESIA_USERS_ID = json[keys.ESIA_USERS_ID] as? String ?? String(json[keys.ESIA_USERS_ID] as? Int ?? 0)
        ESIA_USERS_SESSIONS_ID = json[keys.ESIA_USERS_SESSIONS_ID] as? String ?? String(json[keys.ESIA_USERS_SESSIONS_ID] as? Int ?? 0)
        address = json[keys.address] as? String ?? String(json[keys.address] as? Int ?? 0)
        createdAt = json[keys.createdAt] as? String ?? String(json[keys.createdAt] as? Int ?? 0)
        district = json[keys.district] as? String ?? String(json[keys.district] as? Int ?? 0)
        custom_district_string = json[keys.custom_district] as? String
        email = json[keys.email] as? String ?? String(json[keys.email] as? Int ?? 0)
        expire_at = json[keys.expire_at] as? String ?? String(json[keys.expire_at] as? Int ?? 0)
        firstName = json[keys.firstName] as? String ?? String(json[keys.firstName] as? Int ?? 0)
        isMobile = json[keys.isMobile] as? Bool ?? false
        is_mobile = json[keys.is_mobile] as? Bool ?? false
        lastAuthorized = json[keys.lastAuthorized] as? String ?? String(json[keys.lastAuthorized] as? Int ?? 0)
        lastName = json[keys.lastName] as? String ?? String(json[keys.lastName] as? Int ?? 0)
        middleName = json[keys.middleName] as? String ?? String(json[keys.middleName] as? Int ?? 0)
        region = json[keys.parsedAddress.region] as? String ?? String(json[keys.parsedAddress.region] as? Int ?? 0)
        votes = json[keys.votes] as? Int ?? Int(json[keys.votes] as? String ?? "0") ?? 0

        
        if let dict = json[keys.parsedAddress.main] as? NSDictionary {
            city = dict[keys.parsedAddress.city] as? String ?? String(dict[keys.parsedAddress.city] as? Int ?? 0)
            cityOwner = dict[keys.parsedAddress.cityOwner] as? String ?? String(dict[keys.parsedAddress.cityOwner] as? Int ?? 0)
            street = dict[keys.parsedAddress.street] as? String ?? ""
            building = dict[keys.parsedAddress.building] as? String ?? (dict[keys.parsedAddress.building] as? Int).string
            block = dict[keys.parsedAddress.block] as? String ?? (dict[keys.parsedAddress.block] as? Int).string
            flat = dict[keys.parsedAddress.flat] as? String ?? (dict[keys.parsedAddress.flat] as? Int).string
            index = dict[keys.parsedAddress.index] as? String ?? (dict[keys.parsedAddress.index] as? Int).string

        }
        
        if let questions = json[keys.questionsAndIdes.questions] as? NSDictionary {
            let answeredValue = questions[keys.questionsAndIdes.answered]
            let notansweredValue = questions[keys.questionsAndIdes.not_answered]
            questions_answered = answeredValue as? Int ?? Int(answeredValue as? String ?? "0") ?? 0
            questions_not_answered = notansweredValue as? Int ?? Int(notansweredValue as? String ?? "0") ?? 0
        }
        
        treatments = json[keys.treatments] as? Int ?? Int(json[keys.treatments] as? String ?? "0") ?? 0
        
        if let ideas = json[keys.questionsAndIdes.ideas] as? NSDictionary {
            let answeredValue = ideas[keys.questionsAndIdes.answered]
            let notansweredValue = ideas[keys.questionsAndIdes.not_answered]
            ideas_answered = answeredValue as? Int ?? Int(answeredValue as? String ?? "0") ?? 0
            ideas_not_answered = notansweredValue as? Int ?? Int(notansweredValue as? String ?? "0") ?? 0
        }
        
        phone = json[keys.phone] as? String ?? String(json[keys.phone] as? Int ?? 0)
        snils = json[keys.snils] as? String ?? String(json[keys.snils] as? Int ?? 0)
        user_id = json[keys.user_id] as? String ?? String(json[keys.user_id] as? Int ?? 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        ESIA_USERS_ID = aDecoder.decodeObject(forKey: keys.ESIA_USERS_ID) as? String ?? ""
        ESIA_USERS_SESSIONS_ID = aDecoder.decodeObject(forKey: keys.ESIA_USERS_SESSIONS_ID) as? String ?? ""
        address = aDecoder.decodeObject(forKey: keys.address) as? String ?? ""
        createdAt = aDecoder.decodeObject(forKey: keys.createdAt) as? String ?? ""
        district = aDecoder.decodeObject(forKey: keys.district) as? String ?? ""
        custom_district_string = aDecoder.decodeObject(forKey: keys.custom_district) as? String
        email = aDecoder.decodeObject(forKey: keys.email) as? String ?? ""
        expire_at = aDecoder.decodeObject(forKey: keys.expire_at) as? String ?? ""
        firstName = aDecoder.decodeObject(forKey: keys.firstName) as? String ?? ""
        isMobile = aDecoder.decodeObject(forKey: keys.isMobile) as? Bool ?? false
        is_mobile = aDecoder.decodeObject(forKey: keys.is_mobile) as? Bool ?? false
        lastAuthorized = aDecoder.decodeObject(forKey: keys.lastAuthorized) as? String ?? ""
        lastName = aDecoder.decodeObject(forKey: keys.lastName) as? String ?? ""
        middleName = aDecoder.decodeObject(forKey: keys.middleName) as? String ?? ""
        city = aDecoder.decodeObject(forKey: keys.parsedAddress.city) as? String ?? ""
        cityOwner = aDecoder.decodeObject(forKey: keys.parsedAddress.cityOwner) as? String ?? ""
        region = aDecoder.decodeObject(forKey: keys.parsedAddress.region) as? String ?? ""
        street = aDecoder.decodeObject(forKey: keys.parsedAddress.street) as? String ?? ""
        phone = aDecoder.decodeObject(forKey: keys.phone) as? String ?? ""
        snils = aDecoder.decodeObject(forKey: keys.snils) as? String ?? ""
        user_id = aDecoder.decodeObject(forKey: keys.user_id) as? String ?? ""
        questions_answered = aDecoder.decodeInteger(forKey: "questions_answered")
        questions_not_answered = aDecoder.decodeInteger(forKey: "questions_not_answered")
        ideas_answered = aDecoder.decodeInteger(forKey: "ideas_answered")
        ideas_not_answered = aDecoder.decodeInteger(forKey: "ideas_not_answered")
        treatments = aDecoder.decodeInteger(forKey: "treatments")
        votes = aDecoder.decodeInteger(forKey: "votes")
        
        building = aDecoder.decodeObject(forKey: keys.parsedAddress.building) as? String ?? ""
        block = aDecoder.decodeObject(forKey: keys.parsedAddress.block) as? String ?? ""
        flat = aDecoder.decodeObject(forKey: keys.parsedAddress.flat) as? String ?? ""
        index = aDecoder.decodeObject(forKey: keys.parsedAddress.index) as? String ?? ""


    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(ESIA_USERS_ID, forKey: keys.ESIA_USERS_ID)
        aCoder.encode(ESIA_USERS_SESSIONS_ID, forKey: keys.ESIA_USERS_SESSIONS_ID)
        aCoder.encode(address, forKey: keys.address)
        aCoder.encode(createdAt, forKey: keys.createdAt)
        aCoder.encode(district, forKey: keys.district)
        aCoder.encode(custom_district_string, forKey: keys.custom_district)
        aCoder.encode(email, forKey: keys.email)
        aCoder.encode(expire_at, forKey: keys.expire_at)
        aCoder.encode(firstName, forKey: keys.firstName)
        aCoder.encode(isMobile, forKey: keys.isMobile)
        aCoder.encode(is_mobile, forKey: keys.is_mobile)
        aCoder.encode(lastAuthorized, forKey: keys.lastAuthorized)
        aCoder.encode(lastName, forKey: keys.lastName)
        aCoder.encode(middleName, forKey: keys.middleName)
        aCoder.encode(city, forKey: keys.parsedAddress.city)
        aCoder.encode(cityOwner, forKey: keys.parsedAddress.cityOwner)
        aCoder.encode(region, forKey: keys.parsedAddress.region)
        aCoder.encode(street, forKey: keys.parsedAddress.street)
        aCoder.encode(phone, forKey: keys.phone)
        aCoder.encode(snils, forKey: keys.snils)
        aCoder.encode(user_id, forKey: keys.user_id)
        aCoder.encode(questions_answered, forKey: "questions_answered")
        aCoder.encode(questions_not_answered, forKey: "questions_not_answered")
        aCoder.encode(ideas_answered, forKey: "ideas_answered")
        aCoder.encode(ideas_not_answered, forKey: "ideas_not_answered")
        aCoder.encode(treatments, forKey: "treatments")
        aCoder.encode(votes, forKey: "votes")
        
        aCoder.encode(building, forKey: keys.parsedAddress.building)
        aCoder.encode(block, forKey: keys.parsedAddress.block)
        aCoder.encode(flat, forKey: keys.parsedAddress.flat)
        aCoder.encode(index, forKey: keys.parsedAddress.index)


    }
    
    
    

    
}

extension User {
    var dictToSave: [String: Any] {
        get {
            return [
                keys.ESIA_USERS_ID: ESIA_USERS_ID,
                keys.ESIA_USERS_SESSIONS_ID: ESIA_USERS_SESSIONS_ID,
                keys.address: address,
                keys.createdAt: createdAt,
                keys.district: district,
                keys.custom_district: custom_district_string,
                keys.email: email,
                keys.expire_at: expire_at,
                keys.isMobile: isMobile,
                keys.is_mobile: is_mobile,
                keys.lastAuthorized: lastAuthorized,
                keys.phone: phone,
                keys.snils: snils,
                keys.user_id: user_id,
                keys.parsedAddress.index: index,
                keys.parsedAddress.city: city,
                keys.parsedAddress.region: region,
                keys.parsedAddress.street: street,
                keys.parsedAddress.building: building,
                keys.parsedAddress.block: block,
                keys.parsedAddress.flat: flat
            ]
        }
    }
}
