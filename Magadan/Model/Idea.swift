//
//  Idea.swift
//  Magadan
//
//  Created by Константин on 11.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation

class Idea {
    
    enum keys {
        static let date = "date"
        static let id = "id"
        static let title = "title"
        static let theme_id = "theme"
        static let watches_count = "views"
        static let answer = "answer"
        static let isAnswered = "hasAnswer"
        static let likes_count = "likes"
        static let isLiked = "isLiked"
        static let isMy = "userCreate"
        static let image = "image"
        static let commentsCount = "comments"
        static let files = "file"
    }
    
    var title: String = "preved"
    var date: Date = Date()
    var watches_count = 0
    var likes_count = 0
    var theme_id = ""
    var theme_title = ""
    var id = 0
    var answer = ""
    var isAnswered = false
    var isLiked = false
    var isMy = false
    var image = ""
    var commentsCount = 0
    var files: [String] = []
    
    var hasImage: Bool {
        get {
            return !image.isEmpty && URL(string: image) != nil
        }
    }
    
    var answerIsEmpty: Bool {
        get {
            return answer.isEmpty || answer == "Ответ еще не поступил"
        }
    }
    
    
    init(with dict: NSDictionary) {
        id  = dict[keys.id] as? Int ?? Int(dict[keys.id] as? String ?? "0") ?? 0
        title = dict[keys.title] as? String ?? ""
        watches_count = dict[keys.watches_count] as? Int ?? Int(dict[keys.watches_count] as? String ?? "0") ?? 0
        
        likes_count = dict[keys.likes_count] as? Int ?? Int(dict[keys.likes_count] as? String ?? "0") ?? 0
        isLiked = dict[keys.isLiked] as? Bool ?? false
        
        date = Date.init(dateFromVeryStrangeString: dict[keys.date] as? String ?? "00000000000000")
        theme_id = dict[keys.theme_id] as? String ?? String(dict[keys.theme_id] as? Int ?? 0)
        theme_title = dict["theme_title"] as? String ?? ""
        answer = dict[keys.answer] as? String ?? "Ответ еще не поступил"
        isAnswered = dict[keys.isAnswered] as? Bool ?? false
        isMy = dict[keys.isMy] as? Bool ?? false
        image = dict[keys.image] as? String ?? ""
        commentsCount = dict[keys.commentsCount] as? Int ?? Int(dict[keys.commentsCount] as? String ?? "0") ?? 0
        files = (dict[keys.files] as? [String] ?? []).map({Server.url_paths.imagesPathPreffix + $0})
    }
}

