//
//  Comment.swift
//  Magadan
//
//  Created by Константин on 08/10/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation


class Comment: Comparable {
    static func < (lhs: Comment, rhs: Comment) -> Bool {
        return lhs.date.timeIntervalSince1970 < rhs.date.timeIntervalSince1970
    }
    
    static func == (lhs: Comment, rhs: Comment) -> Bool {
        return lhs.id == rhs.id
    }
    
    
    var id: String!
    var date: Date!
    var text: String!
    var username: String!
    
    init?(with _dict: NSDictionary?)  {
        guard let dict = _dict else { return nil }
        
        id = dict["id"] as? String ?? ""
        date = Date.init(dateFromVeryStrangeString: dict["date"] as? String ?? "00000000000000")
        text = dict["message"] as? String ?? ""
        username = dict["name"] as? String ?? ""
        if id.isEmpty {
            return nil
        }
        
    }
}
