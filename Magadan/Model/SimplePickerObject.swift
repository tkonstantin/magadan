//
//  SimplePickerObject.swift
//  Magadan
//
//  Created by Константин on 13.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation


class SimplePickerObject {
    var id = ""
    var title = ""
    
    
    init(with json: NSDictionary) {
        id = json["id"] as? String ?? String(json["id"] as? Int ?? 0)
        title = json["title"] as? String ?? ""
    }
    
}
