//
//  MenuItem.swift
//  Magadan
//
//  Created by Константин on 26.01.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit


class MenuItem {
    var id: String?
    var img: UIImage?
    var title = ""
    var backgroundImage: String = ""
    var img_link: String = ""
    var isDynamic = false
    var shouldHideTitle = false
    
    init(img _img: UIImage, title _title: String) {
        img = _img
        title = _title
        isDynamic = false
    }
    
    init(with json: NSDictionary) {
        img_link = Server.url_paths.imagesPathPreffix + (json["image"] as? String ?? "")
        backgroundImage = Server.url_paths.imagesPathPreffix + (json["background"] as? String ?? "")
        id = json["id"] as? String ?? String(json["id"] as? Int ?? 0)
        title = json["title"] as? String ?? ""
        isDynamic = true
        shouldHideTitle = json["hideTitle"] as? Bool ?? false

    }
    
}


