//
//  Poll.swift
//  Magadan
//
//  Created by Константин on 07.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation


class Poll {

    var id = ""
    var startDate = Date()
    var endDate = Date()
    var votes_count = "0"
    var isCompleted: Bool = false
    var image: String?
    
    var title = "Выбор «народного» символа Магаданской области"
    var body = "В преддверии празднования Дня России ФГУП «Роспатриотцентр» проводит акцию в формате видеосъемки информационно-просветительских роликов о «народных» символах субъектов Российской Федерации, которые впоследствии будут использоваться в качестве презентационно-образовательных пособий в школах, дошкольных организациях, детских лагерях страны. "
    
    var questions: [Question] = []
    
    
    init(with json: NSDictionary) {
        id = json[pollKeys.id] as? String ?? String(json[pollKeys.id] as? Int ?? 0)
        startDate = Date.init(dateFromVeryStrangeString: json[pollKeys.startDate] as? String ?? "00000000000000")
        endDate = Date.init(dateFromVeryStrangeString: json[pollKeys.endDate] as? String ?? "00000000000000")
        votes_count = json[pollKeys.votes_count] as? String ?? String(json[pollKeys.votes_count] as? Int ?? 0)
        title = json[pollKeys.title] as? String ?? ""
        
        body = json[pollKeys.body] as? String ?? ""
        isCompleted = json[pollKeys.isCompleted] as? Bool ?? (Int(json[pollKeys.isCompleted] as? String ?? "0") == 1)
        image = json[pollKeys.image] as? String

        
        if let raw_answers = json[pollKeys.questionsArr] as? NSDictionary {
            for key in raw_answers.allKeys {
                if let dict = raw_answers[key] as? NSDictionary {
                    questions.append(Question(with: dict))
                    
                }
            }
        }
        
        questions.sort(by: {$0.id < $1.id})
    }
}


extension Poll {
    enum pollKeys {
        static let id = "VOTE_ID"
        static let startDate = "START_DATE"
        static let endDate = "END_DATE"
        static let title = "TITLE"
        static let body = "AUTHOR_NAME"
        static let questionsArr = "QA"
        static let votes_count = "FILL_COUNT"
        static let isCompleted = "voteExist"
        static let image = "image"
    }
}

