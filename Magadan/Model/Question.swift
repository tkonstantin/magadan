//
//  Question.swift
//  Magadan
//
//  Created by Константин on 30.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation



class Question {
    
    enum questionKeys {
        static let title = "TITLE"
        static let type = "VOTE_TYPE"
        static let id = "VOTE_QUESTION_ID"
        static let parent_asnwer = "PARENT_ANSWER"
        static let text = "text"
    }
    
    var id = ""
    var title = ""
    var type = ""
    var answers: [Answer] = []
    var selectedAnswerIndexes: [Int] = []
    var parentAnswer = ""
    var text: String?
    
    var answersCount: Int {
        get {
            if isText {
                return 1
            } else {
                return answers.count
            }
        }
    }
    
    var isMultiple: Bool {
        get {
            return type.lowercased() != "single"
        }
    }
    
    var isText: Bool {
        get {
            return type.lowercased() == "text"
        }
    }
    
    
    
    init(with json: NSDictionary) {
        id = json[questionKeys.id] as? String ?? String(json[questionKeys.id] as? Int ?? 0)
        title = json[questionKeys.title] as? String ?? ""
        type = json[questionKeys.type] as? String ?? ""
        parentAnswer = json[questionKeys.parent_asnwer] as? String ?? (json[questionKeys.parent_asnwer] as? Int).string
        text = json[questionKeys.text] as? String
        
        if let items = json["ITEMS"] as? NSDictionary {
            for key in items.allKeys {
                if let fuckingAnswer = items[key] as? NSDictionary {
                    answers.append(Answer(with: fuckingAnswer))
                }
            }
        }
        
        answers.sort(by: {$0.order < $1.order})
    }
}
