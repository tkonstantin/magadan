//
//  AuthorizationSession.swift
//  Magadan
//
//  Created by Константин on 31.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation


class AuthorizationSession: NSObject, NSCoding {
    var expire_at: Int = 0
    var is_mobile: Bool = true
    var session_id: String = ""
    var user_id: Int = 0
    
    init?(with o_json: NSDictionary?) {
        guard let json = o_json, let sid = json["session_id"] as? String, let uid = json["user_id"] as? Int ?? Int(json["user_id"] as! String) else { return nil }
            expire_at = json["expire_at"] as? Int ?? 0
            is_mobile = json["is_mobile"] as? Bool ?? true
            session_id = sid
            user_id = uid
    }
    
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(expire_at, forKey: "expire_at")
        aCoder.encode(is_mobile, forKey: "is_mobile")
        aCoder.encode(session_id, forKey: "session_id")
        aCoder.encode(user_id, forKey: "user_id")

    }
    
    required init?(coder aDecoder: NSCoder) {
        guard let sid = aDecoder.decodeObject(forKey: "session_id") as? String else { return }
        self.expire_at = aDecoder.decodeInteger(forKey: "expire_at")
        self.is_mobile = aDecoder.decodeBool(forKey: "is_mobile")
        self.session_id =  sid
        self.user_id = aDecoder.decodeInteger(forKey: "user_id")
    }
}
