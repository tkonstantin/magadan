//
//  AppDelegate+Shortcuts.swift
//  GD-Phonebook
//
//  Created by Константин on 10.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

extension AppDelegate {
    
    func addShourcuts(for application: UIApplication) {
        if #available(iOS 9.1, *) {

            let favotires = UIApplicationShortcutItem(type: shortCuts.events.rawValue, localizedTitle: "Мероприятия", localizedSubtitle: nil, icon:  UIApplicationShortcutIcon(templateImageName: "events_icon"), userInfo: nil)
            let recents = UIApplicationShortcutItem(type: shortCuts.reception.rawValue, localizedTitle: "Приемная", localizedSubtitle: nil, icon:  UIApplicationShortcutIcon(templateImageName: "reception_icon"), userInfo: nil)
            let contacts = UIApplicationShortcutItem(type: shortCuts.contacts.rawValue, localizedTitle: "Справочник", localizedSubtitle: nil, icon:  UIApplicationShortcutIcon(templateImageName: "phonebook_icon"), userInfo: nil)
            let search = UIApplicationShortcutItem(type: shortCuts.polls.rawValue, localizedTitle: "Опросы", localizedSubtitle: nil, icon:  UIApplicationShortcutIcon(templateImageName: "polls_icon"), userInfo: nil)
            
            application.shortcutItems = [favotires, recents, contacts, search]
        }
        
    }
    
    
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        shortCuts(shortcutItem.type)?.action()
        completionHandler(true)
    }
    
    
    enum shortCuts {
        case events
        case reception
        case contacts
        case polls
        
        var rawValue: String {
            get {
                switch self {
                case .events: return "events"
                case .reception: return "reception"
                case .contacts: return "contacts"
                case .polls: return "polls"
                }
            }
        }
        
        var action: (()->Void) {
            get {
                let currentAction: (()->Void) = {
                    switch self {
                    case .events: return { root?.mainNav?.viewControllers = [EventsViewController.makeOne()]}
                    case .reception: return { root?.mainNav?.viewControllers = [ReceptionViewController.makeThemes()]}
                    case .contacts: return { root?.mainNav?.viewControllers = [PhonebookViewController.makeOne()]}
                    case .polls: return { root?.mainNav?.viewControllers = [PollsListViewController.makeOne()]}
                    }
                }()
                
                if root?.mainNav == nil {
                    (UIApplication.shared.delegate as? AppDelegate)?.waitingShortCutAction = currentAction
                }
                return currentAction
                
            }
        }
        
        
        init?(_ rawValue: String) {
            for type in [shortCuts.events, .reception, .contacts, .polls] {
                if rawValue == type.rawValue {
                    self = type
                    return
                }
            }
            return nil
        }
    }
}

func forceOpenReception(with theme: Theme?=nil) {
    if theme == nil {
        root?.mainNav?.viewControllers = [ReceptionViewController.makeThemes()]
    } else {
        let fzAlert = UIAlertController(title: "ВНИМАНИЕ!", message: "В соответствии со статьей 7 Федерального закона от 2 мая 2006 г. № 59-ФЗ \"О порядке рассмотрения обращения граждан Российской Федерации\" в обязательном порядке Вам необходимо указать фамилию, имя, отчество (последнее - при наличии), адрес электронной почты, если ответ должен быть направлен в форме электронного документа, и почтовый адрес, если ответ должен быть направлен в письменной форме.", preferredStyle: .alert)
        fzAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            let themes = ReceptionViewController.makeThemes()
            let reception = ReceptionViewController.makeOne(theme: theme!)
            root?.mainNav?.viewControllers = [themes, reception]
        }))
        root?.mainNav?.present(fzAlert, animated: true, completion: nil)
        

    }
}

func forceOpenQuestions(with _theme: Theme?=nil) {
    if let theme = _theme {
        let main = IdeasMainViewController.makeOne(theme: theme)
        root?.mainNav?.pushViewController(main, animated: true)
    } else {
        root?.mainNav?.viewControllers = [ThemesViewController.makeOne(inMode: .simple, delegate: nil)]
    }

}

var root: UIViewController? {
    get {
        return UIApplication.shared.keyWindow?.rootViewController
    }
}


extension UIViewController {
    var mainNav: MainNav? {
        get {
            if self is MainNav {
                return self as? MainNav
            } else {
                return self.navigationController as? MainNav
            }
        }
    }
}



