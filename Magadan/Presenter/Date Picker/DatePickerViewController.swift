//
//  DatePickerViewController.swift
//  Magadan
//
//  Created by Константин on 07.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit


protocol DatePickerDelegate:class {
    func datePicker_didPicked(month: Int, year: Int)
}

class DatePickerViewController: RotatableViewController {
    
    class func makeOne(month: Int=currentMonth, year: Int=currentYear, delegate: DatePickerDelegate) -> DatePickerViewController {
        let new = UIStoryboard(name: "DatePickerViewController", bundle: nil).instantiateInitialViewController() as! DatePickerViewController
        new.modalTransitionStyle = .crossDissolve
        new.modalPresentationStyle = .overCurrentContext
        new.selectedMonth = month
        new.selectedYear = year
        new.delegate = delegate
        return new
    }
    
    @IBOutlet weak var cancelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var pickButton: UIButton!
    @IBOutlet weak var picker: UIPickerView!
    
    weak var delegate: DatePickerDelegate?
    var selectedMonth: Int!
    var selectedYear: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        picker.dataSource = self
        
        picker.selectRow(selectedMonth - 1, inComponent: 0, animated: false)
        picker.selectRow(DatePickerViewController.years.index(of: selectedYear)!, inComponent: 1, animated: false)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.cancelBottomConstraint.constant = 24
        
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.85, initialSpringVelocity: 0.2, options: [], animations: {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.33)
        })
    }
    
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func pickTapped(_ sender: UIButton) {
        self.delegate?.datePicker_didPicked(month: picker.selectedRow(inComponent: 0) + 1, year: DatePickerViewController.years[picker.selectedRow(inComponent: 1)])
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        self.cancelBottomConstraint.constant = -700
        
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.85, initialSpringVelocity: 0.2, options: [], animations: {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0)
        }, completion: { _ in
            super.dismiss(animated: false, completion: completion)

        })
    }
    
    static var currentMonth: Int {
        get {
            return Date().dateToComponents().month
        }
    }
    
    static var currentYear: Int {
        get {
            return Date().dateToComponents().year
        }
    }
    
    fileprivate static var years: [Int] {
        get {
            return Array(2014...currentYear)
        }
    }
    
}

extension DatePickerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0: return 13
        default: return DatePickerViewController.years.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let text: String = {
            switch component {
            case 0:
                return (row + 1).monthName
            default:
                return String(DatePickerViewController.years[row])
            }
        }()
        
        return NSAttributedString(string: text, attributes: [NSAttributedStringKey.font : UIFont.init(name: "Roboto-Regular", size: 16) as Any])
    }
}
