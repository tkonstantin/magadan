//
//  Presenter.swift
//  Magadan
//
//  Created by Константин on 19.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import SVProgressHUD

func showHUD() {
    SVProgressHUD.show()
}

func dismissHUD() {
    SVProgressHUD.dismiss()
}

