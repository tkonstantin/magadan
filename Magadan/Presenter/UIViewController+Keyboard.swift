//
//  UIViewController+Keyboard.swift
//  Magadan
//
//  Created by Константин on 08.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

//MARK: - Keyboard appearance's logic
extension UIViewController {
    
    
    func addTapOutsideGestureRecognizer() {
        let tapOutSide = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        tapOutSide.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapOutSide)
    }
    
    func subscribeToKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeShown(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeShown(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    
    @objc func keyboardWillBeShown (_ notification: Notification) {
        let info = (notification as NSNotification).userInfo
        if let keyboardSize = (info?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.3, animations: {
                self.view.frame.origin.y = -keyboardSize.height
            })
        }
    }
    
    @objc func keyboardWillBeHidden() {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame.origin.y = 0
        })
    }
    
    
    
    
}


