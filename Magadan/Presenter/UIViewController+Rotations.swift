//
//  UIViewController+Rotations.swift
//  Magadan
//
//  Created by Константин on 19.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class RotatableViewController: ReloadingViewController {
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            if self is PhotoViewerViewController {
                return .all
            } else {
                return .portrait
            }
        }
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        get {
            return .portrait
        }
    }
    
}

class ReloadingViewController: UIViewController {
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(reloadScreen), name: networkNotification, object: nil)
    }
    
    @objc func reloadScreen() {}
}


class RotatableNavigationController: UINavigationController {
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            if self.topViewController is PhotoViewerViewController {
                return .all
            } else  {
                return .portrait
            }
        }
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        get {
            return .portrait
        }
    }
}
