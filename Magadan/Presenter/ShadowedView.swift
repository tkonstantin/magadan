//
//  ShadowedView.swift
//  Heloo2017
//
//  Created by Константин on 24.09.17.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class ShadowedView: UIView {
    
    override func awakeFromNib() {

        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 0.7
        self.layer.shadowColor = UIColor(hex: "36393C").withAlphaComponent(0.13).cgColor
        self.layer.masksToBounds = false
        

    }
    
}



class ShadowedImageView: UIImageView {
    
    override func awakeFromNib() {
        
        self.layer.shadowOffset = CGSize(width: 16, height: 16)
        self.layer.shadowRadius = 32
        self.layer.shadowOpacity = 1
        self.layer.shadowColor = UIColor(hex: "36393C").withAlphaComponent(0.13).cgColor
        self.layer.masksToBounds = false
        
        
    }
    
}


extension UIColor {
    convenience init(hex: String) {
        let color: UIColor = {
            var cString:String = hex.trimmingCharacters(in: (NSCharacterSet.whitespacesAndNewlines as NSCharacterSet) as CharacterSet).uppercased()
            
            if (cString.hasPrefix("#")) {
                cString = cString.substring(from: cString.characters.index(cString.startIndex, offsetBy: 1))
            }
            
            if ((cString.characters.count) != 6) {
                return UIColor.gray
            }
            
            var rgbValue:UInt32 = 0
            Scanner(string: cString).scanHexInt32(&rgbValue)
            
            return UIColor(
                red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                alpha: CGFloat(1.0)
            )
        }()
        
        self.init(cgColor: color.cgColor)
    }
}

