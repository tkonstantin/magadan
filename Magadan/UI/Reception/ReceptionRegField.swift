//
//  ReceptionRegField.swift
//  Magadan
//
//  Created by Константин on 08.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation

@objc class ReceptionRegField: NSObject {
    
    enum fieldTypes {
        case name
        case lastname
        case middlename
        case index
        case city
        case region
        case raw_region
        case district
        case raw_district
        case custom_district_string
        case street
        case building
        case block
        case flat
        case email
        case phone
        case address
        
        var displayString: String {
            get {
                switch self {
                case .name: return "Имя"
                case .lastname: return "Фамилия"
                case .middlename: return "Отчество"
                case .index: return "Индекс"
                case .city: return "Населенный пункт"
                case .region: return "Регион"
                case .district: return "Район"
                case .street: return "Улица"
                case .building: return "Дом"
                case .block: return "Корпус"
                case .flat: return "Квартира"
                case .email: return "E-mail"
                case .phone: return "Телефон"
                case .address: return "Адрес"
                case .raw_region: return ""
                case .custom_district_string: return ""
                case .raw_district: return ""
                    
                }
            }
        }
        
        var apiString: String {
            get {
                switch self {
                case .name: return "firstname"
                case .lastname: return "lastname"
                case .middlename: return "middlename"
                case .index: return "postal_index"
                case .city: return "city"
                case .region: return ""
                case .district: return ""
                case .street: return "street"
                case .building: return "building"
                case .block: return "building_block"
                case .flat: return "flat"
                case .email: return "email"
                case .phone: return "phone"
                case .address: return "address"
                case .raw_region: return "region"
                case .custom_district_string: return "custom_district"
                case .raw_district: return "district"
                }
            }
        }

    }
    
    var title: String {
        get {
            return type.displayString + (isRequired ? "*" : "")
        }
    }
    var type: fieldTypes = .name
    var value: String?
    var isRequired: Bool = false
    
    var isFilled: Bool {
        get {
            return value != nil && !value!.isEmpty
        }
    }
    init(_ _type: fieldTypes, isRequired required: Bool=false) {
        type = _type
        isRequired = required
    }
    
}


