//
//  ReceptionRegFieldGroup.swift
//  Magadan
//
//  Created by Константин on 08.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation


@objc class ReceptionRegFieldGroup: NSObject {
    var fields: [ReceptionRegField] = []
    
    init(_ fieldsArr: [ReceptionRegField]) {
        fields = fieldsArr
    }
}

