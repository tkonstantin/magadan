//
//  RecipientsViewController.swift
//  Magadan
//
//  Created by Константин on 08.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

protocol SimplePickerDelegate:class {
    func didPicked(item: SimplePickerObject?, type: SimplePickerViewController.picker_types)
}

class SimplePickerViewController: RotatableViewController, UITableViewDelegate, UITableViewDataSource {
    
    enum picker_types {
        case regions
        case districts
        case reception
        
        
        var title: String {
            get {
                switch self {
                case .regions: return "Регионы"
                case .districts: return "Районы"
                case .reception: return "Адресаты"
                }
            }
        }
    }
    
    class func makeOne(type: picker_types, needsClearButton: Bool, delegate: SimplePickerDelegate, items: [SimplePickerObject]) -> SimplePickerViewController {
        let new = UIStoryboard(name: "Reception", bundle: nil).instantiateViewController(withIdentifier: "SimplePickerViewController") as! SimplePickerViewController
        new.title = type.title
        new.type = type
        new.needsClearButton = needsClearButton
        new.delegate = delegate
        new.items = items
        return new
    }
    weak var delegate: SimplePickerDelegate?
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView()
        }
    }
    
    var needsClearButton = false
    var items: [SimplePickerObject] = []
    var type: picker_types = .regions
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if !needsClearButton {
            self.navigationItem.setRightBarButtonItems([], animated: false)
        }
    }
    
    @IBAction func clearTapped(_ sender: UIBarButtonItem) {
        delegate?.didPicked(item: nil, type: type)
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        (cell?.viewWithTag(1) as? UILabel)?.text = items[indexPath.row].title
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didPicked(item: items[indexPath.row], type: type)
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
