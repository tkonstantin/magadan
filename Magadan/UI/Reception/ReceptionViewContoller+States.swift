//
//  ReceptionViewContoller+States.swift
//  Magadan
//
//  Created by Константин on 08.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation

extension ReceptionViewController {
    enum states: Int {
        case first = 0
        case second
        
        var title: String {
            get {
                switch self {
                case .first: return "Ввод личной информации "
                case .second: return "Адресат обращения"
                }
            }
        }
    }
}
