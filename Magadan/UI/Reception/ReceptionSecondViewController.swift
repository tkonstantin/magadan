//
//  ReceptionSecondViewController.swift
//  Magadan
//
//  Created by Константин on 14.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

protocol ReceptionSecondDelegate:class {
    func second_completed()
}

class ReceptionSecondViewController: FileAttachingViewController {

    var state: ReceptionViewController.states = .second
    var theme: Theme!
    
    var values: [ReceptionRegField.fieldTypes: String] = [:]
    var req: ReceptionRequest!
    
    weak var delegate: ReceptionSecondDelegate!
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var symbolsCountLabel: UILabel!
    @IBOutlet weak var txtview: UITextView! {
        didSet {
            txtview.delegate = self
        }
    }
    @IBOutlet weak var recipien_label: UILabel!
    @IBOutlet weak var txtViewPlaceholder: UILabel!
    @IBOutlet weak var mainView: UIView!
  
    var pickedRecipient: Recipient! {
        didSet {
            self.recipien_label.text = pickedRecipient.title
        }
    }
    
    
    func checkFields() -> Bool {
        var hasEmpty = false

        hasEmpty = pickedRecipient == nil || txtview.text.isEmpty
        if hasEmpty {
            self.showOKAlert(title: "Ошибка", message: "Заполните обязательные поля")
            return false
        } else {
            return true
        }
    }
    
    @IBAction func nextTapped(_ sender: UIButton) {

            if checkFields() {
                showHUD()
                self.view.isUserInteractionEnabled = false
                Server.makeRequest.leaveReceptionRequest(req, theme: theme, completion: { (request_id) in
                    if request_id != nil && (self.attachedImages.count != 0 || self.hasFiles) {
                        Server.makeRequest.upload(images: self.attachedImages, files: self.attachedFiles, for: .reception, id: request_id!, completion: { (success) in
                            dismissHUD()
                            asyncAfter {
                                self.view.isUserInteractionEnabled = true
                                let alert = UIAlertController(title: success ? "Спасибо за Ваше обращение!" : "Ошибка", message: success ? nil : "Не удалось отправить сообщение", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                                    if success {
                                        self.navigationController?.popToRootViewController(animated: true)
                                        self.delegate.second_completed()
                                    }
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                        })
                    } else {
                        dismissHUD()
                        asyncAfter {
                            self.view.isUserInteractionEnabled = true
                            let alert = UIAlertController(title: request_id != nil ? "Спасибо за Ваше обращение!" : "Ошибка", message: request_id != nil ? nil : "Не удалось отправить сообщение", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                                if request_id != nil {
                                    self.navigationController?.popToRootViewController(animated: true)
                                    self.delegate.second_completed()
                                }
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
                    
                })
            }
    }
    
    @IBAction func doneTapped(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let picker = segue.destination as? SimplePickerViewController {
            picker.delegate = self
            picker.items = Server.makeRequest.receptionRecipients
        }
    }
    
}



extension ReceptionSecondViewController: SimplePickerDelegate {
    func didPicked(item: SimplePickerObject?, type: SimplePickerViewController.picker_types) {
        guard item != nil else {
            pickedRecipient = nil
            req.custom_fields["recipient"] = nil
            return;
        }
        pickedRecipient = item as? Recipient
        req.custom_fields["recipient"] = String(item!.id)
    }
}

extension ReceptionSecondViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.txtViewPlaceholder.isHidden = true
        self.doneButton.isHidden = false

    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            self.txtViewPlaceholder.isHidden = false
        }
        req.custom_fields["text"] = textView.text
        self.doneButton.isHidden = true
  
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.symbolsCountLabel.text = String(txtview.text.count) + "/1000"
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        return newText.count <= 1000
    }
}



