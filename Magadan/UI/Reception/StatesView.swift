//
//  StatesView.swift
//  Magadan
//
//  Created by Константин on 08.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class StatesView: UIView {

    var state: ReceptionViewController.states = .first
    
    @IBOutlet weak var bottomLine: UIView!
    @IBOutlet weak var bottomLine_leading: NSLayoutConstraint!
    @IBOutlet weak var bottomLine_width: NSLayoutConstraint!

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var progressLabel: UILabel!
    
    let numberOfItems = 2
    
    var lineWidth: CGFloat {
        get {
            return UIScreen.main.bounds.width/CGFloat(numberOfItems)
        }
    }
    
    
    func update(with _state: ReceptionViewController.states) {
        state = _state
        titleLabel.text = state.title
        progressLabel.text = "\(state.rawValue + 1) из \(numberOfItems)"
        
        bottomLine_width.constant = lineWidth
        bottomLine_leading.constant = lineWidth * CGFloat(state.rawValue)
        
        UIView.animate(withDuration: 0.3) {
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
        
    }


}
