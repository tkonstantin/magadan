//
//  ReceptionFieldTableViewCell.swift
//  Magadan
//
//  Created by Константин on 08.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

@objc protocol FieldCellDelegate:class {
    func fieldCellDidReturn(with regField: ReceptionRegField, group: ReceptionRegFieldGroup)
    func fieldCellDidStartTyping(with regField: ReceptionRegField, group: ReceptionRegFieldGroup)
    func fieldCellDidEndEditing(with regField: ReceptionRegField, group: ReceptionRegFieldGroup)
    @objc optional func fieldCellShouldBeginEditing(with regField: ReceptionRegField, group: ReceptionRegFieldGroup) -> Bool
}


class ReceptionFieldTableViewCell: UITableViewCell {
    
    static func cell_id(for count: Int) -> String {
        switch count {
        case 1: return "fieldCell"
        case 2: return "fieldCell2"
        default: return "fieldCell3"
        }
    }


    
    @IBOutlet weak var view: ReceptionFieldTableViewCellView!
    @IBOutlet weak var view2: ReceptionFieldTableViewCellView?
    @IBOutlet weak var view3: ReceptionFieldTableViewCellView?
    
    func update(with _regField: ReceptionRegField, value: String?=nil, group: ReceptionRegFieldGroup, isActive: Bool, isFilled: Bool, delegate _delegate: FieldCellDelegate, tag: Int) {
        switch tag {
        case 1: view.update(with: _regField, value: value, group: group, isActive: isActive, isFilled: isFilled, delegate: _delegate)
        case 2: view2?.update(with: _regField, value: value, group: group, isActive: isActive, isFilled: isFilled, delegate: _delegate)
        case 3: view3?.update(with: _regField, value: value, group: group, isActive: isActive, isFilled: isFilled, delegate: _delegate)
        default: return;
        }
        
    }
}

class ReceptionFieldTableViewCellView: UITableViewCell {
    
    
    fileprivate weak var delegate: FieldCellDelegate!
    
    
    fileprivate let greenColor = UIColor(hex: "52B448")
    fileprivate let redColor = UIColor(hex: "B44848")
    
    
    fileprivate var isActive: Bool = false
    fileprivate var isFilled: Bool = false
    
    @IBOutlet fileprivate weak var field: UITextField!
    
    @IBOutlet fileprivate weak var bottomLine: UIView!
    @IBOutlet fileprivate weak var field_subtitle: UILabel!
    //    @IBOutlet fileprivate weak var dotView: UIView!
    
    fileprivate var regField: ReceptionRegField!
    fileprivate var fieldGroup: ReceptionRegFieldGroup!
    
    func update(with _regField: ReceptionRegField, value: String?=nil, group: ReceptionRegFieldGroup, isActive: Bool, isFilled: Bool, delegate _delegate: FieldCellDelegate) {
        regField = _regField
        fieldGroup = group
        delegate = _delegate
        field.attributedPlaceholder = NSAttributedString(string: regField.title, attributes: [NSAttributedStringKey.foregroundColor : UIColor(hex: "808080"), NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16)])
        field.text = value ?? regField.value
        field.delegate = self
        field_subtitle.text = regField.title
        update_active(for: isActive)
        update_filled(for: isFilled)
    }

    fileprivate func update_active(for value: Bool) {
        bottomLine.backgroundColor = value ? blueColor : UIColor(hex: "808080")
    }
    
    fileprivate func update_filled(for value: Bool) {
        field_subtitle.isHidden = !value
        //        dotView.backgroundColor = value ? greenColor : redColor
        
    }
    
    
}

extension ReceptionFieldTableViewCellView: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return self.delegate?.fieldCellShouldBeginEditing?(with: self.regField, group: fieldGroup) ?? true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        self.delegate?.fieldCellDidReturn(with: self.regField, group: fieldGroup)
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.delegate?.fieldCellDidStartTyping(with: self.regField, group: fieldGroup)
        self.update_active(for: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.regField.value = textField.text
        self.delegate?.fieldCellDidEndEditing(with: self.regField, group: fieldGroup)
        self.update_active(for: false)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
        self.update_filled(for: !newText.isEmpty)
        return true
    }
}



