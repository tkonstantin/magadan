//
//  ReceptionViewController.swift
//  Magadan
//
//  Created by Константин on 26.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit


class ReceptionViewController: SearchableViewController {
    
    class func makeOne(theme: Theme) -> ReceptionViewController {
        let reception = (UIStoryboard(name: "Reception", bundle: nil).instantiateInitialViewController() as! UINavigationController).viewControllers.first as! ReceptionViewController
        reception.theme = theme
        return reception
    }
    
    class func makeThemes() -> ThemesViewController {
        return ThemesViewController.makeOne(inMode: .reception, delegate: nil)
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var statesView: StatesView!
    @IBOutlet weak var nextButton: UIButton!
    
    var state: states = .first
    var theme: Theme!
    
    var groupFields: [ReceptionRegFieldGroup] {
        get {
            switch state {
            case .first:
                return [ReceptionRegFieldGroup([ReceptionRegField(.lastname, isRequired: true)]),
                        ReceptionRegFieldGroup([ReceptionRegField(.name, isRequired: true)]),
                        ReceptionRegFieldGroup([ReceptionRegField(.middlename)]),
                        ReceptionRegFieldGroup([ReceptionRegField(.index), ReceptionRegField(.city)]),
                        ReceptionRegFieldGroup([ReceptionRegField(.region), ReceptionRegField(.district)]),
                        ReceptionRegFieldGroup([ReceptionRegField(.street)]),
                        ReceptionRegFieldGroup([ReceptionRegField(.building), ReceptionRegField(.block), ReceptionRegField(.flat)]),
                        ReceptionRegFieldGroup([ReceptionRegField(.email, isRequired: true), ReceptionRegField(.phone)])
                ]
                
            default: return []
            }
            
        }
    }
    
    var activeFieldType: ReceptionRegField.fieldTypes?
    
    var values: [ReceptionRegField.fieldTypes: String] = [:]
    
    var availableDistricts: [District] {
        get {
            return Server.makeRequest.availableProfileDistricts
        }
    }
    
    
    func fillWithCurrentUser() {
        let user = UserDataManager.shared.currentUser
        values[.lastname] = user?.lastName
        values[.name] = user?.firstName
        values[.middlename] = user?.middleName
        values[.index] = user?.index
        values[.city] = user?.city
        values[.region] = Server.makeRequest.availableRegions.first(where: {$0.id == user?.region})?.title
        values[.raw_region] = user?.region
        values[.custom_district_string] = user?.custom_district_string
        values[.raw_district] = user?.district
        values[.district] = user?.custom_district_string ?? availableDistricts.first(where: {$0.id == values[.raw_district]})?.title
        values[.street] = user?.street
        values[.building] = user?.building
        values[.block] = user?.block
        values[.flat] = user?.flat
        values[.email] = user?.email
        values[.phone] = user?.phone
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Server.makeRequest.getAvailableDistricts {}
        self.addTapOutsideGestureRecognizer()
        self.configureTable()
        self.subscribeToKeyboard()
        Server.makeRequest.getReceptionRecipients {}
        self.fillWithCurrentUser()
        
    }
    
    
    var keyboardHeight: CGFloat = 0
    
    override func keyboardWillBeShown(_ notification: Notification) {
        let info = (notification as NSNotification).userInfo
        if let keyboardSize = (info?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
            let keyboardFrame = self.tableView.convert(keyboardSize, from: nil)
            let intersect = keyboardFrame.intersection(self.tableView.bounds)
            UIView.animate(withDuration: (info?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0.0, animations: {
                self.tableView.contentInset = UIEdgeInsetsMake(0, 0, intersect.size.height + 16, 0)
                self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, intersect.size.height + 16, 0)
            })
        }
        
    }
    
    override func keyboardWillBeHidden() {
        UIView.animate(withDuration: 0.3) {
            self.tableView.contentInset = .zero
            self.tableView.scrollIndicatorInsets = .zero
        }
    }
    
    
    @IBAction func termsTextTapped(_ sender: UIButton) {
        UIApplication.shared.openURL(URL(string: "https://www.49gov.ru/feedback/desk/personal_information")!)
    }
    
    @IBOutlet weak var checkMarkButton: UIButton!
    
    
    
    
    func reloadFields() {
        self.tableView.reloadData()
        asyncAfter {
            self.tableView.scrollRectToVisible(CGRect(origin: .zero, size: CGSize(width: self.view.bounds.width, height: 20)), animated: true)
        }
        
    }
    
    @IBAction func checkmarkTapped(_ sender: UIButton) {
        if sender.image(for: .normal) == #imageLiteral(resourceName: "checkmark_empty") {
            isCheckmarkFilled = true
            sender.setImage(#imageLiteral(resourceName: "checkmark_filled"), for: .normal)
            nextButton.alpha = 1
        } else {
            sender.setImage(#imageLiteral(resourceName: "checkmark_empty"), for: .normal)
            isCheckmarkFilled = false
            nextButton.alpha = 0.5
        }
    }
    
    let req = ReceptionRequest()
    var isCheckmarkFilled = false
    
    func checkFields() -> Bool {
        
        guard isCheckmarkFilled else {self.showOKAlert(title: "Необходимо принять условия пользовательского соглашения", message: nil); return false }
        
        var hasEmpty = false
        
        for value in self.values {
            if value.value.isEmpty && (value.key == .name || value.key == .lastname || value.key == .email) {
                hasEmpty = true
            }
            if !value.value.isEmpty && !value.value.isValidEmail && value.key == .email {
                self.showOKAlert(title: "Ошибка", message: "Некорректный Email")
                return false
            }
        }
        
        
        
        if hasEmpty {
            self.showOKAlert(title: "Ошибка", message: "Заполните обязательные поля")
            return false
        } else {
            return true
        }
    }
    
    @IBAction func nextTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if checkFields() {
            self.performSegue(withIdentifier: "toSecond", sender: nil)
        }
        
    }
    
    func fillMeAlert() {
        let alert = UIAlertController(title: "Заполните все поля!", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMenu" {
            currentPath = IndexPath(row: 4, section: 0)
        } else if let second = segue.destination as? ReceptionSecondViewController {
            second.values = self.values
            second.req = self.prepareToSecondStage(self.req)
            second.delegate = self
            second.theme = self.theme
        }
    }
    
    func prepareToSecondStage(_ req: ReceptionRequest) -> ReceptionRequest {
        let newReq = req
        
        for value in values {
            newReq.custom_fields[value.key.apiString] = value.value
        }
        
        if values[.custom_district_string] != nil {
            newReq.custom_fields[ReceptionRegField.fieldTypes.raw_district.apiString] = nil
            newReq.custom_fields[ReceptionRegField.fieldTypes.custom_district_string.apiString] = values[.custom_district_string]
        } else if values[.raw_district] != nil {
            newReq.custom_fields[ReceptionRegField.fieldTypes.raw_district.apiString] = values[.raw_district]
            newReq.custom_fields[ReceptionRegField.fieldTypes.custom_district_string.apiString] = nil
        }
        
        newReq.custom_fields[ReceptionRegField.fieldTypes.raw_region.apiString] = values[.raw_region]
        newReq.custom_fields[ReceptionRegField.fieldTypes.region.apiString] = nil
        
        return newReq
    }
    
    
    
}


extension ReceptionViewController: ReceptionSecondDelegate {
    func second_completed() {
        self.fillWithCurrentUser()
        self.reloadFields()
    }
    
    
}


extension ReceptionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func configureTable() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentGroup = groupFields[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: ReceptionFieldTableViewCell.cell_id(for: currentGroup.fields.count)) as! ReceptionFieldTableViewCell
        for index in 0..<currentGroup.fields.count {
            let field = currentGroup.fields[index]
            field.value = values[field.type]
            if field.type == .district {
                
            }
            cell.update(with: field, group: currentGroup, isActive: activeFieldType == field.type, isFilled: field.isFilled, delegate: self, tag: index + 1)
        }
        
        return cell
    }
}

extension ReceptionViewController: FieldCellDelegate {
    
    func fieldCellShouldBeginEditing(with regField: ReceptionRegField, group: ReceptionRegFieldGroup) -> Bool {
        if regField.type == .district && self.values[.raw_region] == Region.magadan_id {
            self.view.endEditing(true)
            self.navigationController?.pushViewController(SimplePickerViewController.makeOne(type: .districts, needsClearButton: self.values[.district] != nil, delegate: self, items: Server.makeRequest.availableProfileDistricts), animated: true)
            return false
        } else if regField.type == .region {
            self.view.endEditing(true)
            self.navigationController?.pushViewController(SimplePickerViewController.makeOne(type: .regions, needsClearButton: self.values[.region] != nil, delegate: self, items: Server.makeRequest.availableRegions), animated: true)
            return false
        }else if (regField.type == .lastname || regField.type == .name || regField.type == .middlename) && UserDataManager.shared.isAuthorized {
            return false
        }
        return true
    }
    
    func fieldCellDidEndEditing(with regField: ReceptionRegField, group: ReceptionRegFieldGroup) {
        if regField.type == .district {
            values[.raw_district] = nil
            values[.custom_district_string] = regField.value
            values[.district] = regField.value
        } else {
            values[regField.type] = regField.value
        }
        
        req.custom_fields[regField.type.apiString] = regField.value
        activeFieldType = nil
        self.tableView.reloadData()
    }
    
    func fieldCellDidReturn(with regField: ReceptionRegField, group: ReceptionRegFieldGroup) {
        activeFieldType = nil
    }
    
    func fieldCellDidStartTyping(with regField: ReceptionRegField, group: ReceptionRegFieldGroup) {
        activeFieldType = regField.type
    }
}


extension ReceptionViewController: SimplePickerDelegate {
    
    func didPicked(item _item: SimplePickerObject?, type: SimplePickerViewController.picker_types) {
        guard let item = _item else {
            if type == .regions {
                self.values[.region] = nil
                self.values[.raw_region] = nil
                self.values[.raw_district] = nil
                self.values[.district] = nil
                self.values[.custom_district_string] = nil
            } else {
                self.values[.raw_district] = nil
                self.values[.district] = nil
                self.values[.custom_district_string] = nil
            }
            self.tableView.reloadData()
            return;
        }
        
        if item is Region {
            self.values[.region] = item.title
            self.values[.raw_region] = item.id
            self.values[.raw_district] = nil
            self.values[.district] = nil
            self.values[.custom_district_string] = nil
        } else {
            self.values[.raw_district] = item.id
            self.values[.district] = availableDistricts.first(where: {$0.id == values[.raw_district]})?.title ?? values[.custom_district_string]
            self.values[.custom_district_string] = nil
        }
        self.tableView.reloadData()
        
        
        
    }
}
