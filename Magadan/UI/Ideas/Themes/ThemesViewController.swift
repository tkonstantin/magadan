//
//  ThemesViewController.swift
//  Magadan
//
//  Created by Константин on 06.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit


class Theme {
    var icon: UIImage?
    var title: String?
    var id = "0"
    
    init(_ t: String, _ i: UIImage?, _ _id: String) {
        title = t
        icon = i
        id = _id
    }
}


protocol ThemePickerDelegate:class {
    func didPickTheme(theme: Theme)
}

class ThemesViewController: RotatableViewController {
    
    
    weak var delegate: ThemePickerDelegate?
    
    enum modes {
        case simple
        case picker
        case reception
    }
    
    var mode: modes = .simple
    
    class func makeOne(inMode: modes = modes.simple, delegate d: ThemePickerDelegate?=nil) -> ThemesViewController {
        let new = UIStoryboard(name: "Ideas", bundle: nil).instantiateViewController(withIdentifier: "ThemesViewController") as! ThemesViewController
        new.mode = inMode
        new.delegate = d
        return new
    }
    var themes: [Theme] = [
        Theme("Государство", #imageLiteral(resourceName: "Государство"), "6"),
        Theme("Экономика\nи финансы", #imageLiteral(resourceName: "Экономика"), "30"),
        Theme("Строительство\nи ЖКХ", #imageLiteral(resourceName: "Строительство"), "8"),
        Theme("Здравоохранение", #imageLiteral(resourceName: "Здравоохранение"), "10"),
        Theme("Социальная сфера", #imageLiteral(resourceName: "Социальная"), "23"),
        Theme("Образование\nи наука", #imageLiteral(resourceName: "Образование"), "16"),
        Theme("Дороги\nи транспорт", #imageLiteral(resourceName: "Дороги"), "25"),
        Theme("Информатизация\nи связь", #imageLiteral(resourceName: "Информатизация"), "35"),
        Theme("Спорт\nи туризм", #imageLiteral(resourceName: "Спорт"), "36"),
        Theme("Культура\nи досуг", #imageLiteral(resourceName: "Культура"), "13"),
        Theme("Природные ресуры\nи экология", #imageLiteral(resourceName: "Природные"), "18"),
        Theme("Безопасность\nи правопорядок", #imageLiteral(resourceName: "Безопасность"), "37")
    ]
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMenu", mode == .reception {
            currentPath = IndexPath(row: 4, section: 0)
        }
    }
        
        
    
}


extension ThemesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return themes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ThemeCollectionViewCell
        cell.update(with: themes[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = self.view.bounds.width/3 - 12
        return CGSize(width: side, height: side)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if mode == .simple {
            let main = IdeasMainViewController.makeOne(theme: themes[indexPath.item])
            self.navigationController?.pushViewController(main, animated: true)

        } else if mode == .picker {
            delegate?.didPickTheme(theme: themes[indexPath.item])
            self.navigationController?.popViewController(animated: true)
        }  else if mode == .reception {
            let questionsCount = Server.makeRequest.quesionsCountForTheme[self.themes[indexPath.item].id] ?? "🤷🏻‍♂️"
            guard questionsCount != "0" else {
                let main = ReceptionViewController.makeOne(theme: self.themes[indexPath.item])
                self.navigationController?.pushViewController(main, animated: true)
                return;
            }
            
            let alert = UIAlertController(title: "По теме \"\(self.themes[indexPath.item].title ?? "")\" опубликовано \(questionsCount) вопросов.", message: "Предлагаем Вам ознакомиться с ними. Возможно, вы найдете интересующий Вас ответ.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Посмотреть вопросы", style: .default, handler: { (_) in
                forceOpenQuestions(with: self.themes[indexPath.item])
            }))
            alert.addAction(UIAlertAction(title: "Написать обращение", style: .default, handler: { (_) in
                let fzAlert = UIAlertController(title: "ВНИМАНИЕ!", message: "В соответствии со статьей 7 Федерального закона от 2 мая 2006 г. № 59-ФЗ \"О порядке рассмотрения обращения граждан Российской Федерации\" в обязательном порядке Вам необходимо указать фамилию, имя, отчество (последнее - при наличии), адрес электронной почты, если ответ должен быть направлен в форме электронного документа, и почтовый адрес, если ответ должен быть направлен в письменной форме.", preferredStyle: .alert)
                fzAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    let main = ReceptionViewController.makeOne(theme: self.themes[indexPath.item])
                    self.navigationController?.pushViewController(main, animated: true)
                }))
                self.present(fzAlert, animated: true, completion: nil)

            }))
            
            self.present(alert, animated: true, completion: nil)

        }
    }

}
