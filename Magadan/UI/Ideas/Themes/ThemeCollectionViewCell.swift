//
//  ThemeCollectionViewCell.swift
//  Magadan
//
//  Created by Константин on 06.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class ThemeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var icon: ShadowedImageView!
    
    
    func update(with theme: Theme) {
        label.text = theme.title
        icon.image = theme.icon
    }
}
