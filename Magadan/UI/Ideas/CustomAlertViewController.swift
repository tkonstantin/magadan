//
//  CustomAlertViewController.swift
//  Magadan
//
//  Created by Константин on 11.01.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit


protocol CustomAlertDelegate: class {
    func receptionTapped()
    func okTapped()
}

class CustomAlertViewController: UIViewController {
    
    weak var delegate: CustomAlertDelegate?
    
    class func present(isIdeas: Bool, delegate: CustomAlertDelegate, from: UIViewController?=nil) {
        let alert = UIStoryboard(name: "Ideas", bundle: nil).instantiateViewController(withIdentifier: "CustomAlertViewController_" + (isIdeas ? "I" : "Q")) as! CustomAlertViewController
        alert.delegate = delegate
        alert.modalPresentationStyle = .overCurrentContext
        alert.modalTransitionStyle = .crossDissolve
        (from ?? UIApplication.shared.keyWindow?.rootViewController)?.present(alert, animated: true, completion: nil)
    }

 
    @IBAction func receptionTapped(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.delegate?.receptionTapped()
        }
    }
    
    @IBAction func okTapped(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.delegate?.okTapped()
        }
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
}
