//
//  CreateIdeaViewController.swift
//  Magadan
//
//  Created by Константин on 06.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class CreateIdeaViewController: FileAttachingViewController {
    
    class func makeOne(isIdea: Bool, defaultTheme: Theme?) -> CreateIdeaViewController {
        let new = UIStoryboard(name: "Ideas", bundle: nil).instantiateViewController(withIdentifier: "CreateIdeaViewController") as! CreateIdeaViewController
        new.isIdea = isIdea
        new.currentTheme = defaultTheme
        return new
    }

    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var txtViewDoneButton: UIButton!
    @IBOutlet weak var theme_label: UILabel?
    @IBOutlet weak var symbolsCountLabel: UILabel!
    @IBOutlet weak var textPlaceholder: UILabel!
    @IBOutlet weak var label: UILabel?
    
   
    
    @IBAction func txtViewDoneTapped(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    
    @IBOutlet weak var sendButton: UIButton? {
        didSet {
            sendButton?.setTitle((isIdea ? "Отправить идею" : "Отправить вопрос").uppercased(), for: .normal)
        }
    }
    

    
    var currentTheme: Theme? {
        didSet {
            theme_label?.text = (currentTheme?.title ?? (isIdea ? "Тема идеи" : "Тема вопроса")).replacingOccurrences(of: "\n", with: " ")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtView.delegate = self
        theme_label?.text = (currentTheme?.title ?? (isIdea ? "Тема идеи" : "Тема вопроса")).replacingOccurrences(of: "\n", with: " ")
        self.textPlaceholder.text = isIdea ? "Введите текст идеи..." : "Введите текст вопроса..."
        self.label?.text = isIdea ? "Текст идеи" : "Текст вопроса"
        
        self.title = isIdea ? "Предложить идею" : "Задать вопрос"
        self.addTapOutsideGestureRecognizer()
        
    }
    
    var isIdea: Bool = false
    
    
    @IBAction func pickTheme(_ sender: UIButton) {
        let picker = ThemesViewController.makeOne(inMode: .picker, delegate: self)
        picker.navigationItem.setLeftBarButtonItems([], animated: false)
        self.navigationController?.pushViewController(picker, animated: true)
    }
    
    
    @IBAction func send(_ sender: UIButton) {
        
        self.view.endEditing(true)
        guard !self.txtView.text.isEmpty else {showOKAlert(title: "Ошибка", message: "Текст сообщения не может быть пустым"); return }
        
        showHUD()
        
        if isIdea {
            Server.makeRequest.setIdea(self.txtView.text, for: currentTheme, completion: { (id) in
                if !self.hasFiles {
                    dismissHUD()
                    if id != nil {
                        self.showOKAlert(title: "Спасибо за Вашу идею! Она будет опубликована после модерации.", message: nil, from: self, tappedHandler: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                } else if id != nil {
                    Server.makeRequest.upload(images: self.attachedImages, files: self.attachedFiles, for: .ideas, id: id!, theme_id: self.currentTheme?.id, completion: { (success) in
                        dismissHUD()
                        if success {
                            self.showOKAlert(title: "Спасибо за Вашу идею! Она будет опубликована после модерации.", message: nil, from: self, tappedHandler: {
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                    })
                }
                
            })
        } else {
            Server.makeRequest.setQuestion(self.txtView.text, for: currentTheme, completion: { (id) in
                if !self.hasFiles {
                    dismissHUD()
                    
                    if id != nil {
                        self.showOKAlert(title: "Спасибо за Ваш вопрос! Он будет опубликован после модерации.", message: nil, from: self, tappedHandler: {
                            self.navigationController?.popViewController(animated: true)
                        })
                        
                    }
                } else if id != nil {
                    Server.makeRequest.upload(images: self.attachedImages, files: self.attachedFiles, for: .questions, id: id!, theme_id: self.currentTheme?.id, completion: { (success) in
                        dismissHUD()
                        
                        if success {
                            self.showOKAlert(title: "Спасибо за Ваш вопрос! Он будет опубликован после модерации.", message: nil, from: self, tappedHandler: {
                                self.navigationController?.popViewController(animated: true)
                            })
                            
                        }
                    })
                }
            })
        }
    }
    
    
    
    
    
}

extension CreateIdeaViewController: ThemePickerDelegate {
    func didPickTheme(theme: Theme) {
        currentTheme = theme
    }
}

extension CreateIdeaViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        self.symbolsCountLabel.text = "\(newText.count)/1000"
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.txtViewDoneButton.isHidden = false
        self.textPlaceholder.isHidden = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.txtViewDoneButton.isHidden = true
        self.textPlaceholder.isHidden = !textView.text.isEmpty
        
    }
}

