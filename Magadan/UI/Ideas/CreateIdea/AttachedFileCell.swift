//
//  AttachedFileCell.swift
//  Magadan
//
//  Created by Константин on 25/10/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class AttachedFileCell: UITableViewCell {

    @IBOutlet weak var showButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var extLabel: UILabel!
    
    private var showBlock: (()->Void)? = nil
    private var removeBlock: (()->Void)? = nil

    
    func update(with image: UIImage?=nil, url: URL?=nil, index: Int, onSnowTapped: @escaping ()->Void, onRemoveTapped: @escaping ()->Void) {
        nameLabel.text = image != nil ? "Изображение \(index + 1)" : url?.lastPathComponent
        showBlock = onSnowTapped
        removeBlock = onRemoveTapped
        extLabel.text = image != nil ? "JPG" : url?.pathExtension.uppercased()
    }
    
    @IBAction func showTapped(_ sender: Any) {
        showBlock?()
    }
    @IBAction func removeTapped(_ sender: Any) {
        removeBlock?()
    }
}
