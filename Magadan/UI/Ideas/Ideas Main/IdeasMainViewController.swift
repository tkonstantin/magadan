//
//  IdeasMainViewController.swift
//  Magadan
//
//  Created by Константин on 06.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class IdeasMainViewController: SearchableViewController, CustomAlertDelegate {
    
    class func makeOne(theme: Theme) -> IdeasMainViewController {
        let new = UIStoryboard(name: "Ideas", bundle: nil).instantiateViewController(withIdentifier: "IdeasMainViewController") as! IdeasMainViewController
        new.theme = theme
        return new
    }
    
    @IBOutlet weak var line_leading: NSLayoutConstraint!
    @IBOutlet weak var line: UIView!
    @IBOutlet weak var ideasButton: UIButton!
    @IBOutlet weak var questionsButton: UIButton!
    @IBOutlet weak var collectionItems: UICollectionView!
    
    var questions: [Idea] = []
    var ideas: [Idea] = []
    var isLoaded = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = theme.title?.replacingOccurrences(of: "\n", with: " ")
        loadItems()
    }
    
    var theme: Theme!
    
    
    
    
    enum modes {
        case questions
        case ideas
    }
    
    var currentMode: modes = .questions {
        didSet {
            guard currentMode != oldValue else { return }
            questionsButton.alpha = currentMode == .questions ? 1 : 0.5
            ideasButton.alpha = currentMode == .ideas ? 1 : 0.5
            line_leading.constant = currentMode == .questions ? 0 : self.view.bounds.width/2
            
            UIView.animate(withDuration: 0.25) {
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
            }
            
        }
    }
    
    var currentQuestionsPage: Int = 0
    var currentIdeasPage: Int = 0
    
    
    func loadItems() {
        showHUD()
        self.loadQuestions {
            dismissHUD()
            
            self.loadIdeas {
                dismissHUD()
                DispatchQueue.main.async {
                    self.isLoaded = true
                    self.collectionItems.reloadData()
                }
            }
        }
        
    }
    
    func loadQuestions(completion: @escaping ()->Void) {
        Server.makeRequest.getQuestions(page: currentQuestionsPage, for: self.theme, completion: { (result) in
            for question in result {
                if !self.questions.contains(where: {$0.id == question.id}) {
                    self.questions.append(question)
                }
            }
            completion()
        })
    }
    
    func loadIdeas(completion: @escaping ()->Void) {
        Server.makeRequest.getIdeas(page: currentIdeasPage, for: self.theme, completion: { (result) in
            for idea in result {
                if !self.ideas.contains(where: {$0.id == idea.id}) {
                    self.ideas.append(idea)
                }
            }
        })
        completion()
    }
    
    @IBAction func changeMode(_ sender: UIButton) {
        
        if sender == questionsButton {
            currentMode = .questions
        } else {
            currentMode = .ideas
        }
        
        self.isAnimatingTableOffset = true
        self.collectionItems.scrollToItem(at: IndexPath(item: currentMode == .questions ? 0 : 1, section: 0), at: .centeredHorizontally, animated: true)
        asyncAfter(milliseconds: 300) {
            self.isAnimatingTableOffset = false
        }
        
    }
    
    
    @IBAction func addIdea(_ sender: UIButton) {
        CustomAlertViewController.present(isIdeas: currentMode == .ideas, delegate: self, from: self)
    }
    
    
    func receptionTapped() {
        forceOpenReception(with: theme)
    }
    
    func okTapped() {
        guard UserDataManager.shared.isAuthorized else { self.showNeedsAuthAlert(); return }
        let create = CreateIdeaViewController.makeOne(isIdea: currentMode == .ideas, defaultTheme: self.theme)
        self.navigationController?.pushViewController(create, animated: true)
    }
    
    
    
    fileprivate var isAnimatingTableOffset = false
    
}

extension IdeasMainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, IdeasCellDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionItems.dequeueReusableCell(withReuseIdentifier: "tabCell", for: indexPath) as! IdeasTabCollectionViewCell
        cell.update(ideas: ideas, questions: questions, delegate: self, mode: indexPath.item == 0 ? .questions : .ideas, theme: theme, controller: self, isLoaded: self.isLoaded)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView == collectionItems && !isAnimatingTableOffset else { return }
        let currentPage = Int(scrollView.contentOffset.x/self.view.bounds.width)
        let mode: modes = currentPage == 0 ? .questions : .ideas
        
        guard self.currentMode != mode else { return }
        self.currentMode = mode
        
        asyncAfter {
            self.collectionItems.reloadData()
        }
        
        
    }
    
    func shouldLoadNextPage(isIdeas: Bool, theme: Theme, cell: IdeasTabCollectionViewCell) {
        if isIdeas {
            guard self.ideas.count == (currentIdeasPage + 1)*10 else { return }
            
            currentIdeasPage += 1
            self.loadIdeas {
                asyncAfter {
                    cell.update(ideas: self.ideas, questions: self.questions, delegate: self, mode: isIdeas ? .ideas : .questions, theme: theme, controller: self, isLoaded: self.isLoaded)
                }
            }
        } else {
            guard self.questions.count == (currentQuestionsPage + 1)*10 else { return }

            currentQuestionsPage += 1
            self.loadQuestions {
                asyncAfter {
                    cell.update(ideas: self.ideas, questions: self.questions, delegate: self, mode: isIdeas ? .ideas : .questions, theme: theme, controller: self, isLoaded: self.isLoaded)
                }
            }
        }
        
    }
}

extension IdeasMainViewController: IdeaControllerDelegate {
    func didUpdate(idea: Idea, isIdea: Bool) {
        if isIdea {
            if let index = self.ideas.index(where: {$0.id == idea.id}) {
                self.ideas[index] = idea
                asyncAfter {
                    self.collectionItems.reloadData()
                }
            }
        } else {
            if let index = self.questions.index(where: {$0.id == idea.id}) {
                self.questions[index] = idea
                asyncAfter {
                    self.collectionItems.reloadData()
                }
            }
        }
        
    }
}
