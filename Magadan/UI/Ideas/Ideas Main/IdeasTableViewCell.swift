//
//  IdeasTableViewCell.swift
//  Magadan
//
//  Created by Константин on 06.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class IdeasTableViewCell: UITableViewCell {

    @IBOutlet weak var date_label: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var watches_count: UILabel!
    @IBOutlet weak var likes_count: UILabel!
    @IBOutlet weak var isMy: UIImageView?
    @IBOutlet weak var likeButton: LikeButton!
    @IBOutlet weak var likeIcon: UIImageView!
    
    func update(with idea: Idea, path: IndexPath) {
        date_label.text = idea.date.parseDateShort(dots: true)
        title.text = idea.title
        watches_count.text = String(idea.watches_count)
        likes_count.text = String(idea.likes_count)
        likeIcon.tintColor = UIColor(hex: idea.isLiked ? "486fb4" : "999999")
        likeIcon.image = idea.isLiked ? #imageLiteral(resourceName: "like_icon") : #imageLiteral(resourceName: "like_icon_disabled")
        isMy?.isHidden = !idea.isMy
        likeButton.idea = idea
        likeButton.path = path
    }
    
    func update(with something: Something) {
        date_label.text = something.date.parseDateShort(dots: true)
        title.text = something.title
        isMy?.isHidden = !something.isMy
        watches_count.text = String(something.watchesCount)
    }
    

}
