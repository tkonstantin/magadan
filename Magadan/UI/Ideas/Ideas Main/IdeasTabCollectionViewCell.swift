//
//  IdeasTabCollectionViewCell.swift
//  Magadan
//
//  Created by Константин on 11.01.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

protocol IdeasCellDelegate: class {
    func shouldLoadNextPage(isIdeas: Bool, theme: Theme, cell: IdeasTabCollectionViewCell)
}

class IdeasTabCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var tableItems: UITableView!
    @IBOutlet weak var headerLabel: UILabel!
    
    fileprivate var questions: [Idea] = []
    fileprivate var ideas: [Idea] = []
    
    fileprivate weak var delegate: IdeasCellDelegate?
    fileprivate var currentMode: IdeasMainViewController.modes = .questions
    
    fileprivate var theme: Theme!
    
    var isLoaded = false

    
    fileprivate var currentIdeas: [Idea] {
        get {
            return currentMode == .questions ? questions : ideas
        }
    }
    
    override func awakeFromNib() {
        tableItems.contentInset.bottom = 96
    }
    
    fileprivate weak var controller: IdeasMainViewController?
    
    override func prepareForReuse() {
        self.isLoaded = false
        self.ideas = []
        self.questions = []
        theme = nil
        self.tableItems.reloadData()
    }
    
    func update(ideas i: [Idea], questions q: [Idea], delegate d: IdeasCellDelegate,  mode m: IdeasMainViewController.modes, theme t: Theme, controller c: IdeasMainViewController, isLoaded: Bool) {
        delegate = d
        ideas = i
        questions = q
        currentMode = m
        headerLabel.text = currentMode == .ideas ? "Идеи по выбранной теме\nотсутствуют" : "Вопросы по выбранной теме\nотсутствуют"
        theme = t
        controller = c
        self.isLoaded = isLoaded
        self.tableItems.reloadData()
    }
    
    
}

extension IdeasTabCollectionViewCell: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        tableView.tableHeaderView?.frame.size.height = (currentIdeas.count != 0 || !isLoaded) ? 0 : 128
        tableView.tableHeaderView?.isHidden = currentIdeas.count != 0 || !isLoaded
        return currentIdeas.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! IdeasTableViewCell
        cell.update(with: currentIdeas[indexPath.section], path: indexPath)
        cell.likeButton.addTarget(self, action: #selector(self.like(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc private func like(_ sender: LikeButton) {
        sender.isEnabled =  false
        if currentMode == .ideas {
            Server.makeRequest.likeIdea(sender.idea, completion: { (newLikesCount, isLiked, isReal) in
                if let index = self.currentIdeas.index(where: {$0.id == sender.idea.id}) {
                    self.currentIdeas[index].likes_count = newLikesCount
                    self.currentIdeas[index].isLiked = isLiked
                    asyncAfter {
                        sender.isEnabled = isReal
                        if self.tableItems.numberOfSections == self.numberOfSections(in: self.tableItems) {
                            self.tableItems.reloadRows(at: [sender.path], with: .none)
                        } else {
                            self.tableItems.reloadData()
                        }
                    }
                }
            })
        } else {
            Server.makeRequest.likeQuestion(sender.idea, completion: { (newLikesCount, isLiked, isReal) in
                if let index = self.currentIdeas.index(where: {$0.id == sender.idea.id}) {
                    self.currentIdeas[index].likes_count = newLikesCount
                    self.currentIdeas[index].isLiked = isLiked
                    asyncAfter {
                        sender.isEnabled = isReal
                        if self.tableItems.numberOfSections == self.numberOfSections(in: self.tableItems) {
                            self.tableItems.reloadRows(at: [sender.path], with: .none)
                        } else {
                            self.tableItems.reloadData()
                        }
                    }
                }
            })
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ideaVC = IdeaViewController.makeOne(with: currentIdeas[indexPath.section], theme_title: theme.title ?? "", isIdea: currentMode == .ideas, delegate: controller)
        controller?.navigationController?.pushViewController(ideaVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 127
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == tableView.numberOfSections - 3 {
            delegate?.shouldLoadNextPage(isIdeas: currentMode == .ideas, theme: theme, cell: self)
        }
    }
}
