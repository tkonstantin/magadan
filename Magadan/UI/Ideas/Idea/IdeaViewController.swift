//
//  IdeaViewController.swift
//  Magadan
//
//  Created by Константин on 14.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import SafariServices

protocol IdeaControllerDelegate: class {
    func didUpdate(idea: Idea, isIdea: Bool)
}

class IdeaViewController: SearchableViewController {

    class func makeOne(with idea: Idea, theme_title: String, isIdea: Bool, delegate: IdeaControllerDelegate?=nil) -> IdeaViewController {
        let new = UIStoryboard(name: "Ideas", bundle: nil).instantiateViewController(withIdentifier: "IdeaViewController") as! IdeaViewController
        new.idea = idea
        new.isIdea = isIdea
        new.theme_title = theme_title
        new.delegate = delegate
        return new
    }
    
    weak var delegate: IdeaControllerDelegate?
    fileprivate var idea: Idea! {
        didSet {
            asyncAfter {
                self.atttachmentsTableHeight?.constant = CGFloat(self.idea?.files.count ?? 0)*44
                self.view.setNeedsLayout()
                self.view.layoutIfNeeded()
                self.attachmentsTable?.reloadData()
            }
        }
    }
    fileprivate var theme_title: String!
    var isIdea: Bool = false
    
    
    @IBOutlet weak var atttachmentsTableHeight: NSLayoutConstraint!
    @IBOutlet weak var attachmentsTable: UITableView!
    @IBOutlet weak var imgContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var watchesCountLabel: UILabel!
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var body: UITextView!
    @IBOutlet weak var isAnsweredIcon: UIImageView!
    @IBOutlet weak var likeIcon: UIImageView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var commentsButton: UIButton!
    
    @IBAction func showImageTapped(_ sender: UIButton) {
        PhotoViewerViewController.present(with: [idea.image], navigationController: self.navigationController)
    }
    
    @IBAction func commentsTapped(_ sender: UIButton) {
        guard idea  != nil else { return }
        
        
        if idea.commentsCount == 0 {
            if !UserDataManager.shared.isAuthorized {
                showNeedsAuthAlert()
                return;
            }
            let create = CreateCommentViewController.make(with: idea)
            self.navigationController?.pushViewController(create, animated: true)
        } else {
            let list = CommentsListViewController.make(with: idea)
            self.navigationController?.pushViewController(list, animated: true)
        }
        

    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = theme_title
        fill()
        body.text = "Загрузка..."
        body.delegate = self
        loadAnswer()
    }
    
    func fill() {
        dateLabel.text = idea.date.parseDateShort(dots: true)
        watchesCountLabel.text = String(idea.watches_count)
        likesCountLabel.text = String(idea.likes_count)
        likeIcon.tintColor = UIColor(hex: idea.isLiked ? "486fb4" : "999999")
        likeIcon.image = idea.isLiked ? #imageLiteral(resourceName: "like_icon") : #imageLiteral(resourceName: "like_icon_disabled")
        titleLabel.text = idea.title
        body.attributedText = idea.answer.htmlToAttributedString()
        isAnsweredIcon.isHidden = !idea.isMy
        imgContainerHeight.constant = idea.hasImage ? 201 : 0
        imgView.sd_setImage(with: URL(string: idea.image))
        commentsButton?.setTitle(idea.commentsCount == 0 ? "ДОБАВИТЬ КОММЕНТАРИЙ" : "КОММЕНТАРИИ (\(idea.commentsCount))", for: .normal)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.delegate?.didUpdate(idea: self.idea, isIdea: self.isIdea)
    }
    
    @IBAction func like(_ sender: LikeButton) {
        sender.isEnabled = false
        if isIdea {
            Server.makeRequest.likeIdea(idea, completion: { (newLikesCount, isLiked, isReal) in
                self.idea.isLiked = isLiked
                self.idea.likes_count = newLikesCount
                self.delegate?.didUpdate(idea: self.idea, isIdea: true)
                self.likeIcon.tintColor = UIColor(hex: isLiked ? "486fb4" : "999999")
                self.likeIcon.image = isLiked ? #imageLiteral(resourceName: "like_icon") : #imageLiteral(resourceName: "like_icon_disabled")

                asyncAfter {
                    sender.isEnabled = isReal
                    self.likesCountLabel.text = String(newLikesCount)
                }
            })
        } else {
            Server.makeRequest.likeQuestion(idea, completion: { (newLikesCount, isLiked, isReal) in
                self.idea.isLiked = isLiked
                self.idea.likes_count = newLikesCount
                self.delegate?.didUpdate(idea: self.idea, isIdea: false)
                self.likeIcon.tintColor = UIColor(hex: isLiked ? "486fb4" : "999999")
                self.likeIcon.image = isLiked ? #imageLiteral(resourceName: "like_icon") : #imageLiteral(resourceName: "like_icon_disabled")
                asyncAfter {
                    sender.isEnabled = isReal
                    self.likesCountLabel.text = String(newLikesCount)
                }
                
            })
        }
    }
    
    
    func loadAnswer() {
        showHUD()
        if isIdea {
            Server.makeRequest.getFullIdea(idea, completion: { (fullIdea) in
                self.idea = fullIdea
                dismissHUD()
                asyncAfter {
                    self.fill()
                }
            })
        } else {
            Server.makeRequest.getFullQuestion(idea, completion: { (fullQuestion) in
                self.idea = fullQuestion
                dismissHUD()
                asyncAfter {
                    self.fill()
                }
            })
        }
    }
    
}

extension IdeaViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        let urlString = URL.absoluteString
        guard urlString.hasPrefix("http") else { return true }
        let sf = SFSafariViewController(url: URL, entersReaderIfAvailable: true)
        UIApplication.shared.keyWindow?.rootViewController?.present(sf, animated: true, completion: nil)
        return false
    }
}


extension IdeaViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return idea?.files.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AttachedFileCell
        let url = URL(string: idea.files[indexPath.row])
        cell.update(with: nil, url: url, index: indexPath.row, onSnowTapped: {
            asyncAfter {
                if url != nil {
                    let sf = SFSafariViewController(url: url!)
                    self.present(sf, animated: true, completion: nil)
                }
            }
            
        }, onRemoveTapped: { })
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 44
    }
}
