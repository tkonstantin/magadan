//
//  AboutViewController.swift
//  Magadan
//
//  Created by Константин on 30.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class AboutViewController: RotatableViewController {

    class func makeOne() -> AboutViewController {
        return (UIStoryboard(name: "About", bundle: nil).instantiateInitialViewController() as! UINavigationController).viewControllers.first as! AboutViewController
    }

    @IBOutlet weak var versionLabel: UILabel! {
        didSet {
            versionLabel.text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.0"
        }
    }
}
