//
//  AccountStuffViewController.swift
//  Magadan
//
//  Created by Константин on 15.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class Something {
    var title = ""
    var date = Date()
    var isMy = false
    var id = ""
    var _idea: Idea?
    var _theme_id: String = ""
    var theme_title: String = ""
    var watchesCount = 0
    
    
    init(with json: NSDictionary) {
        title = json["TEXT"] as? String ?? ""
        date = Date.init(dateFromVeryStrangeString: json["DATE"] as? String ?? "00000000000000")
        isMy = json["userCreate"] as? Bool ?? false
        id = json["ID"] as? String ?? String(json["ID"] as? Int ?? 0)
        theme_title = json["theme_title"] as? String ?? ""
        watchesCount = json["views"] as? Int ?? Int(json["views"] as? String ?? "0") ?? 0
    }
    
    
    init(with idea: Idea) {
        title = idea.title
        date = idea.date
        isMy = idea.isMy
        id = String(idea.id)
        _idea = idea
        _theme_id = idea.theme_id
        theme_title = idea.theme_title
        watchesCount = idea.watches_count
    }
    
}

extension Array where Element: Something {
    init(with ideas: [Idea]) {
        self = ideas.map({Something(with: $0)}) as! [Element]
    }
    
}

class AccountStuffViewController: RotatableViewController {
    
    enum modes {
        case ideas
        case questions
        case treatments
    }

    class func makeOne(items: [Something]=[], ideas: [Idea]=[], mode: AccountStuffViewController.modes) -> AccountStuffViewController {
        let new = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "AccountStuffViewController") as! AccountStuffViewController
        new.items = items + [Something](with: ideas)
        new.mode = mode
        return new
        
    }
    
    var mode: modes = .ideas
    var items: [Something] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        self.title = mode ==  .ideas ? "Мои идеи" : mode == .questions ? "Мои вопросы" : "Мои обращения"
    }
}

extension AccountStuffViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! IdeasTableViewCell
        cell.update(with: items[indexPath.section])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch mode {
        case .treatments: break;
        default:
            let ideaVC =  IdeaViewController.makeOne(with: items[indexPath.section]._idea!, theme_title: items[indexPath.section].theme_title, isIdea: mode == .ideas, delegate: self)
            self.navigationController?.pushViewController(ideaVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 127
    }
    
    
}

extension AccountStuffViewController: IdeaControllerDelegate {
    func didUpdate(idea: Idea, isIdea: Bool) {
        if let index = self.items.index(where: {$0._idea!.id == idea.id}) {
            self.items[index]._idea = idea
            self.tableView.reloadData()
        }
    }
    
    
}
