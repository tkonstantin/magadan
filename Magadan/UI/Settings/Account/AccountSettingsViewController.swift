//
//  AccountSettingsViewController.swift
//  Magadan
//
//  Created by Константин on 30.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class AccountSettingsViewController: UITableViewController {
    
    @IBOutlet weak var lastnameCell: ReceptionFieldTableViewCell!
    @IBOutlet weak var nameCell: ReceptionFieldTableViewCell!
    @IBOutlet weak var middlenameCell: ReceptionFieldTableViewCell!
    @IBOutlet weak var index_cityCell: ReceptionFieldTableViewCell!
    @IBOutlet weak var region_discrictCell: ReceptionFieldTableViewCell!
    @IBOutlet weak var streetCell: ReceptionFieldTableViewCell!
    @IBOutlet weak var building_block_flatCell: ReceptionFieldTableViewCell!
    @IBOutlet weak var email_phoneCell: ReceptionFieldTableViewCell!
    
    
    
    
    @IBOutlet weak var saveButton: UIButton!
    
    var availableDistricts: [District] {
        get {
            return Server.makeRequest.availableProfileDistricts
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showHUD()
        Server.makeRequest.getAvailableDistricts {
            dismissHUD()
            self.fillWithCurrentUser()
            self.configureCells()
        }
    }
    
    
    
    
    func fillWithCurrentUser() {
        let user = UserDataManager.shared.currentUser
        values[.lastname] = user?.lastName
        values[.name] = user?.firstName
        values[.middlename] = user?.middleName
        values[.index] = user?.index
        values[.city] = user?.city
        values[.region] = Server.makeRequest.availableRegions.first(where: {$0.id == user?.region})?.title
        values[.raw_region] = user?.region
        values[.raw_district] = user?.district
        values[.district] = values[.custom_district_string] ?? availableDistricts.first(where: {$0.id == values[.raw_district]})?.title ?? user?.custom_district_string
        values[.street] = user?.street
        values[.building] = user?.building
        values[.block] = user?.block
        values[.flat] = user?.flat
        values[.email] = user?.email
        values[.phone] = user?.phone
        
    }
    
    
    func configureCells() {
        update(lastnameCell, with: .lastname)
        update(nameCell, with: .name)
        update(middlenameCell, with: .middlename)
        update(index_cityCell, with: .index)
        update(index_cityCell, with: .city, tag: 2)
        update(region_discrictCell, with: .region)
        update(region_discrictCell, with: .district, tag: 2)
        update(streetCell, with: .street)
        update(building_block_flatCell, with: .building)
        update(building_block_flatCell, with: .block, tag: 2)
        update(building_block_flatCell, with: .flat, tag: 3)
        update(email_phoneCell, with: .email)
        update(email_phoneCell, with: .phone, tag: 2)
    }
    
    
    
    @IBAction func saveTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        
        
        guard let user = UserDataManager.shared.currentUser else { return }
        user.custom_district_string = nil
        user.district = nil
        
        showHUD()
        
        for value in values.enumerated() {
            switch value.element.key {
            case .lastname: user.lastName = value.element.value
            case .name: user.firstName = value.element.value
            case .middlename: user.middleName = value.element.value
            case .index: user.index = value.element.value
            case .city: user.city = value.element.value
            case .raw_region: user.region = value.element.value
            case .raw_district: user.district = value.element.value
            case .custom_district_string: user.custom_district_string = value.element.value
            case .street: user.street = value.element.value
            case .building: user.building = value.element.value
            case .block: user.block = value.element.value
            case .flat: user.flat = value.element.value
            case .email: user.email = value.element.value
            case .phone: user.phone = value.element.value
            default: break;
            }
        }
        UserDataManager.shared.currentUser = user
        
        Server.makeRequest.setCurrentUser(completion: {
            dismissHUD()
        })
        
    }
    
    var activeFieldType: ReceptionRegField.fieldTypes?
    var values: [ReceptionRegField.fieldTypes: String] = [:]
    
    
    
}

extension AccountSettingsViewController: FieldCellDelegate {
    func fieldCellDidEndEditing(with regField: ReceptionRegField, group: ReceptionRegFieldGroup) {
        if regField.type == .district {
            values[.district] = nil
            values[.raw_district] = nil
            values[.custom_district_string] = regField.value
        } else {
            values[regField.type] = regField.value
        }
        activeFieldType = nil
    }
    
    func fieldCellDidReturn(with regField: ReceptionRegField, group: ReceptionRegFieldGroup) {
        activeFieldType = nil
    }
    
    func fieldCellDidStartTyping(with regField: ReceptionRegField, group: ReceptionRegFieldGroup) {
        activeFieldType = regField.type
    }
    
    func fieldCellShouldBeginEditing(with regField: ReceptionRegField, group: ReceptionRegFieldGroup) -> Bool {
        if regField.type == .district && self.values[.raw_region] == Region.magadan_id {
            self.view.endEditing(true)
            self.navigationController?.pushViewController(SimplePickerViewController.makeOne(type: .districts, needsClearButton: self.values[.district] != nil, delegate: self, items: Server.makeRequest.availableProfileDistricts), animated: true)
            return false
        } else if regField.type == .region {
            self.view.endEditing(true)
            self.navigationController?.pushViewController(SimplePickerViewController.makeOne(type: .regions, needsClearButton: self.values[.region] != nil, delegate: self, items: Server.makeRequest.availableRegions), animated: true)
            return false
        } else if regField.type == .name || regField.type == .lastname || regField.type == .middlename {
            return false
        }
        return true
    }
}

extension AccountSettingsViewController: SimplePickerDelegate {
    func didPicked(item _item: SimplePickerObject?, type: SimplePickerViewController.picker_types) {
        guard let item = _item else {
            if type == .regions {
                self.values[.region] = nil
                self.values[.raw_region] = nil
                self.values[.raw_district] = nil
                self.values[.district] = nil
                self.values[.custom_district_string] = nil
            } else {
                self.values[.raw_district] = nil
                self.values[.district] = nil
                self.values[.custom_district_string] = nil
            }
            configureCells()
            return;
        }
        
        if item is Region {
            if self.values[.region] != item.title || self.values[.raw_region] != item.id {
                self.values[.region] = item.title
                self.values[.raw_region] = item.id
                self.values[.raw_district] = nil
                self.values[.district] = nil
                self.values[.custom_district_string] = nil
            }
        } else {
            self.values[.raw_district] = item.id
            self.values[.district] = availableDistricts.first(where: {$0.id == values[.raw_district]})?.title ?? values[.custom_district_string]
            self.values[.custom_district_string] = nil
        }
        configureCells()
    }
    
    func update(_ cell: ReceptionFieldTableViewCell, with type: ReceptionRegField.fieldTypes, tag: Int=1) {
        cell.update(with: ReceptionRegField(type), value: self.values[type], group: ReceptionRegFieldGroup([ReceptionRegField(type)]), isActive: self.activeFieldType == type, isFilled: self.values[type] != nil && !self.values[type]!.isEmpty, delegate: self, tag: tag)
    }
    
    
}






