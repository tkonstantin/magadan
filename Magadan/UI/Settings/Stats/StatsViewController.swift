//
//  StatsViewController.swift
//  Magadan
//
//  Created by Константин on 25.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class StatsViewController: UITableViewController {

    @IBOutlet weak var questions_now_answered: UILabel!
    @IBOutlet weak var questions_answered: UILabel!
    @IBOutlet weak var ideas_not_answered: UILabel!
    @IBOutlet weak var ideas_answered: UILabel!
    @IBOutlet weak var treatments: UILabel!
    @IBOutlet weak var votes: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fillWithCurrentUser()
        
        Server.makeRequest.getCurrentUser {
            self.fillWithCurrentUser()
        }
    }
    
    func fillWithCurrentUser() {
        let user = UserDataManager.shared.currentUser

        questions_now_answered.text = String((user?.questions_answered ?? 0) + (user?.questions_not_answered ?? 0))
        questions_answered.text = String(user?.questions_answered ?? 0)
        ideas_not_answered.text = String((user?.ideas_not_answered ?? 0) + (user?.ideas_answered ?? 0))
        ideas_answered.text = String(user?.ideas_answered ?? 0)
        treatments.text = String(user?.treatments ?? 0)
        votes.text = String(user?.votes ?? 0)
    }
    
    @IBAction func showQuestions(_ sender: UIButton) {
        let user = UserDataManager.shared.currentUser
        guard (user?.questions_answered ?? 0) + (user?.questions_not_answered ?? 0) != 0 else { return }
        
        showHUD()
        Server.makeRequest.getMyQuestions { (questions) in
            asyncAfter {
                dismissHUD()
                let asf =  AccountStuffViewController.makeOne(ideas: questions, mode: .questions)
                self.navigationController?.pushViewController(asf, animated: true)
            }
        }
    }
    
    @IBAction func showIdeas(_ sender: UIButton) {
        let user = UserDataManager.shared.currentUser
        guard (user?.ideas_answered ?? 0) + (user?.ideas_not_answered ?? 0) != 0 else { return }
        
        showHUD()
        Server.makeRequest.getMyIdeas { (ideas) in
            asyncAfter {
                dismissHUD()
                let asf =  AccountStuffViewController.makeOne(ideas: ideas, mode: .ideas)
                self.navigationController?.pushViewController(asf, animated: true)
            }
        }
    }
    
    @IBAction func showTreatments(_ sender: UIButton) {
        let user = UserDataManager.shared.currentUser
        guard (user?.treatments ?? 0) != 0 else { return }
        
        showHUD()
        Server.makeRequest.getMyTreatments { (treatments) in
            asyncAfter {
                dismissHUD()
                let asf =  AccountStuffViewController.makeOne(items: treatments, mode: .treatments)
                self.navigationController?.pushViewController(asf, animated: true)
            }
        }
    }
    
    @IBAction func showVotes(_ sender: UIButton) {
        let user = UserDataManager.shared.currentUser
        guard (user?.votes ?? 0) != 0 else { return }
        
        showHUD()
        Server.makeRequest.getMyVotes { (votes) in
            asyncAfter {
                dismissHUD()
                let asf = PollsListViewController.makeOne(with: votes)
                self.navigationController?.pushViewController(asf, animated: true)
            }
        }
        
    }
}
