//
//  NotificationSettingsViewController.swift
//  Magadan
//
//  Created by Константин on 30.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import SVProgressHUD

class NotificationSettingsViewController: RotatableViewController {
    
    
    var titles: [String] = ["Новости", "Мероприятия", "Трансляции", "Опросы"]
    
    
    @IBOutlet weak var pushSwitch: UISwitch!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pushSwitch.isOn = Server.pushSettings.isEnabled
        tableView.delegate = self
        tableView.dataSource = self
        
        update()
    }
    
    
    func update() {
        SVProgressHUD.show()
        Server.makeRequest.getPushSettings {
            self.pushSwitch.isOn = Server.pushSettings.isEnabled
            self.tableView.reloadData()
            SVProgressHUD.dismiss()
        }
        
    }
    
    
    func getNotificationMode(for row: Int) -> PushSettings.states {
        switch row {
        case 2: return .important
        case 3: return .never
        default: return .all
        }
    }
    
    
    @IBAction func pushSwitchChanged(_ sender: UISwitch) {
        Server.pushSettings.isEnabled = sender.isEnabled
    }
}


extension NotificationSettingsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "header")!
            (cell.viewWithTag(1) as? UILabel)?.text = titles[indexPath.section]
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! NotificationCell
            cell.label.text = getNotificationMode(for: indexPath.row).label
            cell.checkmark_image.image = pushState(for: indexPath.section) == getNotificationMode(for: indexPath.row) ? #imageLiteral(resourceName: "notification_filled")  : #imageLiteral(resourceName: "notifications_empty")
            cell.checkmark_button.indexPath = indexPath
            cell.checkmark_button.addTarget(self, action: #selector(self.checkmarkTapped(_:)), for: .touchUpInside)
            return cell
        }
    }
    
    func pushState(for section: Int) -> PushSettings.states  {
        switch section {
        case 0: return Server.pushSettings.news
        case 1: return Server.pushSettings.events
        case 2: return Server.pushSettings.live
        default: return Server.pushSettings.vote
        }
    }
    
    @objc private func checkmarkTapped(_ sender: NotififationCheckmarkButton) {
        SVProgressHUD.show()
        self.view.isUserInteractionEnabled = false
        
        let settings = Server.pushSettings
        
        switch sender.indexPath.section {
            case 0: settings.news = getNotificationMode(for: sender.indexPath.row)
            case 1: settings.events = getNotificationMode(for: sender.indexPath.row)
            case 2: settings.live = getNotificationMode(for: sender.indexPath.row)
            case 3: settings.vote = getNotificationMode(for: sender.indexPath.row)
            default: break;
        }
        
        Server.pushSettings = settings
        
        Server.makeRequest.setPushSettings { (success) in
            self.tableView.reloadData()
            SVProgressHUD.dismiss()
            self.view.isUserInteractionEnabled = true
        }
        
    }
}


class NotififationCheckmarkButton: UIButton {
    var indexPath: IndexPath!
}
