//
//  NotificationCell.swift
//  Magadan
//
//  Created by Константин on 30.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var checkmark_image: UIImageView!
    @IBOutlet weak var checkmark_button: NotififationCheckmarkButton!
    @IBOutlet weak var label: UILabel!
    

}
