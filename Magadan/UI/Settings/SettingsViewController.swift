//
//  SettingsViewController.swift
//  Magadan
//
//  Created by Константин on 22.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {


    
    
    class func makeOne() -> SettingsViewController {
        return (UIStoryboard(name: "Settings", bundle: nil).instantiateInitialViewController() as! UINavigationController).viewControllers.first as! SettingsViewController
    }
    
    @IBOutlet weak var esia_img: UIImageView? {
        didSet {
            esia_img?.image = esia_img?.image?.withRenderingMode(.alwaysTemplate).imageWithColor(UIColor(hex: "666666"))
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UserDataManager.shared.isAuthorized ? 52 : 0
        } else  if indexPath.row == 3 {
            return UserDataManager.shared.isAuthorized ? 0 : 52
        } else if indexPath.row == 1 {
            return UserDataManager.shared.isAuthorized ? 52 : 0
        } else {
            return 52
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 3 && !UserDataManager.shared.isAuthorized  {
            Server.makeRequest.logout()
        }
    }
}
