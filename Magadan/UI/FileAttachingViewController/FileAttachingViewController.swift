//
//  FileAttachingViewController.swift
//  Magadan
//
//  Created by Константин on 25/10/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import UIKit
import QuickLook

class PreviewItem: NSObject, QLPreviewItem {
    var previewItemURL: URL?
    var previewItemTitle: String?
}


class FileAttachingViewController: SearchableViewController {
    
    @IBOutlet weak var attachButton: UIButton?
    @IBOutlet weak var attachesTableView: UITableView!
    
    var picker: FilePicker!
    
    
    var previewItem: PreviewItem?
    
    var attachedImages: [UIImage] = [] {
        didSet {
            asyncAfter {
                self.attachButton?.isHidden = (self.attachedImages.count + self.attachedFiles.count) >= 3
            }
        }
    }
    
    var attachedFiles: [URL] = [] {
        didSet {
            asyncAfter {
                self.attachButton?.isHidden = (self.attachedImages.count + self.attachedFiles.count) >= 3
            }
        }
    }
    
    var hasFiles: Bool {
        get {
            return self.attachedImages.count != 0 || self.attachedFiles.count == 0
        }
    }
    
    @IBAction func attachFile(_ sender: UIButton) {
        picker = FilePicker()
        picker.showPicker(source: sender, from: self, onImagePicked: { [weak self] (img) in
            if img != nil {
                self?.attachedImages.append(img!)
            }
            asyncAfter {
                self?.attachesTableView?.reloadSections([0], with: .automatic)
            }
            }, onFilePicked: { [weak self] (url) in
                if url != nil {
                    self?.attachedFiles.append(url!)
                }
                asyncAfter {
                    self?.attachesTableView?.reloadSections([0], with: .automatic)
                }
        })
    }
    
}



extension FileAttachingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attachedImages.count + attachedFiles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AttachedFileCell
        var currentObject: Any!
        
        var index: Int!
        if indexPath.row < attachedImages.count {
            index = indexPath.row
            currentObject = attachedImages[index]
        } else {
            index = indexPath.row - attachedImages.count
            currentObject = attachedFiles[index]
        }
        cell.update(with: currentObject as? UIImage, url: currentObject as? URL, index: index, onSnowTapped: {
            if let url = currentObject as? URL {
                let item = PreviewItem()
                item.previewItemURL = url
                item.previewItemTitle = url.lastPathComponent
                self.previewItem = item
                
                let preview = QLPreviewController.init()
                preview.dataSource = self
                self.present(preview, animated: true, completion: nil)
            } else if let img = currentObject as? UIImage {
                PhotoViewerViewController.present(images: [img], navigationController: self.navigationController)
            }
            
            //TODO: - open system file viewer
        }, onRemoveTapped: {
            if currentObject is UIImage {
                self.attachedImages.remove(at: index)
            } else {
                self.attachedFiles.remove(at: index)
            }
            asyncAfter {
                self.attachesTableView?.isUserInteractionEnabled = false
                self.attachesTableView?.deleteRows(at: [indexPath], with: .left)
                asyncAfter(milliseconds: 300, {
                    self.attachesTableView?.reloadData()
                    self.attachesTableView?.isUserInteractionEnabled = true
                    
                })
            }
        })
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}



extension FileAttachingViewController: QLPreviewControllerDelegate, QLPreviewControllerDataSource {
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return previewItem == nil ? 0 : 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return previewItem!
    }
    
    
}
