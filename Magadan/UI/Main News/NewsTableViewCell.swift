//
//  NewsTableViewCell.swift
//  Magadan
//
//  Created by Константин on 19.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import SDWebImage


class LikeButton: UIButton {
    var idea: Idea!
    var news: News!
    var path: IndexPath!
}

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel?
    @IBOutlet weak var date_bg: UIImageView?
    @IBOutlet weak var timelabel: UILabel?
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var activity: UIActivityIndicatorView?
    @IBOutlet weak var likesCountLabel: UILabel?
    @IBOutlet weak var likeButton: LikeButton?
    @IBOutlet weak var likeIcon: UIImageView?
    
    let bannerColor = UIColor(hex: "A1B2D1")
    
    
    override func prepareForReuse() {
        activity?.startAnimating()
    }
    
    func update(with news: News, path: IndexPath) {
        dateLabel?.text = news.date?.parseDateShort()
        timelabel?.text = news.date?.parseTime()
        likesCountLabel?.text = String(news.likesCount)
        likeButton?.news = news
        likeButton?.path = path
        likeIcon?.tintColor =  UIColor(hex: news.isLiked ? "486fb4" : "999999")
        likeIcon?.image = news.isLiked ? #imageLiteral(resourceName: "like_icon") : #imageLiteral(resourceName: "like_icon_disabled")
        newsTitleLabel.text = news.title
        icon?.sd_setImage(with: URL(string: news.smallestImage))
//        self.borderWidth = news.isBanner ? 4 : 0
//        self.borderColor = news.isBanner ? bannerColor : .white
        self.date_bg?.image = self.date_bg!.image?.withRenderingMode(.alwaysTemplate)
        self.date_bg?.tintColor = news.isBanner ? bannerColor : UIColor(hex: "E2E2E2")
        self.dateLabel?.textColor = news.isBanner ? .white : UIColor(hex: "5A5A5A")
    }

}
