//
//  MainViewController.swift
//  Magadan
//
//  Created by Константин on 17.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import SideMenu


class MainViewController: SearchableViewController {
    
    class func makeOne() -> MainViewController {
        let main = (UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as! UINavigationController).viewControllers.first as! MainViewController
        return main
    }

    var activeCollectionItem: Int = 0
    var loadingCategory: Int?
    var loadedCategories: Set<Int> = []
    var endedCategories: Set<Int> = []


   
    var categories: [NewsCategory] {
        get {
            return Server.makeRequest.newsCategories
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tabsCollection: UICollectionView!
    fileprivate var isAnimatingTableOffset = false
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureMenu()
        configureColleciton()
        
        SideMenuManager.default.menuLeftNavigationController = self.storyboard?.instantiateViewController(withIdentifier: "SideMenuController") as? UISideMenuNavigationController
        

    }
    
    
    @objc override func reloadScreen() {
        self.tabsCollection.reloadData()
        self.collectionView.reloadData()
    }

    
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .default
        }
    }
    
    func configureMenu() {
        SideMenuManager.default.menuWidth = self.view.frame.size.width-69
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuShadowColor = .black
        SideMenuManager.default.menuShadowRadius = 18
        SideMenuManager.default.menuShadowOpacity = 0.5
        
        if let nav = self.navigationController {
            SideMenuManager.default.menuAddPanGestureToPresent(toView: nav.navigationBar)
            SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: nav.view)
            SideMenuManager.default.menuFadeStatusBar = false
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if segue.identifier == "showMenu" {
            currentPath = IndexPath(row: 0, section: 0)
        }
    }
    
}





extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func configureColleciton() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "CategoryCell", bundle: nil), forCellWithReuseIdentifier: "cell")
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard collectionView == tabsCollection else {
            self.isAnimatingTableOffset = true

            asyncAfter {
                self.activeCollectionItem = indexPath.item
                self.collectionView.reloadData()
                self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                self.tabsCollection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                
                asyncAfter(milliseconds: 300, {
                    self.isAnimatingTableOffset = false

                })
            }

            return
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard collectionView == tabsCollection else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCell
            cell.update(with: categories[indexPath.item].title, isActive: activeCollectionItem == indexPath.item)
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tabCell", for: indexPath) as! TabsCollectionViewCell
        cell.update(with: categories[indexPath.item].id, delegate: self)
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard collectionView == tabsCollection else {
            return CGSize(width: categories[indexPath.item].title.requiredWidth(12, font: UIFont.systemFont(ofSize: 12, weight: .medium), height: 56) + 16, height: collectionView.bounds.height)
        }
        
        return collectionView.bounds.size
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView == tabsCollection && !isAnimatingTableOffset else { return }
        let currentPage = Int(scrollView.contentOffset.x/self.view.bounds.width)
        
        guard self.activeCollectionItem != currentPage else { return }
        self.activeCollectionItem = currentPage
        
        asyncAfter {
            self.collectionView.reloadData()
            self.collectionView.scrollToItem(at: IndexPath(item: currentPage, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
    func canLoadMore(categoryID: Int) -> Bool {
        let specifiedNewsArr = (Server.makeRequest.savedNewsForCategory[categoryID] ?? []).filter({!$0.isBanner})
        return ((specifiedNewsArr.count % Server.makeRequest.pageSize == 0) || !self.loadedCategories.contains(categoryID)) && !self.endedCategories.contains(categoryID)
    }
    
    func shouldLoadMore(categoryID: Int, cell: TabsCollectionViewCell) {
        guard loadingCategory != categoryID else { return }
        loadingCategory = categoryID
        let specifiedNewsArr = (Server.makeRequest.savedNewsForCategory[categoryID] ?? []).filter({!$0.isBanner})
        let hasNext = ((specifiedNewsArr.count % Server.makeRequest.pageSize == 0) || !self.loadedCategories.contains(categoryID)) && !self.endedCategories.contains(categoryID)
        if hasNext {
            let nextPage = specifiedNewsArr.count/Server.makeRequest.pageSize
            Server.makeRequest.getNews(page: nextPage, cat_id: categoryID, completion: { (loadedNews) in
                if loadedNews.count == 0 {
                    self.endedCategories.insert(categoryID)
                }
                for news in loadedNews {
                    if !(Server.makeRequest.savedNewsForCategory[categoryID] ?? []).contains(where: {$0.id == news.id}) {
                        if Server.makeRequest.savedNewsForCategory[categoryID] == nil {
                            Server.makeRequest.savedNewsForCategory[categoryID] = [news]
                        } else {
                            Server.makeRequest.savedNewsForCategory[categoryID]!.append(news)
                        }
                    }
                }
                self.loadedCategories.insert(categoryID)
                self.loadingCategory = nil
                cell.newsReceived()
            })
        } else {
            self.loadedCategories.insert(categoryID)
            self.loadingCategory = nil
            cell.newsReceived()
        }
    }    
}


extension MainViewController: TabsCellDelegate {
    
    func didSelect(news: News,  cell: NewsTableViewCell, tabCell: TabsCollectionViewCell) {
        let next = NewsViewController.makeOne(news: news)
        next.delegate = tabCell
        self.navigationController?.pushViewController(next, animated: true)
        Server.makeRequest.getNewsMore(news, completion: { (updatedNews) in
            guard let upd = updatedNews else { return }
            next.update(with: upd)
            if let index = tabCell.news.index(where: {$0.id == news.id}) {
                tabCell.news[index] = news
                asyncAfter {
                    tabCell.tableItems.reloadData()
                }
            }
        })
    }
}

extension Array where Element: News {
    mutating func appendSafely(_ newElement: Element) {
        if !self.contains(where: {$0.id == newElement.id}) {
            self.append(newElement)
        } else if newElement.isBanner {
            if let index = self.index(where: {$0.id == newElement.id}) {
                self[index] = newElement
            }
        }
        
    }
    
    mutating func appendSafely(_ newArr: [Element]) {
        for newElement in newArr {
            self.appendSafely(newElement)
        }
    }
}
