//
//  VideoView.swift
//  Magadan
//
//  Created by Константин on 28.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit


class PlayButton: UIButton {
    
    func hide() {
        self.setImage(nil, for: .normal)
        self.backgroundColor = .clear
    }
    
    func show() {
        self.setImage(#imageLiteral(resourceName: "playButton"), for: .normal)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.33)
    }
}

class VideoView: UIView {
    
    func set(urlString str: String?) -> Bool {
        guard str != nil else { return false }
        urlString = str
        url = URL(string: urlString)
        if url != nil {
            initPlayer()
        }
        return url != nil
    }
    
    private var playerController: AVPlayerViewController!
    private var player: AVPlayer?
    private var urlString: String!
    private var url: URL!

    private func initPlayer() {
        
        let playerItem = AVPlayerItem(url: url)
        player = AVPlayer(playerItem: playerItem)
        
        playerController = AVPlayerViewController()
        playerController.player = player
        
        playerController.allowsPictureInPicturePlayback = true
        playerController.showsPlaybackControls = true

        if #available(iOS 10.0, *) {
            if #available(iOS 11.0, *) {
                playerController.exitsFullScreenWhenPlaybackEnds = true
            }
        }
        
        self.addSubview(playerController.view)
        playerController.view.frame = self.bounds

        
    }
}
