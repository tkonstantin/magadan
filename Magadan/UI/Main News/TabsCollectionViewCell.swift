//
//  TabsCollectionViewCell.swift
//  Magadan
//
//  Created by Константин on 10.01.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import PullToRefresh


protocol TabsCellDelegate: class {
    func didSelect(news: News, cell: NewsTableViewCell, tabCell: TabsCollectionViewCell)
    func shouldLoadMore(categoryID: Int, cell: TabsCollectionViewCell)
    func canLoadMore(categoryID: Int) -> Bool
}

class TabsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var tableItems: UITableView!
    fileprivate weak var delegate: TabsCellDelegate?
    
    fileprivate var refresher = PullToRefresh()
    
    var news: [News] {
        get {
            return Server.makeRequest.savedNewsForCategory[currentCategoryId] ?? []
        } set {
            Server.makeRequest.savedNewsForCategory[currentCategoryId] = newValue
        }
    }
    
    override func prepareForReuse() {
        pinnedNews = []
        configureTable()
    }
    

    
    
    func update(with categoryID: Int, delegate: TabsCellDelegate) {
        self.delegate = delegate
        self.configureTable()
        self.currentCategoryId = categoryID
        self.loadPinned()
        self.resetPaging()
    }
    
    
    private var currentCategoryId: Int = 0
    
    var footerView: UIView {
        get {
            let footer = UIView(frame: CGRect(origin: .zero, size: CGSize(width: self.bounds.width, height: 64)))
            let label = UILabel(frame: CGRect(x: 0, y: 32, width: self.bounds.width, height: 32))
            label.text = "Загрузка..."
            label.textAlignment = .center
            label.textColor = .darkGray
            footer.addSubview(label)
            let activity = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: 44))
            activity.activityIndicatorViewStyle = .gray
            activity.color = blueColor
            activity.startAnimating()
            footer.addSubview(activity)
            return footer
        }
    }
    
    
    
    
    func resetPaging() {
        self.pinnedNews = []
        self.tableItems.reloadData()
        loadNews()
    }
    
    var loadNewsCompletion: (()->Void)?
    
    func loadNews(completion: (()->Void)?=nil) {
        loadNewsCompletion = completion
        guard (delegate?.canLoadMore(categoryID: self.currentCategoryId) ?? false) else { loadNewsCompletion?(); return }
        delegate?.shouldLoadMore(categoryID: self.currentCategoryId, cell: self)
        self.tableItems.tableFooterView = footerView
    }
    
    
    func newsReceived() {
        asyncAfter {
            self.loadNewsCompletion?()
            self.tableItems.tableFooterView = UIView()
            self.tableItems.reloadData()
        }
    }
    
    var pinnedNews: [News] = []
    
    func loadPinned() {
        guard currentCategoryId == 0 else { return }
        
        Server.makeRequest.getPinnedNews(completion: { (result) in

            self.pinnedNews = result
            if self.currentCategoryId == 0 {
                var temp = result
                temp.appendSafely(self.news)
                self.news = temp
            }
            asyncAfter {
                self.tableItems.reloadData()
            }
        })
    }
}


extension TabsCollectionViewCell: NewsLikesDelegate {
    func didUpdate(news: News) {
        if let index = self.news.index(where: {$0.id == news.id}) {
            self.news[index] = news
            asyncAfter {
                self.tableItems.reloadData()
            }
        }
    }
}

extension TabsCollectionViewCell: UITableViewDelegate, UITableViewDataSource {
    
    func configureTable() {
        tableItems.delegate = self
        tableItems.dataSource = self
        tableItems.tableFooterView = UIView()
        tableItems.estimatedRowHeight = 200
        tableItems.rowHeight = UITableViewAutomaticDimension
        
        tableItems.addPullToRefresh(refresher) {
            asyncAfter(milliseconds: 300, {
                self.loadPinned()
                self.loadNews {
                    self.tableItems.endAllRefreshing()
                }
            })
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let isBanner = news[indexPath.row].isBanner || (pinnedNews.count == 0 && indexPath.row == 0)
        let cell = tableView.dequeueReusableCell(withIdentifier: isBanner ? "banner": "cell") as! NewsTableViewCell
        cell.update(with: news[indexPath.row], path: indexPath)
        cell.likeButton?.addTarget(self, action: #selector(self.like(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelect(news: news[indexPath.row], cell: tableView.cellForRow(at: indexPath) as! NewsTableViewCell, tabCell: self)
    }
    
    @objc private func like(_ sender: LikeButton) {
        sender.isEnabled = false
        Server.makeRequest.likeNews(sender.news) { (newLikesCount, isLiked, isReal) in
            if let index = self.news.index(where: {$0.id == sender.news.id}) {
                self.news[index].likesCount = newLikesCount
                self.news[index].isLiked = isLiked
                asyncAfter {
                    sender.isEnabled = isReal
                    if self.tableItems.numberOfRows(inSection: 0) == self.tableView(self.tableItems, numberOfRowsInSection: 0) {
                        self.tableItems.reloadRows(at: [sender.path], with: .none)
                    } else {
                        self.tableItems.reloadData()
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row >= tableView.numberOfRows(inSection: 0) - 3 {
            self.loadNews()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = .clear
        return header
    }
}


