//
//  NewsCardCell.swift
//  Magadan
//
//  Created by Константин on 24.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import SafariServices



class NewsCardCell: UITableViewCell {
    
    @IBOutlet weak var body: UITextView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var likeIcon: UIImageView?
    @IBOutlet weak var likesCount: UILabel?
    @IBOutlet weak var likeButton: LikeButton?
    @IBOutlet weak var ogvLabel: UILabel?
    @IBOutlet weak var ogvHeightZero: NSLayoutConstraint?
    //видео в новостях
    @IBOutlet weak var videoView: VideoView?
    @IBOutlet weak var videoViewHeight: NSLayoutConstraint?
    @IBOutlet weak var dateBG: UIImageView?
    @IBOutlet weak var dateBGHeight: NSLayoutConstraint?
    
    var news_id: Int?
    var share_link: String?
    
    func update(with item: Any) {
        ogvHeightZero?.isActive = true
        news_id = nil
        
        if let news = item as? News {
            dateLabel.text = news.date?.parseDateShort()
            timeLabel.text = news.date?.parseTime()
            title.text = news.title
            body.attributedText = news.currentDisplayingTextAttributed
            likesCount?.text = String(news.likesCount)
            likeButton?.news = news
            likeIcon?.tintColor =  UIColor(hex: news.isLiked ? "486fb4" : "999999")
            likeIcon?.image = news.isLiked ? #imageLiteral(resourceName: "like_icon") : #imageLiteral(resourceName: "like_icon_disabled")
            ogvLabel?.text = news.ogv
            ogvHeightZero?.isActive = news.ogv.isEmpty
            news_id = news.id
            share_link = news.share_link
            videoViewHeight?.constant = (videoView?.set(urlString: news.videos.first?.link) == true) ? 196 : 0
        } else if let event = item as? Event {
            dateLabel.text = durationString(for: event)
            timeLabel.text = timeString(for: event)
            title.text = event.title
            body.attributedText = event.currentDisplayingTextAttributed
            likesCount?.isHidden = true
            likeIcon?.isHidden = true
            likeButton?.isHidden = true
        } else if let subItem = item as? SubMenuItem {
            let dateExists = subItem.date != nil || subItem.endDate != nil
            
            dateLabel.isHidden = !dateExists
            dateBG?.isHidden = !dateExists
            dateBGHeight?.constant = dateExists ? 19 : 0
            timeLabel.isHidden = !dateExists
            
            dateLabel.text = durationString(for: subItem)
            timeLabel.text = timeString(for: subItem)
            title.text = subItem.title//subItem.isSingle ? subItem.title : nil
            body.attributedText = subItem.attributedBody
            videoViewHeight?.constant = (videoView?.set(urlString: subItem.videos.first?.link) == true) ? 196 : 0
            
        }
        
        body.delegate = self
    }
    
    @IBAction func ogvTapped(_ sender: UIButton) {
        guard news_id != nil else { return }
        guard let url = URL(string: share_link ?? "https://www.49gov.ru/press/press_releases/index.php?id_4=\(news_id!)") else { return }
        root?.present(SFSafariViewController(url: url), animated: true, completion: nil)
    }
    
    
}

extension NewsCardCell: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        let urlString = URL.absoluteString
        guard urlString.hasPrefix("http") else { return true }
        let sf = SFSafariViewController(url: URL, entersReaderIfAvailable: true)
        UIApplication.shared.keyWindow?.rootViewController?.present(sf, animated: true, completion: nil)
        return false
    }
}


extension NewsCardCell {
    
    func timeString(for event: Event) -> String {
        guard event.startTime != nil || event.endTime != nil else { return "Дата не указана" }
        if event.startTime == nil || event.endTime == nil || event.startTime == event.endTime {
            if let start = event.startTime {
                return "\(start.parseTime())"
            } else {
                let end = event.endTime!
                return "\(end.parseTime())"
            }
        } else {
            let start = min(event.startTime!, event.endTime!)
            let end = max(event.startTime!, event.endTime!)
            
            if Calendar.current.isDate(start, inSameDayAs: end) {
                return "\(start.parseTime()) - \(end.parseTime())"
            } else {
                return ""
            }
        }
    }
        
        func timeString(for item: SubMenuItem) -> String {
            guard item.date != nil || item.endDate != nil else { return "Дата не указана" }
            if item.date == nil || item.endDate == nil || item.date == item.endDate {
                if let start = item.date {
                    return "\(start.parseTime())"
                } else {
                    let end = item.endDate!
                    return "\(end.parseTime())"
                }
            } else {
                let start = min(item.date!, item.endDate!)
                let end = max(item.date!, item.endDate!)
                
                if Calendar.current.isDate(start, inSameDayAs: end) {
                    return "\(start.parseTime()) - \(end.parseTime())"
                } else {
                    return ""
                }
                
                
            }
        }
    
    
    func durationString(for event: Event) -> String {
        guard event.startTime != nil || event.endTime != nil else { return "Дата не указана" }
        if event.startTime == nil || event.endTime == nil || event.startTime == event.endTime {
            if let start = event.startTime {
                return "\(start.dateToComponents().day) \(monthString(for: start)) \(yearString(for: start, comparing: Date()))"
                // 1 января (год, если не текущий)
            } else {
                let end = event.endTime!
                return "\(end.dateToComponents().day) \(monthString(for: end)) \(yearString(for: end, comparing: Date()))"
                // 1 января (год, если не текущий)
            }
        } else {
            let start = min(event.startTime!, event.endTime!)
            let end = max(event.startTime!, event.endTime!)
            
            let cal = Calendar.current
            
            if cal.isDate(start, inSameDayAs: end) {
                return "\(start.dateToComponents().day) \(monthString(for: start)) \(yearString(for: start, comparing: Date()))"
                // 1 января (год, если не текущий)
            } else {
                if start.dateToComponents().year == end.dateToComponents().year {
                    if start.dateToComponents().month == end.dateToComponents().month {
                        return "\(start.dateToComponents().day) - \(end.dateToComponents().day) \(monthString(for: start)) \(yearString(for: start, comparing: Date()))"
                        // 1 - 2 января (год, если не текущий)
                    } else {
                        return "\(start.dateToComponents().day) \(monthString(for: start)) - \(end.dateToComponents().day) \(monthString(for: end)) \(yearString(for: start, comparing: Date()))"
                        // 1 января - 2 февраля (год, если не текущий)
                    }
                } else {
                    return "\(start.dateToComponents().day) \(monthString(for: start)) \(start.dateToComponents().year) - \(end.dateToComponents().day) \(monthString(for: end)) \(end.dateToComponents().year)"
                    // 1 января 2017 - 2 февраля 2018
                }
            }
        }
    }
    
    
    func durationString(for menuItem: SubMenuItem) -> String {
        guard menuItem.date != nil || menuItem.endDate != nil else { return "Дата не указана" }
        if menuItem.date == nil || menuItem.endDate == nil || menuItem.date == menuItem.endDate {
            if let start = menuItem.date {
                return "\(start.dateToComponents().day) \(monthString(for: start)) \(yearString(for: start, comparing: Date()))"
                // 1 января (год, если не текущий)
            } else {
                let end = menuItem.endDate!
                return "\(end.dateToComponents().day) \(monthString(for: end)) \(yearString(for: end, comparing: Date()))"
                // 1 января (год, если не текущий)
            }
        } else {
            let start = min(menuItem.date!, menuItem.endDate!)
            let end = max(menuItem.date!, menuItem.endDate!)
            
            let cal = Calendar.current
            
            if cal.isDate(start, inSameDayAs: end) {
                return "\(start.dateToComponents().day) \(monthString(for: start)) \(yearString(for: start, comparing: Date()))"
                // 1 января (год, если не текущий)
            } else {
                if start.dateToComponents().year == end.dateToComponents().year {
                    if start.dateToComponents().month == end.dateToComponents().month {
                        return "\(start.dateToComponents().day) - \(end.dateToComponents().day) \(monthString(for: start)) \(yearString(for: start, comparing: Date()))"
                        // 1 - 2 января (год, если не текущий)
                    } else {
                        return "\(start.dateToComponents().day) \(monthString(for: start)) - \(end.dateToComponents().day) \(monthString(for: end)) \(yearString(for: start, comparing: Date()))"
                        // 1 января - 2 февраля (год, если не текущий)
                    }
                } else {
                    return "\(start.dateToComponents().day) \(monthString(for: start)) \(start.dateToComponents().year) - \(end.dateToComponents().day) \(monthString(for: end)) \(end.dateToComponents().year)"
                    // 1 января 2017 - 2 февраля 2018
                }
            }
        }
    }
    
    func monthString(for date: Date) -> String {
        let df = DateFormatter()
        df.dateFormat = "MMMM"
        return df.string(from: date)
    }
    
    func yearString(for date: Date, comparing with: Date) -> String {
        if date.dateToComponents().year == with.dateToComponents().year {
            return ""
        } else {
            return "\(date.dateToComponents().year)"
        }
    }
    
}
