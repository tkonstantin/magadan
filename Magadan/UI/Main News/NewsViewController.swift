//
//  NewsViewController.swift
//  Magadan
//
//  Created by Константин on 24.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit


protocol NewsLikesDelegate:class {
    func didUpdate(news: News)
}

class NewsViewController: SearchableViewController {
    
    class func makeOne(news: News?=nil, event: Event?=nil, openedFromSearch: Bool=false) -> NewsViewController {
        let new = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NewsViewController") as! NewsViewController
        new.news = news
        new.event = event
        new.openedFromSearch = openedFromSearch
        return new
    }
    
    weak var delegate: NewsLikesDelegate?
    @IBOutlet weak var shareButton: UIBarButtonItem!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionItems: UICollectionView!
    @IBOutlet weak var tableItems: UITableView!
    @IBOutlet weak var commentsButton: UIButton?
    
    
    
    var news: News?
    var event: Event?
    
    var currentItem: Any! {
        get {
            if news != nil {
                return news
            } else {
                return event
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionItems.delegate = self
        collectionItems.dataSource = self
        self.pageControl?.numberOfPages = self.collectionItems.numberOfItems(inSection: 0)
        tableItems.delegate = self
        tableItems.dataSource = self
        tableItems.tableFooterView = UIView()
        
        if (event == nil || event!.currentDisplayingText == loadingText) && (news == nil || news!.currentDisplayingText == loadingText) {
            showHUD()
        }
        
        if (event != nil && event!.image == Server.url_paths.imagesPathPreffix) || (news != nil && news!.largestImage == Server.url_paths.imagesPathPreffix) {
            self.tableItems.tableHeaderView = nil
        }

        
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableItems.reloadData()
    }
    
    var updated = false
    var openedFromSearch = false
    
    var commentsCount: Int {
        get {
            return news?.commentsCount ?? event?.commentsCount ?? 0
        }
    }
    
    func update(with updated: Any) {
        if let updatedNews = updated as? News {
            news = updatedNews
        } else if let updatedEvent = updated as? Event {
            event = updatedEvent
        }
        asyncAfter {
            self.commentsButton?.setTitle(self.commentsCount == 0 ? "ДОБАВИТЬ КОММЕНТАРИЙ" : "КОММЕНТАРИИ (\(self.commentsCount))", for: .normal)
            dismissHUD()
            self.tableItems?.reloadData()
            self.collectionItems?.reloadData()
            self.pageControl?.numberOfPages = self.collectionItems.numberOfItems(inSection: 0)
            self.updated = true
        }
        
    }
    
    @IBAction func share(_ sender: UIBarButtonItem) {
        var link = ""
        if news != nil {
            link = news!.share_link ?? "https://www.49gov.ru/press/press_releases/index.php?id_4=\(news!.id)"
        } else if event != nil {
            link = event!.share_link ?? "https://www.49gov.ru/press/events/?id_4=\(event!.id)"
        }
        let activity = UIActivityViewController(activityItems: [link], applicationActivities: nil)
        activity.popoverPresentationController?.barButtonItem = sender
        self.present(activity, animated: true, completion: nil)
    }
    
    
    @IBAction func commentsTapped(_ sender: UIButton) {
        guard news != nil || event != nil else { return }
        
        if commentsCount == 0 {
            if !UserDataManager.shared.isAuthorized {
                showNeedsAuthAlert()
                return;
            }
            let create = CreateCommentViewController.make(with: news ?? event!)
            self.navigationController?.pushViewController(create, animated: true)
        } else {
            let list = CommentsListViewController.make(with: news ?? event!)
            self.navigationController?.pushViewController(list, animated: true)
        }
    }
    
}

extension NewsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableItems.dequeueReusableCell(withIdentifier: "cell") as! NewsCardCell
        cell.update(with: currentItem)
        cell.likeButton?.addTarget(self, action: #selector(self.like(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc private func like(_ sender: LikeButton) {
        guard self.updated || self.openedFromSearch else { return }
        sender.isEnabled = false
        Server.makeRequest.likeNews(sender.news) { (newLikesCount, isLiked, isReal) in
            self.news?.likesCount = newLikesCount
            self.news?.isLiked = isLiked
            self.delegate?.didUpdate(news: self.news!)
            asyncAfter {
                sender.isEnabled = isReal
                self.tableItems.reloadData()
            }
            
        }
    }
    
}


extension NewsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let news = currentItem as? News {
            return news.allImages.count
        } else if let event = currentItem as? Event {
            return event.allImages.count
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        (cell.viewWithTag(1) as? UIImageView)?.image = #imageLiteral(resourceName: "event_bg")
        (cell.viewWithTag(2) as? UIActivityIndicatorView)?.startAnimating()
        
        if let news = currentItem as? News {
            (cell.viewWithTag(1) as? UIImageView)?.sd_setImage(with: URL(string: news.allImages[indexPath.item]))
        } else if let event = currentItem as? Event {
            (cell.viewWithTag(1) as? UIImageView)?.sd_setImage(with: URL(string: event.allImages[indexPath.item]))
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        PhotoViewerViewController.present(with: (currentItem as? News)?.allImages ?? (currentItem as? Event)?.allImages ?? [], startingIndex: indexPath.item, navigationController: self.navigationController)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == collectionItems {
            self.pageControl?.currentPage = Int(scrollView.contentOffset.x/self.view.bounds.width)
        }
    }
    
}
