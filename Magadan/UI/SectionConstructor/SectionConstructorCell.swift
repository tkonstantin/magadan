//
//  SectionConstructorCell.swift
//  Magadan
//
//  Created by Константин on 26.01.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class SectionConstructorCell: UICollectionViewCell {
    
    var subItem: SubMenuItem! {
        didSet {
            if pageControl != nil {
                pageControl.numberOfPages = subItem.allImages.count
            }
        }
    }
    
    @IBOutlet weak var tableItems: UITableView! {
        didSet {
            tableItems.contentInset.top = 8
        }
    }
    
    @IBOutlet weak var photosCollection: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl! {
        didSet {
            if subItem != nil {
                pageControl.numberOfPages = subItem.allImages.count
            }
        }
    }
    
    
    
}

extension SectionConstructorCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! NewsCardCell
        cell.update(with: subItem)
        return cell
    }
}

extension  SectionConstructorCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subItem.allImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        (cell.viewWithTag(1) as? UIImageView)?.image = #imageLiteral(resourceName: "event_bg")
        (cell.viewWithTag(2) as? UIActivityIndicatorView)?.startAnimating()
        
        (cell.viewWithTag(1) as? UIImageView)?.sd_setImage(with: URL(string: subItem.allImages[indexPath.item]))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        PhotoViewerViewController.present(with: subItem.allImages, startingIndex: indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentPage = Int(scrollView.contentOffset.x/scrollView.bounds.width)
        pageControl.currentPage = currentPage
    }
}
