//
//  SectionConstructorViewController.swift
//  Magadan
//
//  Created by Константин on 25.01.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import SVProgressHUD

class SectionConstructorViewController: UIViewController {

    class func makeOne(with item: MenuItem) -> SectionConstructorViewController {
        let new = UIStoryboard(name: "SectionConstructor", bundle: nil).instantiateViewController(withIdentifier: "SectionConstructorViewController") as! SectionConstructorViewController
        new.item = item
        new.title = item.title
        
        SVProgressHUD.show()
        
        Server.makeRequest.loadSubmenuItems(item: item) { subs in
            new.subItems = subs
            asyncAfter {
                SVProgressHUD.dismiss()
                new.topCollectionHeight.constant = subs.count == 1 ? 0 : 44
                new.mainCollection.reloadData()
                new.topCollection.reloadData()
            }
        }
        return new
    }
    
    var item: MenuItem!
    var subItems: [SubMenuItem] = []
    
    var isAnimatingTableOffset = false

    
    
    @IBOutlet weak var mainCollection: UICollectionView!
    @IBOutlet weak var topCollection: UICollectionView!
    @IBOutlet weak var topCollectionHeight: NSLayoutConstraint!
    
    var currentSubItemIndex: Int = 0 {
        didSet {
            guard currentSubItemIndex != oldValue  else { return }
            let targetPath = IndexPath(item: currentSubItemIndex, section: 0)
            self.topCollection.reloadData()
            self.isAnimatingTableOffset = true
            asyncAfter {
                self.topCollection.scrollToItem(at: targetPath, at: .centeredHorizontally, animated: true)
                self.mainCollection.scrollToItem(at: targetPath, at: .centeredHorizontally, animated: true)
                
                asyncAfter(milliseconds: 300, {
                    self.isAnimatingTableOffset = false
                })
            }

        }
    }
    
    override func viewDidLoad() {
        topCollection.register(UINib(nibName: "CategoryCell", bundle: nil), forCellWithReuseIdentifier: "cell")
    }
    

}

extension SectionConstructorViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == mainCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SectionConstructorCell
            cell.subItem = subItems[indexPath.item]
            return cell
        } else { //top Collection
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCell
            cell.update(with: subItems[indexPath.item].title, isActive: currentSubItemIndex == indexPath.item)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == mainCollection {
            return collectionView.bounds.size
        } else {
            return CGSize(width: subItems[indexPath.item].title.requiredWidth(16, font: nil, height: collectionView.bounds.height), height: collectionView.bounds.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard collectionView == topCollection  else { return }
        currentSubItemIndex = indexPath.item
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView == mainCollection && !isAnimatingTableOffset else { return }
        let currentPage = Int(scrollView.contentOffset.x/self.view.bounds.width)
        
        guard self.currentSubItemIndex != currentPage else { return }
        
        self.currentSubItemIndex = currentPage

        
        
    }
}
