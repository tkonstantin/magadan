//
//  SearchableViewController.swift
//  Magadan
//
//  Created by Константин on 01.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class SearchableViewController: RotatableViewController, SearchNavigationViewDelegate {
    
    private var searchService = SearchService()
    
    
    private var normalTitleView: UIView?
    private var otherRightButtons: [UIBarButtonItem] = []
    
    @IBOutlet weak var searchBarButton: UIBarButtonItem?
    private var savedSearchBarButton: UIBarButtonItem!
    
    
    @IBAction func showSearch(_ sender: UIBarButtonItem) {
        savedSearchBarButton = sender
        otherRightButtons = self.navigationItem.rightBarButtonItems?.filter({$0 != sender}) ?? []
        self.navigationItem.rightBarButtonItems = []
        normalTitleView = self.navigationItem.titleView
        self.navigationItem.titleView = SearchNavigationView.makeOne(delegate: self, searchService: self.searchService, currentController: self)
        
    }
    
    func searchViewDidCancel() {
        self.navigationItem.rightBarButtonItems = [savedSearchBarButton] + otherRightButtons
        self.navigationItem.titleView = normalTitleView
        self.savedSearchBarButton.image = #imageLiteral(resourceName: "search_icon")
    }
    
    func searchViewDidChangeText(to newText: String) {
        searchService.textChanged(newText)
    }
}

