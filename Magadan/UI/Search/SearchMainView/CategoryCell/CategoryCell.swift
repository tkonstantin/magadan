//
//  CategoryCell.swift
//  Magadan
//
//  Created by Константин on 01.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    
    let activeColor = UIColor(hex: "486FB4")
    let inactiveColor = UIColor(hex: "486FB4").withAlphaComponent(0.6)
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var line: UIView!
    
    
    func update(with text: String, isActive: Bool) {
        label.text = text
        label.textColor = isActive ? activeColor : inactiveColor
        line.isHidden = !isActive
    }
}
