//
//  SearchMainView.swift
//  Magadan
//
//  Created by Константин on 01.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class NewsCategory {
    var title = ""
    var id = 0
    
    init(with json: NSDictionary) {
        title = (json["title"] as? String ?? "").uppercased()
        id = json["id"] as? Int ?? Int(json["id"] as? String ?? "0") ?? 0
    }
}

class SearchItemsGroup {
    var type: SearchItem.types = .undefined
    var results: [SearchItem] = []
    var isExpanded = true
    var pageToRequest: Int = 0
    var needsLoadMoreButton = true
    
    init(with t: SearchItem.types) {
        type = t
    }
}


protocol SearchMainViewDelegate:class {
    func mainViewShouldOpen(event with: String)
    func mainViewShouldOpen(news with: String)
    func mainViewShouldOpen(phonebookItem with: String)
    func mainViewShouldOpen(photos with: [String])
    func mainViewShouldOpen(question with: String, theme_id: String, theme_title: String)
    func mainViewShouldOpen(idea with: String, theme_id: String, theme_title: String)
    func mainViewShouldOpen(video with: String)
    func mainViewShouldHideKeyboard()
}

class SearchMainView: UIView {
    
    weak var delegate: SearchMainViewDelegate!
    weak var navigationView: SearchNavigationView!
    
    class func makeOne(searchService: SearchService, delegate d: SearchMainViewDelegate) -> SearchMainView {
        let new = UINib(nibName: "SearchMainView", bundle: nil).instantiate(withOwner: nil, options: nil).first as! SearchMainView
        new.searchService = searchService
        new.delegate = d
        return new
    }
    
    fileprivate var resultsArrived = false
    fileprivate var firstResultsArrived = false
    func setResultsStack(_ stack: [SearchItem]) {
        if firstResultsArrived {
            resultsArrived = true
        }
        firstResultsArrived = true
        computedRowHeights = [:]
        var groups: [SearchItemsGroup] = []
        for item in stack {
            if let group = groups.first(where: {$0.type == item.type}) {
                group.results.append(item)
                group.needsLoadMoreButton = group.results.count == 5
            } else {
                let group = SearchItemsGroup(with: item.type)
                group.results.append(item)
                group.needsLoadMoreButton = group.results.count == 5
                groups.append(group)
            }
        }
        
        self.results = [currentRecentsGroup].flatMap({$0}) + groups.sorted(by: {$0.type.intValue < $1.type.intValue})
        
        self.results_table.reloadData()
    }
    
    fileprivate func updateResultsStack(_ stack: [SearchItem], sectionToInsert: Int, needsToRemoveLoadMore: Bool) {
        let previousItemsCount = self.results[sectionToInsert].results.count
        
        var groups: [SearchItemsGroup] = self.results
        for item in stack {
            if let group = groups.first(where: {$0.type == item.type}) {
                group.results.append(item)
            } else {
                let group = SearchItemsGroup(with: item.type)
                group.results.append(item)
                groups.append(group)
            }
        }
        if self.results.contains(where: {$0.type == .history}) {
            self.results = groups.sorted(by: {$0.type.intValue < $1.type.intValue})
        }  else {
            self.results = [currentRecentsGroup].flatMap({$0}) + groups.sorted(by: {$0.type.intValue < $1.type.intValue})
        }
        let currentItemsCount = self.results[sectionToInsert].results.count
        
        var newPaths: [IndexPath] = []
        
        if previousItemsCount < currentItemsCount {
            for index in previousItemsCount..<currentItemsCount {
                newPaths.append(IndexPath(row: index + 1, section: sectionToInsert))
            }
        }
        
        self.results_table.beginUpdates()
        if needsToRemoveLoadMore {
            self.results_table.deleteRows(at: [IndexPath(row: previousItemsCount + 1, section: sectionToInsert)], with: .middle)
        }
        if newPaths.count != 0 {
            self.results_table.insertRows(at: newPaths, with: .middle)
        }
        self.results_table.endUpdates()
        
    }
    
    var searchService: SearchService!
    
    var currentRecentsGroup: SearchItemsGroup? {
        get {
            let recents = SearchService.searchHistory
            guard recents.count != 0 else { return nil}
            let group = SearchItemsGroup(with: .history)
            group.results = recents
            group.isExpanded = true
            group.needsLoadMoreButton = false
            return group
        }
    }
    
    fileprivate var results: [SearchItemsGroup] = []
    
    var types: [SearchItem.types] = [.undefined, .news, .events, .questions, .ideas, .persons, .photo]
    
    static var defaultTypeIndex: Int = 0
    
    var currentTypeIndex: Int = defaultTypeIndex {
        didSet {
            guard currentTypeIndex != oldValue else { return }
            searchService?.changeType(to: selectedType)
            self.results = []
            self.results_table.reloadData()
            searchPlaceholder.isHidden = false
        }
    }
    
    var selectedType: SearchItem.types {
        get {
            return types[currentTypeIndex]
        }
    }
    
    var computedRowHeights: [IndexPath: CGFloat] = [:]
    
    @IBOutlet weak var searchPlaceholder: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var results_table: UITableView!
    override func awakeFromNib() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "CategoryCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        results_table.delegate = self
        results_table.dataSource = self
        results_table.tableFooterView = UIView()
        results_table.tableHeaderView = UIView()
        
        results_table.register(UINib(nibName: "ResultsHeader", bundle: nil), forCellReuseIdentifier: "header")
        results_table.register(UINib(nibName: "HistoryResultsCell", bundle: nil), forCellReuseIdentifier: "history_cell")
        results_table.register(UINib(nibName: "NormalResultCell", bundle: nil), forCellReuseIdentifier: "normal_cell")
        results_table.register(UINib(nibName: "LoadMoreCell", bundle: nil), forCellReuseIdentifier: "loadMore_cell")
        
        DispatchQueue.main.async {
            if self.collectionView.numberOfItems(inSection: 0) > SearchMainView.defaultTypeIndex {
                self.collectionView.scrollToItem(at: IndexPath(item: SearchMainView.defaultTypeIndex, section: 0), at: .centeredHorizontally, animated: true)
            }
        }
        
        
    }
    
}

extension SearchMainView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        currentTypeIndex = indexPath.item
        self.collectionView.reloadData()
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return types.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCell
        cell.update(with: types[indexPath.item].displayString.uppercased(), isActive: currentTypeIndex == indexPath.item)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: types[indexPath.item].displayString.uppercased().requiredWidth(12, font: UIFont.systemFont(ofSize: 12, weight: .medium), height: 56) + 16, height: collectionView.bounds.height)
    }
    
    
}

extension SearchMainView: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let notFound = (results.count == 1 && results.first!.type == .history) || results.count == 0
        if notFound && resultsArrived {
            let footer = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 88))
            footer.backgroundColor = .white
            let label = UILabel(frame: footer.bounds)
            label.textAlignment = .center
            label.font = UIFont.init(name: "Roboto-Regular", size: 15)
            label.textColor = .darkGray
            label.text = "Ничего не найдено"
            footer.addSubview(label)
            tableView.tableFooterView = footer
        } else {
            tableView.tableFooterView = nil
        }
        return results.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let currentGroup = results[section]
        return currentGroup.isExpanded ? currentGroup.results.count + 1 + (currentGroup.needsLoadMoreButton ? 1 : 0) : 1
    }
    
    func isLoadMorePath(_ path: IndexPath) -> Bool {
        return (path.row - 1) == results[path.section].results.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! ResultsHeader
            cell.update(with: results[indexPath.section])
            return cell
        } else if isLoadMorePath(indexPath) {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "loadMore_cell") as! LoadMoreCell
            return cell
        } else if results[indexPath.section].type == .history {
            let cell = tableView.dequeueReusableCell(withIdentifier: "history_cell") as! HistoryResultsCell
            cell.update(with: results[indexPath.section].results[indexPath.row - 1])
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "normal_cell") as! NormalResultCell
            cell.update(with: results[indexPath.section].results[indexPath.row - 1])
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        computedRowHeights[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let computed = computedRowHeights[indexPath] {
            return computed
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let currentGroup = results[indexPath.section]
            currentGroup.isExpanded = !currentGroup.isExpanded
            
            var changedIndexes: [IndexPath] = []
            for index in 0..<(currentGroup.results.count + (currentGroup.needsLoadMoreButton ? 1 : 0)) {
                changedIndexes.append(IndexPath(row: index + 1, section: indexPath.section))
            }
            tableView.beginUpdates()
            tableView.reloadRows(at: [indexPath], with: .none)
            if currentGroup.isExpanded {
                tableView.insertRows(at: changedIndexes, with: .middle)
            } else {
                tableView.deleteRows(at: changedIndexes, with: .middle)
            }
            tableView.endUpdates()
        } else if isLoadMorePath(indexPath) {
            let cell = tableView.cellForRow(at: indexPath) as? LoadMoreCell
            guard !(cell?.activity?.isAnimating ?? true) else { return }
            cell?.activity?.startAnimating()
            let currentGroup = results[indexPath.section]
            guard currentGroup.needsLoadMoreButton else { return }
            Server.makeRequest.searchBy(type: results[indexPath.section].type, query: self.searchService.currentSearchText, pageNumber: currentGroup.pageToRequest, completion: { (items) in
                currentGroup.pageToRequest += 1
                let previousLoadMoreValue = currentGroup.needsLoadMoreButton
                currentGroup.needsLoadMoreButton = items.count == 5
                self.updateResultsStack(items, sectionToInsert: indexPath.section, needsToRemoveLoadMore: previousLoadMoreValue != currentGroup.needsLoadMoreButton)
                (tableView.cellForRow(at: IndexPath(row: tableView.numberOfRows(inSection: indexPath.section) - 1, section: indexPath.section)) as? LoadMoreCell)?.activity.stopAnimating()
            })
        } else {
            let currentItem = (results[indexPath.section]).results[indexPath.row - 1]
            switch currentItem.type {
            case .events: delegate?.mainViewShouldOpen(event: currentItem.id)
            case .news: delegate?.mainViewShouldOpen(news: currentItem.id)
            case .persons: delegate?.mainViewShouldOpen(phonebookItem: currentItem.id)
            case .photo: delegate?.mainViewShouldOpen(photos: currentItem.links)
            case .video: delegate?.mainViewShouldOpen(photos: currentItem.links)
            case .questions: delegate?.mainViewShouldOpen(question: currentItem.id, theme_id: currentItem.theme_id, theme_title: currentItem.theme_title)
            case .ideas: delegate?.mainViewShouldOpen(idea: currentItem.id, theme_id: currentItem.theme_id, theme_title: currentItem.theme_title)
            case .history:
                navigationView?.searchBar.text = currentItem.title
                searchService.textChanged(currentItem.title, needsForceUpdate: true)
                delegate?.mainViewShouldHideKeyboard()
            case .undefined: return;
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = .clear
        header.frame = CGRect(origin: .zero, size: CGSize(width: self.bounds.width, height: 8))
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }
}
