//
//  ResulstsHeader.swift
//  Magadan
//
//  Created by Константин on 01.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class ResultsHeader: UITableViewCell {
    
    
    @IBOutlet weak var disclosure: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func update(with res: SearchItemsGroup) {
        label.text = res.type.displayString
        icon.image = res.type.displayIcon
        
        UIView.animate(withDuration: 0.3) {
            self.disclosure.transform = CGAffineTransform(rotationAngle: res.isExpanded ? .pi : 0)
        }
        
    }
    
}
