//
//  NormalResultCell.swift
//  Magadan
//
//  Created by Константин on 07.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class NormalResultCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func update(with item: SearchItem) {
        label.text = item.title
        dateLabel.text = item.date.parseDateShort(dots: true)
        dateLabel.isHidden = item.type == .persons
    }
    

}
