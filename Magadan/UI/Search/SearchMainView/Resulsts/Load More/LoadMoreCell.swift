//
//  LoadMoreCell.swift
//  Magadan
//
//  Created by Константин on 07.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class LoadMoreCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    override func prepareForReuse() {
        activity?.stopAnimating()
    }
}
