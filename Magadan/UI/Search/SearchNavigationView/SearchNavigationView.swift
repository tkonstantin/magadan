//
//  SearchView.swift
//  Magadan
//
//  Created by Константин on 01.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

protocol SearchNavigationViewDelegate: class {
    func searchViewDidCancel()
    func searchViewDidChangeText(to newText: String)
}

class SearchNavigationView: UIView {
    
    
    class func makeOne(delegate: SearchNavigationViewDelegate, searchService: SearchService, currentController: UIViewController) -> SearchNavigationView {
        let view = UINib(nibName: "SearchNavigationView", bundle: nil).instantiate(withOwner: nil, options: nil).first as! SearchNavigationView
        
        view.delegate = delegate
        view.searchService = searchService
        view.configureSearchmainView(for: currentController)
        return view
    }
    
    
    private func configureSearchmainView(for controller: UIViewController?) {
        guard controller != nil else { SearchMainView.defaultTypeIndex = 0; return }
        
        switch controller! {
        case is MainViewController:
            SearchMainView.defaultTypeIndex = 1
            searchService.changeType(to: .news)
        case is EventsViewController:
            SearchMainView.defaultTypeIndex = 2
            searchService.changeType(to: .events)
        case is PhonebookViewController, is DivisionViewController, is PersonViewController:
            SearchMainView.defaultTypeIndex = 5
            searchService.changeType(to: .persons)
        case is NewsViewController:
            configureSearchmainView(for: controller?.navigationController?.viewControllers.first)
        case is IdeasMainViewController:
            SearchMainView.defaultTypeIndex = (controller as! IdeasMainViewController).currentMode == .questions ? 3 : 4
            searchService.changeType(to: (controller as! IdeasMainViewController).currentMode == .questions ? .questions : .ideas)
        case is IdeaViewController:
            SearchMainView.defaultTypeIndex = (controller as! IdeaViewController).isIdea ? 4 : 3
            searchService.changeType(to: (controller as! IdeaViewController).isIdea ? .ideas : .questions)
        case is CreateIdeaViewController:
            SearchMainView.defaultTypeIndex = (controller as! CreateIdeaViewController).isIdea ? 4 : 3
            searchService.changeType(to: (controller as! CreateIdeaViewController).isIdea ? .ideas : .questions)
        default:
            SearchMainView.defaultTypeIndex = 0
            searchService.changeType(to: .undefined)
        }
    }
    
    
    fileprivate weak var delegate: SearchNavigationViewDelegate!
    fileprivate var searchService: SearchService!
    
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            searchBar.setScopeBarButtonTitleTextAttributes([NSAttributedStringKey.font.rawValue: UIFont.init(name: "Roboto-Regular", size: 15) as Any], for: .normal)
        }
    }
    
    
    override func didMoveToSuperview() {
        searchBar.delegate = self
        searchBar.becomeFirstResponder()
        addMainView()
    }
    
    
    
    override var intrinsicContentSize: CGSize {
        return UILayoutFittingExpandedSize
    }
    
    var searchMainView: SearchMainView?
    
    fileprivate func addMainView() {
        guard let controller =  self.delegate as? UIViewController, searchMainView == nil else { return }
        let mainView = SearchMainView.makeOne(searchService: self.searchService, delegate: self)
        mainView.frame = controller.view.bounds
        controller.view.addSubview(mainView)
        searchMainView = mainView
        searchMainView?.navigationView = self
        searchService.subscribeForResults { (foundedItems) in
            self.searchMainView?.searchPlaceholder.isHidden = true
            self.searchMainView?.setResultsStack(foundedItems)
            
        }
    }
    
    fileprivate func removeMainView() {
        searchMainView?.removeFromSuperview()
        searchMainView = nil
        
    }
    
}


extension SearchNavigationView: SearchMainViewDelegate {
    func mainViewShouldOpen(event with: String) {
        self.endEditing(true)
        
        guard let controller =  self.delegate as? UIViewController else { return }
        showHUD()
        Server.makeRequest.getEventMore(id: with) { (_event) in
            dismissHUD()
            guard let event = _event else { return }
            let events = NewsViewController.makeOne(event: event, openedFromSearch: true)
            controller.navigationController?.pushViewController(events, animated: true)
        }
        
    }
    
    func mainViewShouldOpen(news with: String) {
        self.endEditing(true)
        
        guard let controller =  self.delegate as? UIViewController else { return }
        showHUD()
        Server.makeRequest.getNewsMore(id: with) { (_news) in
            dismissHUD()
            guard let news = _news else { return }
            let newsVC = NewsViewController.makeOne(news: news, openedFromSearch: true)
            controller.navigationController?.pushViewController(newsVC, animated: true)
        }
        
    }
    
    func mainViewShouldOpen(phonebookItem with: String) {
        self.endEditing(true)
        
        guard let controller =  self.delegate as? UIViewController else { return }
        
        
        guard let item = PhoneBookParser.instance.findItem(with: with) else { return }
        
        if let itm =  item as? (Category, Bool) {

            let category = itm.0
            if category.phones.count == 0 && category.persons.keys.count == 0 {
                let phonebook = PhonebookViewController.makeOne()
                phonebook.navigationItem.setLeftBarButtonItems([], animated: false)
                phonebook.currentItems = [category]
                phonebook.currentParent = PhoneBookParser.instance.findItem(with: category.parent_id) as? Category
                controller.navigationController?.pushViewController(phonebook, animated: true)
            } else {
                let division = DivisionViewController.makeOne()
                division.currentParent = category
                division.navigationItem.setLeftBarButtonItems([], animated: false)
                division.navigationItem.title = category.title
                controller.navigationController?.pushViewController(division, animated: true)
            }
        } else if let itm = item as? Person {
            let pers = itm
            let person = PersonViewController.makeOne(with: pers, isHead: nil)
            controller.navigationController?.pushViewController(person, animated: true)
        }
        
    }
    
    func mainViewShouldOpen(question with: String, theme_id: String, theme_title: String) {
        self.endEditing(true)
        guard let controller =  self.delegate as? UIViewController else { return }
        showHUD()
        Server.makeRequest.getFullQuestion(with: with, theme_id: theme_id) { (_question) in
            guard let question = _question else { dismissHUD(); return }
            asyncAfter {
                let ideaVC =  IdeaViewController.makeOne(with: question, theme_title: theme_title, isIdea: false)
                controller.navigationController?.pushViewController(ideaVC, animated: true)
                dismissHUD()
            }
        }
    }
    
    func mainViewShouldOpen(idea with: String, theme_id: String, theme_title: String) {
        self.endEditing(true)
        guard let controller =  self.delegate as? UIViewController else { return }
        showHUD()
        Server.makeRequest.getFullIdea(with: with, theme_id: theme_id) { (_idea) in
            guard let idea = _idea else { dismissHUD(); return }
            asyncAfter {
                let ideaVC =  IdeaViewController.makeOne(with: idea, theme_title: theme_title, isIdea: true)
                controller.navigationController?.pushViewController(ideaVC, animated: true)
                dismissHUD()
            }
        }

    }
    
    func mainViewShouldOpen(photos with: [String]) {
        self.endEditing(true)
        
        guard let controller =  self.delegate as? UIViewController else { return }
        
        PhotoViewerViewController.present(with: with, navigationController: controller.navigationController)
    }
    
    func mainViewShouldOpen(video with: String) {
        self.endEditing(true)
        
        if let url = URL(string: with) {
            UIApplication.shared.openURL(url)
        }
    }
    
    func mainViewShouldHideKeyboard() {
        self.endEditing(true)
    }
    
    
}

extension SearchNavigationView: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        delegate.searchViewDidCancel()
        searchBar.endEditing(true)
        removeMainView()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        delegate.searchViewDidChangeText(to: searchText)
        searchMainView?.searchPlaceholder.isHidden = false
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            searchBar.endEditing(true)
            return false
        }
        return true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        guard let text = searchBar.text, !text.isEmpty else { return }
        
        let newItem = SearchItem(with: [:])
        newItem.title = text
        newItem.type = .history
        newItem.rawType = "HISTORY"
        
        if !SearchService.searchHistory.contains(where: {$0.title == newItem.title}) {
            SearchService.searchHistory = SearchService.searchHistory + [newItem]
        }
    }
}
