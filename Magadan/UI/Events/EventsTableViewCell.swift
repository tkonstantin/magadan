//
//  EventsTableViewCell.swift
//  Magadan
//
//  Created by Константин on 19.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit



extension UIImageView {
    
    func makeGray(gray: Bool, originalImage: UIImage) {
        if gray {
            let ciraw = CIImage(image: originalImage)
//            DispatchQueue.global(qos: .utility).async {
                if let ciimage = ciraw?.applyingFilter("CIColorControls", parameters: ["inputSaturation": 0, "inputContrast": 1]) {
//                    asyncAfter {
                        self.image = UIImage(ciImage: ciimage)
//                    }
                } else {
//                    asyncAfter {
                        self.image = originalImage
//                    }
                }
//            }
        } else {
            self.image = originalImage
        }
    }
}

class EventsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var datesLabel: UILabel!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
//    var saved_event: Event!
    
    func update(with event: Event) {
        eventTitle.text = event.title
        datesLabel.text = durationString(for: event)
//        saved_event = event
        
        icon.sd_setImage(with: URL(string: event.image), placeholderImage: #imageLiteral(resourceName: "event_bg")) { [weak self] (img, _, _, _) in
            asyncAfter { [weak self] in
                if img != nil {
                    self?.icon.makeGray(gray: !event.isActual, originalImage: img!)
                } else {
                    self?.icon.image = #imageLiteral(resourceName: "event_bg")
                }
            }
        }
        
        self.contentView.alpha = event.isActual ? 1 : 0.5
        datesLabel.textColor = event.isActual ? blueColor : UIColor.darkGray
        
    }
//
//    override func prepareForReuse() {
//        guard saved_event != nil else { return }
//
//        if let img = self.icon.image {
//            self.icon.makeGray(gray: !self.saved_event.isActual, originalImage: img)
//        } else {
//            self.icon.image = #imageLiteral(resourceName: "event_bg")
//        }
//
//    }
    
    
    func durationString(for event: Event) -> String {
        guard event.startTime != nil || event.endTime != nil else { return "Дата не указана" }
        if event.startTime == nil || event.endTime == nil || event.startTime == event.endTime {
            if let start = event.startTime {
                return "\(start.dateToComponents().day) \(monthString(for: start)) \(yearString(for: start, comparing: Date())) \(start.parseTime())"
                // 1 января (год, если не текущий) 00:00
            } else {
                let end = event.endTime!
                return "\(end.dateToComponents().day) \(monthString(for: end)) \(yearString(for: end, comparing: Date())) \(end.parseTime())"
                // 1 января (год, если не текущий) 00:00
            }
        } else {
            let start = min(event.startTime!, event.endTime!)
            let end = max(event.startTime!, event.endTime!)
            
            let cal = Calendar.current
            
            if cal.isDate(start, inSameDayAs: end) {
                return "\(start.dateToComponents().day) \(monthString(for: start)) \(yearString(for: start, comparing: Date())) \(start.parseTime()) - \(end.parseTime())"
                // 1 января (год, если не текущий) 00:00  - 01:00
            } else {
                if start.dateToComponents().year == end.dateToComponents().year {
                    if start.dateToComponents().month == end.dateToComponents().month {
                        return "\(start.dateToComponents().day) - \(end.dateToComponents().day) \(monthString(for: start)) \(yearString(for: start, comparing: Date()))"
                        // 1 - 2 января (год, если не текущий)
                    } else {
                        return "\(start.dateToComponents().day) \(monthString(for: start)) - \(end.dateToComponents().day) \(monthString(for: end)) \(yearString(for: start, comparing: Date()))"
                        // 1 января - 2 февраля (год, если не текущий)
                    }
                } else {
                    return "\(start.dateToComponents().day) \(monthString(for: start)) \(start.dateToComponents().year) - \(end.dateToComponents().day) \(monthString(for: end)) \(end.dateToComponents().year)"
                    // 1 января 2017 - 2 февраля 2018
                }
            }
        }
    }
    
    
    func monthString(for date: Date) -> String {
        let df = DateFormatter()
        df.dateFormat = "MMMM"
        return df.string(from: date)
    }
    
    func yearString(for date: Date, comparing with: Date) -> String {
        if date.dateToComponents().year == with.dateToComponents().year {
            return ""
        } else {
            return "\(date.dateToComponents().year)"
        }
    }
    
}
