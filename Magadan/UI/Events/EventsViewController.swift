//
//  EventsViewController.swift
//  Magadan
//
//  Created by Константин on 19.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import SafariServices
import PullToRefresh

class EventsViewController: SearchableViewController {
    
    static var previousEventsList: [Event] = []
    
    class func makeOne() -> EventsViewController {
        let events = (UIStoryboard(name: "Events", bundle: nil).instantiateInitialViewController() as! UINavigationController).viewControllers.first as! EventsViewController
        return events
    }
    
    @IBOutlet weak var tableItems: UITableView!
    
    @IBOutlet weak var monthButton: UIButton!
    @IBOutlet weak var yearButton: UIButton!
    
    var refresher = PullToRefresh()
    
    var events: [Event] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTable()
        if EventsViewController.previousEventsList.count != 0 {
            self.events = EventsViewController.previousEventsList
            self.reloadTable()
        } else {
            getEvents()
        }
        self.getLives()
        self.updateMonthButton()
        self.updateYearButton()
    }
    
    
    @objc override func reloadScreen() {
        getEvents()
        getLives()
    }
    
    
    func reloadTable() {
            self.tableItems.reloadData()
            guard self.tableItems.numberOfSections != 0 else { return }
            let sortedEvents = self.events.sorted(by: {$0.maxDate < $1.maxDate})
            let index = sortedEvents.index(where: {$0.isActual})
            self.tableItems.scrollToRow(at: IndexPath(row: 0, section: index ?? 0), at: .top, animated: false)
    }
    
    func getLives() {
        Server.makeRequest.getLive { (lives) in
            var temp = lives.filter({$0.isInRange(month: Server.makeRequest.currentMonth, year: Server.makeRequest.currentYear)})
            temp.appendSafely(self.events)
            self.events = temp.sorted(by: {$0.maxDate < $1.maxDate})
            asyncAfter {
                self.reloadTable()
            }
        }
    }
    
    var currentPage: Int = -1
    var isRequestingNextPage = false
    
    var footerView: UIView {
        get {
            let footer = UIView(frame: CGRect(origin: .zero, size: CGSize(width: self.view.bounds.width, height: 88)))
            let label = UILabel(frame: CGRect(x: 0, y: 56, width: self.view.bounds.width, height: 32))
            label.text = "Загрузка..."
            label.textColor = .darkGray
            label.textAlignment = .center
            footer.addSubview(label)
            let activity = UIActivityIndicatorView(frame: CGRect(x: 0, y: 24, width: self.view.bounds.width, height: 44))
            activity.activityIndicatorViewStyle = .gray
            activity.color = blueColor
            activity.startAnimating()
            footer.addSubview(activity)
            return footer
        }
    }
    
    var isLoaded = false
    
    
    func getEvents(completion: (()->Void)?=nil) {
        if !isRequestingNextPage {
            isRequestingNextPage = true
            self.tableItems.tableFooterView = footerView
            currentPage += 1
            Server.makeRequest.getEvents(page: currentPage) { (result) in
                self.events.appendSafely(result)
                self.events.sort(by: {$0.maxDate < $1.maxDate})
            
                asyncAfter {
                    EventsViewController.previousEventsList = self.events
                    dismissHUD()
                    self.isLoaded = true
                    self.reloadTable()
                    self.isRequestingNextPage = false
                    self.tableItems.tableFooterView = nil
                    completion?()
                }
            }
        } else {
            completion?()
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let next = segue.destination as? NewsViewController {

            let index = tableItems.indexPath(for: sender as! UITableViewCell)!.section
            next.event = self.events[index]
            Server.makeRequest.getEventMore(next.event!, completion: { (updatedEvent) in
                guard let upd = updatedEvent else { return }
                next.update(with: upd)
                self.events[index] = upd
            })
        } else if segue.identifier == "showMenu" {
            currentPath = IndexPath(row: 1, section: 0)
        }
    }
    
    func updateMonthButton() {
        self.monthButton.setTitle(Server.makeRequest.currentMonth.monthName, for: .normal)
    }
    
    func updateYearButton() {
        self.yearButton.setTitle(String(Server.makeRequest.currentYear), for: .normal)
        
    }
    
    
    @IBAction func pickMonth(_ sender: UIButton) {
        guard !isRequestingNextPage else { return }
        let picker =  DatePickerViewController.makeOne(month: Server.makeRequest.currentMonth, year: Server.makeRequest.currentYear, delegate: self)
        self.present(picker, animated: true, completion: nil)
        
    }
    
    
    @IBAction func pickYear(_ sender: UIButton) {
        guard !isRequestingNextPage else { return }
        let picker =  DatePickerViewController.makeOne(month: Server.makeRequest.currentMonth, year: Server.makeRequest.currentYear, delegate: self)
        self.present(picker, animated: true, completion: nil)
    }
    
    
    
}

extension EventsViewController: DatePickerDelegate {
    func datePicker_didPicked(month: Int, year: Int) {
        guard Server.makeRequest.currentMonth != month || Server.makeRequest.currentYear != year else { return }
        asyncAfter {
            Server.makeRequest.currentMonth = month
            Server.makeRequest.currentYear = year
            self.updateMonthButton()
            self.updateYearButton()
            showHUD()
            self.events = []
            self.isRequestingNextPage = false
            self.currentPage = -1
            self.getEvents()
            self.getLives()
        }
        
    }
}

extension EventsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func configureTable() {
        tableItems.delegate = self
        tableItems.dataSource = self
        tableItems.tableFooterView = UIView()
        tableItems.estimatedRowHeight = 200
        tableItems.rowHeight = UITableViewAutomaticDimension
        
        tableItems.addPullToRefresh(refresher) {
            asyncAfter(milliseconds: 300, {
                self.currentPage = -1
                self.getEvents {
                    self.tableItems.endAllRefreshing()
                }
                self.getLives()
            })

        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        tableView.tableHeaderView?.frame.size.height = (events.count != 0 || !isLoaded) ? 0 : 88
        tableView.tableHeaderView?.isHidden = events.count != 0 || !isLoaded
        return events.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentEvent = events[indexPath.section]
        let hasImage = currentEvent.image != Server.url_paths.imagesPathPreffix && !currentEvent.image.isEmpty
        let cell = tableView.dequeueReusableCell(withIdentifier: currentEvent.isLive ? "liveCell" : hasImage ? "cell" : "smallCell") as! EventsTableViewCell
        cell.update(with: currentEvent)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentEvent = self.events[indexPath.section]
        if currentEvent.isLive {
            if let browser = BrowserViewController.makeOne(title: "Прямая трансляция", with: currentEvent.source, shareLink: currentEvent.share_link ?? "https://www.49gov.ru/press/broadcast/?id=\(currentEvent.id)") {
                self.navigationController?.pushViewController(browser, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 6
        } else {
            return 3
        }
    }
    

    
}


extension Array where Element: Event {
    mutating func appendSafely(_ newElement: Element) {
        if !self.contains(where: {$0.id == newElement.id}) {
            self.append(newElement)
        }
    }
    
    mutating func appendSafely(_ newArr: [Element]) {
        for newElement in newArr {
            self.appendSafely(newElement)
        }
    }
}


