//
//  SplashViewController.swift
//  Magadan
//
//  Created by Константин on 31.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class SplashViewController: RotatableViewController {
    
    class func makeOne() -> UIViewController {
        let new = UIStoryboard(name: "Splash", bundle: nil).instantiateInitialViewController()!
        return new
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        Server.makeRequest.initialLoading {
            if Server.makeRequest.isAuthorized {
                UIApplication.shared.keyWindow?.rootViewController = MainNav.makeOne()
            } else {
                if UserDataManager.shared.onboardingShowned {
                    UIApplication.shared.keyWindow?.rootViewController = AuthViewController.makeOne()
                } else {
                    UIApplication.shared.keyWindow?.rootViewController = OnboardingViewController.makeOne()
                    
                }
            }
        }
    
    }

}
