//
//  OnboardingViewController.swift
//  Magadan
//
//  Created by Константин on 14.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class OnboardingViewController: RotatableViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    class func makeOne() -> OnboardingViewController {
        return UIStoryboard(name: "Onboarding", bundle: nil).instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
    }

    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBAction func skip(_ sender: UIButton) {
        UserDataManager.shared.onboardingShowned = true
        UIApplication.shared.keyWindow?.rootViewController = AuthViewController.makeOne()

    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        (cell.viewWithTag(1) as? UIImageView)?.image = UIImage(named: "onboarding_\(indexPath.item)")
        if let doneButton = cell.viewWithTag(2) as? UIButton {
            doneButton.isHidden = indexPath.item != collectionView.numberOfItems(inSection: indexPath.section) - 1
            doneButton.addTarget(self, action: #selector(self.skip(_:)), for: .touchUpInside)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x/self.view.bounds.width)
    }

}
