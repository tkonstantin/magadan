//
//  TermsViewController.swift
//  Magadan
//
//  Created by Константин on 14.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

protocol TermsDelegate:class {
    func confirmed()
}

class TermsViewController: RotatableViewController {
    
    weak var delegate: TermsDelegate!

    @IBAction func back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func confirm(_ sender: UIButton) {
        delegate.confirmed()
        self.dismiss(animated: true, completion: nil)
    }
    

    

}
