//
//  AuthViewController.swift
//  Magadan
//
//  Created by Константин on 31.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class AuthViewController: RotatableViewController, UIWebViewDelegate {
    
    class func push(from: UINavigationController?=UIApplication.shared.keyWindow?.rootViewController as? UINavigationController) {
        guard !Server.makeRequest.isAuthorized else { return }
        let new = UIStoryboard(name: "Auth", bundle: nil).instantiateInitialViewController() as! AuthViewController
        new.isPushed = true
        from?.pushViewController(new, animated: true)
    }
    
    class func makeOne() -> AuthViewController {
        let new = UIStoryboard(name: "Auth", bundle: nil).instantiateInitialViewController() as! AuthViewController
        new.isPushed = false
        return new
    }
    
    var isPushed = false
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var termsView: UIView!
    @IBOutlet weak var skipButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        termsView.isHidden = false
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.skipButton.isHidden = isPushed
    }
    
    func loadWeb() {
        SVProgressHUD.show()
        URLCache.shared.removeAllCachedResponses()
        HTTPCookieStorage.shared.cookies?.forEach({HTTPCookieStorage.shared.deleteCookie($0)})
        webView.stringByEvaluatingJavaScript(from: "localStorage.clear();")
        
        asyncAfter {
            self.webView.delegate = self
            let req = URLRequest(url: URL(string: "https://www.49gov.ru/oauth/?mobile=1")!, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 30)
            self.webView.loadRequest(req)
            
        }

    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {

        var password: String?
        var login: String?
        if let body = request.httpBody {
            if let bodyString = String(data: body, encoding: .utf8) {
                let parameters = bodyString.components(separatedBy: "&")
                for param in parameters {
                    let subparams = param.components(separatedBy: "=")
                    let left = subparams.first
                    let right = subparams.last
                    if left == "password" {
                        password = right
                    } else if left == "login" {
                        login = right
                    }
                }
            }
        }


        if (login?.lowercased() == "sample@test.user" || login?.lowercased() == "sample%40test.user") && password?.lowercased() == "passw0rd" {
            print("TEST USER DETECTED")
            SVProgressHUD.show()
            Server.makeRequest.updateAuthSession(with: ["session_id": "56b71351ee3981d8d28f31fa73e44b96", "user_id": "1004181203"])
            Server.makeRequest.getCurrentUser(completion: {
                asyncAfter {
                    SVProgressHUD.dismiss()
                    if self.isPushed {
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        UIApplication.shared.keyWindow?.rootViewController = MainNav.makeOne()
                    }
                }
            })
            
            return false
        }
        if request.url?.absoluteString == "https://www.49gov.ru/simplesaml/module.php/saml/sp/saml2-acs.php/esia" {
            SVProgressHUD.show()
            Alamofire.request(request).responseJSON(completionHandler: { (response) in
                Server.makeRequest.updateAuthSession(with: response.result.value as? NSDictionary)
                Server.makeRequest.getCurrentUser(completion: {
                    asyncAfter {
                        SVProgressHUD.dismiss()
                        if self.isPushed {
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            UIApplication.shared.keyWindow?.rootViewController = MainNav.makeOne()
                        }
                    }
                })
            })
            return false
        }
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if let abs = webView.request?.url?.absoluteString, abs.hasPrefix("https://esia.gosuslugi") {
            SVProgressHUD.dismiss()
        }
    }

    @IBAction func skipTapped(_ sender: UIButton) {
        UIApplication.shared.keyWindow?.rootViewController = MainNav.makeOne()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? TermsViewController {
            dest.delegate = self
        }
    }
    
}


extension AuthViewController: TermsDelegate {
    func confirmed() {
        self.loadWeb()
        Server.makeRequest.isTermConfirmed = true
        self.termsView.isHidden = true
    }
    //terms view
    
}
