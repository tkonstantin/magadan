//
//  RefViewController.swift
//  Magadan
//
//  Created by Константин on 30.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class RefViewController: RotatableViewController {

   
    class func makeOne() -> RefViewController {
        return (UIStoryboard(name: "Ref", bundle: nil).instantiateInitialViewController() as! UINavigationController).viewControllers.first as! RefViewController
    }

}
