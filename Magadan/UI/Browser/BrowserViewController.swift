//
//  BrowserViewController.swift
//  Magadan
//
//  Created by Константин on 07.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import WebKit

class BrowserViewController: UIViewController, UIWebViewDelegate {

    class func makeOne(title: String?=nil, with urlString: String, shareLink: String?=nil) -> BrowserViewController? {
        guard URL(string: urlString) != nil else { return nil }
        let new = UIStoryboard(name: "Browser", bundle: nil).instantiateViewController(withIdentifier: "BrowserViewController") as! BrowserViewController
        new.urlString = urlString
        new.shareLink = shareLink
        new.customTitle = title
        return new
    }
    
    fileprivate var urlString: String!
    fileprivate var shareLink: String?
    fileprivate var customTitle: String?

    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var webView: UIWebView!

    fileprivate var linkToShare: String! {
        get {
            return shareLink ?? urlString
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        webView.loadRequest(URLRequest(url: URL(string: urlString)!))
        title = customTitle
    }

    
    @IBAction func doneTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func shareTapped(_ sender: UIBarButtonItem) {
        let act = UIActivityViewController(activityItems: [linkToShare], applicationActivities: nil)
        self.present(act, animated: true, completion: nil)
    }
    
    var progressTimer: Timer!
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        progressTimer = Timer.scheduledTimer(timeInterval: 1/60, target: self, selector: #selector(self.checkProgress), userInfo: nil, repeats: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        progressTimer?.invalidate()
        progressTimer = nil
        progressView.setProgress(1, animated: true)
        asyncAfter(milliseconds: 300) {
            self.progressView.setProgress(0, animated: false)
        }

    }
    
    @objc private func checkProgress() {
        progressView.setProgress(progressView.progress + 1/600, animated: false)
        if progressView.progress + 1/600 == 1 {
            self.progressView.setProgress(0, animated: false)
        }
    }

    
}
