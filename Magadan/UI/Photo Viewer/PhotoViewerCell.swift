//
//  PhotoViewerCell.swift
//  Magadan
//
//  Created by Константин on 08.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class PhotoViewerCell: UICollectionViewCell, UIScrollViewDelegate {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var placeholder: UIView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    override func prepareForReuse() {
        activity.startAnimating()
        scrollView.setZoomScale(1, animated: false)
    }
    
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return img
    }
    
    
}
