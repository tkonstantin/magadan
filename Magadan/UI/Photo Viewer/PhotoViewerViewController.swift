//
//  PhotoViewerViewController.swift
//  Magadan
//
//  Created by Константин on 08.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class PhotoViewerViewController: RotatableViewController {
    
    class func present(with links: [String]=[], images: [UIImage]=[], startingIndex: Int=0, navigationController: UINavigationController?=nil) {
        let new = UIStoryboard(name: "PhotoViewer", bundle: nil).instantiateInitialViewController() as! PhotoViewerViewController
        new.links = links
        new.images = images
        new.startingIndex = startingIndex
        var nav = navigationController
        if nav == nil {
            nav = UIApplication.shared.keyWindow?.rootViewController?.navigationController ?? UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
        }
        nav?.pushViewController(new, animated: true)
        
    }
    
    var links: [String] = []
    var images: [UIImage] = []
    var startingIndex: Int = 0
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollection()
        pageControl.numberOfPages = links.count
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override var prefersStatusBarHidden: Bool {
        get {
            return true
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.collectionView.alpha = 0
        let currentPage = Int(self.collectionView.contentOffset.x/self.view.bounds.width)
        
        self.collectionView.collectionViewLayout.invalidateLayout()
        
        coordinator.animate(alongsideTransition: { (context) in
            self.collectionView.contentOffset.x = CGFloat(currentPage) * size.width
        }) { (context) in
            UIView.animate(withDuration: 0.15, animations: {
                self.collectionView.alpha = 1
            })
        }
        
    }
    
    @IBAction func doubleTapped(_ sender: UITapGestureRecognizer) {
        if let cell = collectionView.cellForItem(at: IndexPath(item: Int(self.collectionView.contentOffset.x/self.view.bounds.width), section: 0)) as? PhotoViewerCell {
            cell.scrollView.setZoomScale(cell.scrollView.zoomScale == 1 ? 2 : 1, animated: true)
        }
    }
    
    
}


extension PhotoViewerViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func configureCollection() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.setContentOffset(CGPoint(x: self.view.bounds.width*CGFloat(startingIndex), y: 0), animated: false)
        asyncAfter {
            self.pageControl.currentPage = self.startingIndex
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return links.count == 0 ? images.count : links.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PhotoViewerCell
        cell.placeholder.isHidden = false
        cell.activity.startAnimating()
        if links.count != 0 {
            cell.img.sd_setImage(with: URL(string: links[indexPath.item])) { (_, _, _, _) in
                cell.placeholder.isHidden = true
            }
        } else {
            cell.img.image = images[indexPath.item]
            cell.placeholder.isHidden = true
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.view.bounds.size
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView == collectionView else { return }
        let currentPage = Int(scrollView.contentOffset.x/self.view.bounds.width)
        pageControl.currentPage = currentPage
    }
    
}
