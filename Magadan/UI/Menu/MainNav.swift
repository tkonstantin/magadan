//
//  MainNav.swift
//  Magadan
//
//  Created by Константин on 31.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class MainNav: RotatableNavigationController {
    
    class func makeOne() -> UINavigationController {
        let new = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as! MainNav
        return new
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        (UIApplication.shared.delegate as? AppDelegate)?.waitingShortCutAction?()
        (UIApplication.shared.delegate as? AppDelegate)?.waitingShortCutAction = nil
    }
    
    
}
