//
//  WheatherViewController.swift
//  Magadan
//
//  Created by Константин on 20.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

enum dayPart {
    case morning
    case day
    case evening
    case night
}

typealias wheather = (dayPart: dayPart, degrees: String, iconName: String)


class WheatherViewController: RotatableViewController {
    
    class func makeOne() -> WheatherViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WheatherViewController") as! WheatherViewController
    }
    
    @IBOutlet weak var morningView: DayPartView!
    @IBOutlet weak var dayView: DayPartView!
    @IBOutlet weak var evenignView: DayPartView!
    @IBOutlet weak var nightView: DayPartView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var currentIcon: UIImageView!
    @IBOutlet weak var currentDegrees: UILabel!
    
    @IBOutlet weak var dateButton: UIButton! {
        didSet {
            dateButton.setTitle(Date().parseDate(), for: .normal)
        }
    }
    
    @IBOutlet weak var cityImage: UIImageView! {
        didSet {
                cityImage?.sd_setImage(with: URL(string: UserDataManager.shared.currentCityImage?.link ?? ""), placeholderImage: UserDataManager.shared.cityImagePlaceholder, completed: { (img, _, _, _) in
                    UserDataManager.shared.cityImagePlaceholder = img 
                })
        }
    }
    
    
    var currentWheathers: [wheather] = []
    
    
    var districts: [District] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTable()
        configureViews()
        loadHourlyWheather()
        loadDisctrictsWheather()
        currentIcon.image = UIImage(named: Server.makeRequest.currentIcon + "_huge")
        currentDegrees.text = Server.makeRequest.currentDegrees == 300 ? nil : (String(Server.makeRequest.currentDegrees)  + "°С")
    }
    
    func loadDisctrictsWheather() {
        Server.makeRequest.getWheathers { (results) in
            var ds: [District?] = []
            for result in results {
                let disctrictObject = District(result.key, result.degrees == 300 ? "?" : String(result.degrees), result.icon)
                ds.append(disctrictObject)
            }
            DispatchQueue.main.async {
                self.districts = ds.flatMap({$0})
                self.tableView.reloadData()
            }
        }
    }
    
    func loadHourlyWheather() {
            self.currentWheathers = Server.makeRequest.currentWheathers
            self.configureViews()
    }
    
    func configureViews() {
        for w in currentWheathers {
            switch w.dayPart {
            case .morning:
                morningView.update(with: w.degrees, iconName: w.iconName)
                morningView.isHidden = w.iconName == "no"
            case .day:
                dayView.update(with: w.degrees, iconName: w.iconName)
                dayView.isHidden = w.iconName == "no"
            case .evening:
                evenignView.update(with: w.degrees, iconName: w.iconName)
                evenignView.isHidden = w.iconName == "no"
            case .night:
                nightView.update(with: w.degrees, iconName: w.iconName)
                nightView.isHidden = w.iconName == "no"
            }
            
            if morningView.isHidden && dayView.isHidden && evenignView.isHidden && nightView.isHidden {
                nightView.isHidden = false
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMenu" {
            currentPath = IndexPath(row: 10, section: 10)
        }
    }
    
}


extension WheatherViewController: UITableViewDelegate, UITableViewDataSource {
    
    func configureTable() {
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return districts.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let current = districts[indexPath.section]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DistricttTableViewCell
        cell.update(title: current.title, with: current.degrees, iconName: current.iconName)
        return cell
    }
    
    
}
