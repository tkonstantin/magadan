//
//  DayPartView.swift
//  Magadan
//
//  Created by Константин on 20.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class DayPartView: UIView {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var degrees: UILabel!
    @IBOutlet weak var icon: UIImageView!

    
    func update(with _degrees: String, iconName: String) {
        degrees.text = _degrees + "°С"
        icon.image = UIImage(named: iconName)
    }

}
