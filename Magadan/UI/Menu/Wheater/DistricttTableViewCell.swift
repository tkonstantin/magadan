//
//  DistricttTableViewCell.swift
//  Magadan
//
//  Created by Константин on 20.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class DistricttTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var degrees: UILabel!
    
    
    func update(title: String, with _degrees: String, iconName: String) {
        label.text = title
        degrees.text = _degrees + "°С"
        icon.image = UIImage(named: iconName)
    }
    
}
