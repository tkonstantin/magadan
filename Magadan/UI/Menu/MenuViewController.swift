//
//  MenuViewController.swift
//  Magadan
//
//  Created by Константин on 17.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import SideMenu
import CoreLocation
import SDWebImage


var currentPath: IndexPath = IndexPath(row: 0, section: 0)


var menuItems: [[MenuItem]] = [[MenuItem(img: #imageLiteral(resourceName: "search_news_icon"), title: "Новости"),
                                MenuItem(img: #imageLiteral(resourceName: "events_icon"), title: "Мероприятия"),
                                MenuItem(img: #imageLiteral(resourceName: "polls_icon"), title: "Опросы"),
                                MenuItem(img: #imageLiteral(resourceName: "ideas_menu_icon"), title: "Вопросы и идеи"),
                                MenuItem(img: #imageLiteral(resourceName: "reception_icon"), title: "Приемная"),
                                MenuItem(img: #imageLiteral(resourceName: "phonebook_icon"), title: "Справочник")],
                               [MenuItem(img: #imageLiteral(resourceName: "settings_icon"), title: "Личный кабинет"),
                                MenuItem(img: #imageLiteral(resourceName: "about_icon"), title: "О приложении"),
                                MenuItem(img: #imageLiteral(resourceName: "share_icon"), title: "Поделиться"),
                                MenuItem(img: #imageLiteral(resourceName: "auth_menu_icon"), title: UserDataManager.shared.isAuthorized ? "Выйти": "Войти")
    ]]


extension UIViewController {
    func showNeedsAuthAlert() {
        let alert = UIAlertController(title: "Необходима авторизация", message: "Для того, чтобы воспользоваться данной функцией, Вам потребуется авторизоваться через Портал Госуслуг", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Согласен", style: .default, handler: { (_) in
            AuthViewController.push(from: self.navigationController)
        }))
        alert.addAction(UIAlertAction(title: "Пропустить", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

class MenuViewController: RotatableViewController {
    
    @IBOutlet weak var wheatherIcon: UIImageView!
    @IBOutlet weak var wheatherLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var mainIMg: UIImageView!
    @IBOutlet weak var tableHeader: UIView!
    @IBOutlet weak var tableItems: UITableView!
    @IBOutlet weak var headerButtton: UIButton!
    
    fileprivate var animatingIcon: UIImageView!
    fileprivate var animatingIcontTimer: Timer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTable()
        
    }
    
    override func reloadScreen() {
        checkTranslations()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fillHeader()
        getWeather()
        checkTranslations()
        
        mainIMg?.sd_setImage(with: URL(string: UserDataManager.shared.currentCityImage?.link ?? ""), placeholderImage: UserDataManager.shared.cityImagePlaceholder, completed: { (img, _, _, _) in
            UserDataManager.shared.cityImagePlaceholder = img
        })
    }
    
    var hasActiveTranslations = false
    
    func checkTranslations() {
        Server.makeRequest.checkTranslations { hasActive in
            self.hasActiveTranslations = hasActive
            asyncAfter {
                self.tableItems.reloadData()
            }
        }
    }
    
    let locationManager = CLLocationManager()
    
    func getWeather() {
        locationManager.delegate = self
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            loadWhether()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
        
    }
    
    
    
    var isWheatherLoaded = false
    
    private func loadWhether() {
        guard !isWheatherLoaded else { return }
        self.isWheatherLoaded = true
        guard let coordinates = locationManager.location?.coordinate else { return }
        Server.makeRequest.setCurrentCoordinates(lat: coordinates.latitude, lon: coordinates.longitude)
        
        Server.makeRequest.getCurrentWeatherHourly {
            DispatchQueue.main.async {
                self.wheatherLabel.text = Server.makeRequest.currentDegrees == 300 ? nil : (String(Server.makeRequest.currentDegrees) + "°С")
                self.wheatherIcon.image = UIImage(named: Server.makeRequest.currentIcon)
                self.wheatherIcon.isHidden = Server.makeRequest.currentDegrees == 300
                self.headerButtton.isEnabled = true
            }
            
        }
    }
    
    func fillHeader() {
        self.dayLabel.text = Date().parseDate()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            scrollView.contentOffset.y = 0
        }
    }
    
    
    @IBAction func wheatherTapped(_ sender: UIButton) {
        root?.mainNav?.viewControllers = [WheatherViewController.makeOne()]
        currentPath = IndexPath(row: 10, section: 10)
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension MenuViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
            loadWhether()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        loadWhether()
        manager.stopUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location update failed with error = \(error)")
    }
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func configureTable() {
        tableItems.delegate = self
        tableItems.dataSource = self
        tableItems.tableFooterView = UIView()
        tableHeader.frame.size.height = self.view.bounds.width*9/16
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.prepareForLogout), name: NSNotification.Name("logout"), object: nil)
    }
    
    @objc func prepareForLogout() {
        asyncAfter {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentItem = (menuItems[indexPath.section])[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MenuTableViewCell
        
        if currentItem.isDynamic {
            cell.icon.sd_setImage(with: URL(string: currentItem.img_link))
            cell.bg_image.sd_setImage(with: URL(string: currentItem.backgroundImage))
            cell.txt.textColor = currentPath == indexPath ? blueColor : UIColor(hex: "607D8B")
            
            cell.txt.isHidden = currentItem.shouldHideTitle
            cell.bg_image.alpha = currentItem.shouldHideTitle ? 1 : 0.2
        } else {
            cell.icon.image = currentItem.img
            cell.bg_image.image = nil
            cell.txt.textColor = currentPath == indexPath ? blueColor : UIColor(hex: "333333")
            cell.txt.isHidden = false
        }
        
        
        
        if indexPath.row == 1 && indexPath.section == 0 {
            if hasActiveTranslations {
                self.startAnimateIcon(imgView: cell.live_icon)
            } else {
                self.stopAnimatingIcon()
                cell.live_icon.isHidden = true
            }
        } else {
            cell.live_icon.isHidden = true
        }
        
        cell.txt.text = currentItem.title
        if indexPath.section == 1 && indexPath.row == 3 {
            cell.txt.text = UserDataManager.shared.isAuthorized ? "Выйти": "Войти"
        }
        cell.icon.tintColor = currentPath == indexPath ? blueColor : UIColor(hex: "808080")
        cell.backgroundColor = currentPath == indexPath ? UIColor.lightGray.withAlphaComponent(0.2) : .white
        return cell
    }
    
    
    func startAnimateIcon(imgView: UIImageView) {
        animatingIcon = imgView
        animatingIcontTimer?.invalidate()
        animatingIcontTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.runAnimation), userInfo: nil, repeats: true)
        animatingIcontTimer.fire()
    }
    
    @objc private func runAnimation() {
        guard animatingIcon != nil else { return }
        self.animatingIcon.isHidden = false
        UIView.animate(withDuration: 1.4, animations: {
            self.animatingIcon?.alpha = 0
        }) { (_) in
            UIView.animate(withDuration: 1.4, animations: {
                self.animatingIcon?.alpha = 1
            })
        }
    }
    
    func stopAnimatingIcon() {
        animatingIcontTimer?.invalidate()
        animatingIcontTimer = nil
        animatingIcon = nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath == IndexPath(row: 4, section: 0) && !UserDataManager.shared.isAuthorized {
            showNeedsAuthAlert()
            return;
        }
        
        if indexPath != IndexPath(row: 2, section: 1) && indexPath != IndexPath(row: 3, section: 1) {
            currentPath = indexPath
            self.tableItems.reloadData()
        }
        
        guard let rootNav = (UIApplication.shared.keyWindow?.rootViewController as? UINavigationController) else { return }
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0: //новости
                rootNav.viewControllers = [MainViewController.makeOne()]
                self.dismiss(animated: true, completion: nil)
                
            case 1: //мероприятия
                rootNav.viewControllers = [EventsViewController.makeOne()]
                self.dismiss(animated: true, completion: nil)
                
            case 2: //опросы
                rootNav.viewControllers = [PollsListViewController.makeOne()]
                self.dismiss(animated: true, completion: nil)
                
            case 3: //вопросы и идеи
                rootNav.viewControllers = [ThemesViewController.makeOne()]
                self.dismiss(animated: true, completion: nil)
                
            case 4: //приемная
                rootNav.viewControllers = [ReceptionViewController.makeThemes()]
                self.dismiss(animated: true, completion: nil)
                
            case 5: //Справочник
                rootNav.viewControllers = [PhonebookViewController.makeOne()]
                self.dismiss(animated: true, completion: nil)
                
            default: //Разделы из конструктора разделов
                rootNav.viewControllers = [SectionConstructorViewController.makeOne(with: menuItems[indexPath.section][indexPath.row])]
                self.dismiss(animated: true, completion: nil)
            }
        case 1:
            switch indexPath.row {
            case 0: //личный кабинет
                rootNav.viewControllers = [SettingsViewController.makeOne()]
                self.dismiss(animated: true, completion: nil)
                
            case 1: // о приложении
                rootNav.viewControllers = [AboutViewController.makeOne()]
                self.dismiss(animated: true, completion: nil)
                
            case 2: //поделиться
                let activity = UIActivityViewController(activityItems: ["https://mobile.49gov.ru/"], applicationActivities: nil)
                activity.popoverPresentationController?.sourceView = tableView.cellForRow(at: indexPath)
                self.present(activity, animated: true, completion: nil)
                
            default: // войти/выйти
                if Server.makeRequest.isAuthorized {
                    let alert = UIAlertController(title: "Вы уверены, что хотите выйти из аккаунта?", message: nil, preferredStyle: .actionSheet)
                    alert.addAction(UIAlertAction(title: "Выйти", style: .destructive, handler: { (_) in
                        Server.makeRequest.logout()
                    }))
                    alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    Server.makeRequest.logout()
                }
            }
        default: break;
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard section == 1 else { return .leastNonzeroMagnitude }
        return 9
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard section == 1 else { return nil}
        let header = UIView()
        let line = UIView(frame: CGRect(x: 0, y: 4, width: self.view.bounds.width, height: 1))
        line.backgroundColor = UIColor(hex: "E6E6E6")
        header.addSubview(line)
        return header
    }
    
}






