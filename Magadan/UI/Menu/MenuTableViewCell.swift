//
//  MenuTableViewCell.swift
//  Magadan
//
//  Created by Константин on 19.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var txt: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var bg_image: UIImageView!
    @IBOutlet weak var live_icon: UIImageView!
    
    
}
