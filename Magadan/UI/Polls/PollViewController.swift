//
//  PollViewController.swift
//  Magadan
//
//  Created by Константин on 11.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import SVProgressHUD

extension UIViewController {
    func showOKAlert(title: String?, message: String?, from: UIViewController?=UIApplication.shared.keyWindow?.rootViewController, tappedHandler: (()->Void)?=nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in tappedHandler?(); return; }))
        from?.present(alert, animated: true, completion: nil)
    }
}

class PollViewController: SearchableViewController {
    
    class func makeOne(with poll: Poll) -> PollViewController {
        let new = UIStoryboard(name: "Polls", bundle: nil).instantiateViewController(withIdentifier: "PollViewController") as! PollViewController
        new.poll = poll
        return new
    }
    
    private var poll: Poll!
    
    var visibleQuestions: [Question] {
        get {
            let allQuestions: [Question] = poll.questions
            var allAnswersIds: [String] = []
            for question in allQuestions {
                for _index in question.selectedAnswerIndexes {
                    let index = _index - 1
                    if question.answers.count > index && index >= 0{
                        allAnswersIds.append(question.answers[index].id)
                    }
                }
            }
            
            return allQuestions.filter({allAnswersIds.contains($0.parentAnswer) || $0.parentAnswer.isEmpty})
        }
    }
    
    var computedHeights: [IndexPath: CGFloat] = [:]
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendButton: PollSendButton!
    @IBOutlet weak var completedLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sendButton.isHidden = poll.isCompleted
        completedLabel.isHidden = !poll.isCompleted
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateCurrentPoll {
            self.sendButton.isHidden = self.poll.isCompleted
            self.completedLabel.isHidden = !self.poll.isCompleted
            self.tableView.reloadData()
        }
    }
}

extension PollViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return visibleQuestions.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : visibleQuestions[section - 1].answersCount + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! PollHeaderCell
            cell.update(with: poll)
            return cell
        } else {
            let currentQuestion = visibleQuestions[indexPath.section - 1]
            
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "question") as! PollQuestionCell
                cell.update(with: currentQuestion)
                return cell
            } else if currentQuestion.isText {
                let cell = tableView.dequeueReusableCell(withIdentifier: "text") as! PollTextCell
                cell.update(with: currentQuestion, delegate: self, isCompleted: poll.isCompleted)
                return cell
                
            } else {
                let currentAnswer = currentQuestion.answers[indexPath.row - 1]
                let cell = tableView.dequeueReusableCell(withIdentifier: "answer") as! PollAnswerCell
                cell.update(with: currentAnswer, isSelected: currentQuestion.selectedAnswerIndexes.contains(indexPath.row), isMultiple: currentQuestion.isMultiple, isCompleted: poll.isCompleted)
                return cell
            }
        }
    }
    
    
    
    @IBAction func send() {
        guard visibleQuestions.filter({!$0.isText}).filter({$0.selectedAnswerIndexes.count == 0}).count == 0 else { self.showOKAlert(title: "Внимание! Вы не ответили на все вопросы.", message: "Ответьте на все вопросы", from: self); return}
        SVProgressHUD.show()
        Server.makeRequest.sendPollAnswers(poll) {
            self.updateCurrentPoll {
                self.showOKAlert(title: "Ваш голос учтен", message: "Спасибо, что приняли участие в опросе!") {
                    self.sendButton.isHidden = self.poll.isCompleted
                    self.completedLabel.isHidden = !self.poll.isCompleted
                    self.tableView.reloadData()
                }
                
            }
        }
        
    }
    
    fileprivate func updateCurrentPoll(completion: @escaping ()->Void) {
        
        Server.makeRequest.getPoll(by: self.poll.id) { (updatedPoll) in
            if updatedPoll != nil  {
                self.poll = updatedPoll
            }
            asyncAfter {
                SVProgressHUD.dismiss()
                completion()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let computed = computedHeights[indexPath] {
            return computed
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 6 : 3
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.computedHeights[indexPath] = cell.frame.size.height
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard UserDataManager.shared.isAuthorized else { self.showNeedsAuthAlert(); return }
        
        guard indexPath.section != 0, indexPath.row != 0 else { return }
        let currentQuestion = visibleQuestions[indexPath.section - 1]
        
        if let index = currentQuestion.selectedAnswerIndexes.index(of: indexPath.row) {
            currentQuestion.selectedAnswerIndexes.remove(at: index)
        } else {
            if currentQuestion.isMultiple {
                currentQuestion.selectedAnswerIndexes.append(indexPath.row)
            } else {
                currentQuestion.selectedAnswerIndexes = [indexPath.row]
            }
        }
        
        let offsetY = self.tableView.contentOffset.y
        self.tableView.reloadData()
        self.tableView.contentOffset.y = offsetY
        
    }
}


extension PollViewController: TextCellDelegate {
    func didBeginEditing(question: Question?) {
        UIView.animate(withDuration: 0.25) {
            self.tableView.contentInset.bottom = 230
            if let index = self.visibleQuestions.index(where: {$0.id == question?.id}) {
                self.tableView.scrollToRow(at: IndexPath(row: 0, section: index + 1), at: .top, animated: true)
            }
        }
    }
    
    func didEndEditing(text: String, question: Question?) {
        if let currentQuestion = visibleQuestions.first(where: {$0.id == question?.id}) {
            currentQuestion.text =  text
        }
        
        UIView.animate(withDuration: 0.25) {
            self.tableView.contentInset.bottom = 0
        }
    }
}
