//
//  PollsListViewController.swift
//  Magadan
//
//  Created by Константин on 11.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import SVProgressHUD

class PollsListViewController: SearchableViewController {
    
    class func makeOne(with polls: [Poll]?=nil) -> PollsListViewController {
        let new = UIStoryboard(name: "Polls", bundle: nil).instantiateViewController(withIdentifier: "PollsListViewController") as! PollsListViewController
        new.shouldLoadPolls = polls == nil
        new.polls = polls ?? []
        new.pushed = polls != nil
        return new
    }

    @IBOutlet weak var tableView: UITableView!
    
    var polls: [Poll] = []
    var shouldLoadPolls = true
    var page: Int = -1
    var pushed = false
    
    private var isLoaded = false
    var isLoading = false

    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show()
        configureTable()
        if pushed {
            self.navigationItem.setLeftBarButtonItems([], animated: false)
        }
    }
    
    override func reloadScreen() {
        loadPolls(for: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadPolls(for: 0)
    }
    
    func loadPolls(for specifiedPage: Int?=nil) {
        guard shouldLoadPolls && !isLoading else { SVProgressHUD.dismiss(); return }
        
        if specifiedPage != nil && specifiedPage! > page {
            page = specifiedPage!
        } else {
            page += 1
        }
        
        isLoading = true
        Server.makeRequest.getPolls(page: page) { (result, hasNext) in
            self.shouldLoadPolls = hasNext
            self.isLoaded = true
            self.polls += result
            self.polls.sort(by: {$0.startDate > $1.startDate})
            asyncAfter {
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
                self.isLoading = false
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard !pushed else { return }
        if segue.identifier == "showMenu" {
            currentPath = IndexPath(row: 2, section: 0)
        }
    }

}

extension PollsListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func configureTable() {
        tableView.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        tableView.tableHeaderView?.frame.size.height = (polls.count != 0 || !isLoaded) ? 0 : 128
        tableView.tableHeaderView?.isHidden = polls.count != 0 || !isLoaded
        return polls.count 
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PollTableViewCell
        cell.update(with: polls[indexPath.section])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pollVC = PollViewController.makeOne(with: polls[indexPath.section])
        self.navigationController?.pushViewController(pollVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard isLoaded && indexPath.section > polls.count - 2 else { return }
        loadPolls()
    }
}
