//
//  PollTextCell.swift
//  Magadan
//
//  Created by Константин on 12.02.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit


protocol TextCellDelegate: class {
    func didEndEditing(text: String, question: Question?)
    func didBeginEditing(question: Question?)
}

class PollTextCell: UITableViewCell {
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var symbolsCountLabel: UILabel!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    fileprivate weak var delegate: TextCellDelegate?
    fileprivate var question: Question?
    
    func update(with q: Question, delegate d: TextCellDelegate?, isCompleted: Bool) {
        self.question = q
        textView.text = q.text
        self.placeholderLabel.isHidden = (q.text != nil && !q.text!.isEmpty) || isCompleted
        
        delegate = d
        
        textView.isEditable = !isCompleted
        symbolsCountLabel.isHidden = isCompleted
        doneButton.isHidden = isCompleted
        
        if isCompleted && (q.text == nil || q.text!.isEmpty) {
            textView.text = /*q.answers.first?.title ??*/ "Нет ответа"
        }
        
    }
    
    @IBAction func doneTapped(_ sender: UIButton) {
        textView.resignFirstResponder()
    }
    
    
}

extension PollTextCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        doneButton.isHidden = false
        self.placeholderLabel.isHidden = true
        delegate?.didBeginEditing(question: self.question)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        doneButton.isHidden = true
        self.placeholderLabel.isHidden = !textView.text.isEmpty
        delegate?.didEndEditing(text: textView.text, question: self.question)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        symbolsCountLabel.text = "\(textView.text.count)/256"
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        return newText.count <= 256
    }
}
