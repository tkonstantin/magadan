//
//  PollTableViewCell.swift
//  Magadan
//
//  Created by Константин on 11.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class PollTableViewCell: UITableViewCell {

    @IBOutlet weak var poll_title: UILabel!
    @IBOutlet weak var votes_count: UILabel!
    @IBOutlet weak var rightDate: UILabel!
    @IBOutlet weak var isCompleted_icon: UIImageView!
    
    
    func update(with poll: Poll) {
        rightDate.text = "до " + poll.endDate.parseDateShort(dots: true)
        poll_title.text = poll.title
        votes_count.text = poll.votes_count
        isCompleted_icon.isHidden = !poll.isCompleted
    }

}
