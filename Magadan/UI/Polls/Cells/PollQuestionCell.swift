//
//  PollQuestionCell.swift
//  Magadan
//
//  Created by Константин on 30.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import UIKit

class PollQuestionCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    
    func update(with question: Question) {
        label.text = question.title
    }
}
