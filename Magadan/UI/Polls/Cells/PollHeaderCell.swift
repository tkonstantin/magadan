//
//  PollTableViewCell.swift
//  Magadan
//
//  Created by Константин on 07.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class PollHeaderCell: UITableViewCell {

    @IBOutlet weak var date_label: UILabel!
    @IBOutlet weak var title_label: UILabel!
    @IBOutlet weak var votes_count: UILabel!
    @IBOutlet weak var completedIcon: UIImageView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgContainterHeight: NSLayoutConstraint!
    

    @IBAction func showImage(_ sender: UIButton) {
        guard let img = _poll?.image, !img.isEmpty else { return }
        PhotoViewerViewController.present(with: [img])
    }
    
    var _poll: Poll?
    
    func update(with poll: Poll) {
        _poll = poll
        date_label.text = "до " + poll.endDate.parseDateShort(dots: true)
        title_label.text = poll.title
        votes_count.text = poll.votes_count
        completedIcon.isHidden = !poll.isCompleted
        
        if let url = URL(string: poll.image ?? "") {
            imgContainterHeight.constant = 200
            imgView.sd_setImage(with: url)
        } else {
            imgContainterHeight.constant = 0
        }
        

    }
    
}



