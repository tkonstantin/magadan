//
//  PollAnswerCell.swift
//  Magadan
//
//  Created by Константин on 30.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import UIKit

class PollAnswerCell: UITableViewCell {

    @IBOutlet weak var checkmark_button: UIButton!
    @IBOutlet weak var checkmark_icon: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var stats: UILabel!
    
    
    func update(with answer: Answer, isSelected: Bool, isMultiple: Bool, isCompleted: Bool) {
        label.text = answer.title
        if isCompleted {
            checkmark_icon.image = answer.previousVote ? #imageLiteral(resourceName: "polls_completed_icon") : nil
            stats.text = "\(answer.votesCount) (\(answer.votesPercentage)%)"
        } else {
            checkmark_icon.image = isSelected ? (isMultiple ?  #imageLiteral(resourceName: "checkmark_filled")  : #imageLiteral(resourceName: "notification_filled")) : (isMultiple ? #imageLiteral(resourceName: "checkmark_empty") : #imageLiteral(resourceName: "notifications_empty"))
            stats.text = nil
        }
    }
}
