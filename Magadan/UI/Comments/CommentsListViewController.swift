//
//  CommentsListViewController.swift
//  Magadan
//
//  Created by Константин on 08/10/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class CommentsListViewController: UIViewController {
    
    class func make(with obj: Any) -> CommentsListViewController {
        let new = UIStoryboard(name: "Comments", bundle: nil).instantiateViewController(withIdentifier: "CommentsListViewController") as! CommentsListViewController
        new.obj = obj
        return new
    }
    
    var comments: [Comment] = []
    var obj: Any?
    var currentPage: Int = 0
    var pagesCount: Int = 1
    var isLoading = false
    var currentEarlierThanParam: String? = nil {
        didSet {
            currentPage = 0
        }
    }

    var currentLaterThanParam: String? = nil
    
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startLoadingComments()
    }
  
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        currentLaterThanParam = comments.first?.id
        startLoadingComments(showingHUD: false)
    }
    
    
    private func startLoadingComments(showingHUD: Bool = true) {
        guard !isLoading, (currentPage < self.pagesCount || currentLaterThanParam != nil) else { return }
        isLoading = true
        if showingHUD {
            showHUD()
        } else {
            self.loadingActivity?.startAnimating()
        }
        
        Server.makeRequest.getComments(obj as? News, event: obj as? Event, idea: obj as? Idea, page: currentPage, laterThan: currentLaterThanParam, earlierThan: currentEarlierThanParam) { (result, _pagesCount) in
            
            if self.currentPage == 0 && self.currentEarlierThanParam == nil && self.currentLaterThanParam == nil {
                self.comments = result
            } else {
                self.comments += result
            }
            
            self.comments = self.comments.sorted(by: >)
            
            if self.currentLaterThanParam == nil {
                self.currentPage += 1
                self.pagesCount = _pagesCount
            }
            self.currentLaterThanParam = nil
            asyncAfter {
                if let header = self.tableView.tableHeaderView {
                    header.frame.size.height = self.comments.count == 0 ? 64 : 0
                    self.tableView.tableHeaderView = header
                }
                self.tableView.reloadData()
                self.isLoading = false
                self.loadingActivity?.stopAnimating()
                dismissHUD()
            }
        }
        
    }
    
    
    @IBAction func createCommentTapped(_ sender: Any) {
        guard UserDataManager.shared.isAuthorized else {
            showNeedsAuthAlert();
            return
        }
        
        self.performSegue(withIdentifier: "createComment", sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let creating = segue.destination as? CreateCommentViewController {
            creating.obj = self.obj
        }
    }
    
}

extension CommentsListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CommentTableViewCell
        cell.update(with: comments[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            self.currentEarlierThanParam = self.comments.last?.id
            self.startLoadingComments(showingHUD: false)
        }
    }
}
