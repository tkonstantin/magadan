//
//  CommentTableViewCell.swift
//  Magadan
//
//  Created by Константин on 08/10/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {


    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var commentTextLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    
    
    func update(with comment: Comment) {
        dateLabel.text = comment.date.parseDateShort()
        timeLabel.text = comment.date.parseTime()
        commentTextLabel.text = comment.text
        usernameLabel.text = comment.username
    }
}
