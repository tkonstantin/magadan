//
//  CreateCommentViewController.swift
//  Magadan
//
//  Created by Константин on 08/10/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit

class CreateCommentViewController: CreateIdeaViewController {
    
    class func make(with obj: Any) -> CreateCommentViewController {
        let new = UIStoryboard(name: "Comments", bundle: nil).instantiateViewController(withIdentifier: "CreateCommentViewController") as! CreateCommentViewController
        new.obj = obj
        return new
    }
    
    var obj: Any? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Комментарий"
    }

    
    @IBAction func sendTapped(_ sender: UIButton) {
        guard txtView.text.count > 5 else {
            showOKAlert(title: "Слишком короткий комментарий", message: "Минимальная длина комментария - 6 символов")
            return;
        }
        
        showHUD()
        Server.makeRequest.leaveComment(obj as? News, event: obj as? Event, idea: obj as? Idea, text: txtView.text) { (createdComment) in
            asyncAfter {
                dismissHUD()
                if createdComment != nil {
                    self.showOKAlert(title: "Ваш комментарий получен и появится после модерации.", message: "Благодарим за оставленный комментарий!", from: self, tappedHandler: {
                        if let list = self.navigationController?.viewControllers.dropLast().last as? CommentsListViewController {
                            list.comments = [createdComment!] + list.comments
                            list.tableView?.reloadData()
                        }
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            }
        }
    }
}
