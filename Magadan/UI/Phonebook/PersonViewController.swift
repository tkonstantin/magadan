//
//  PersonViewController.swift
//  Magadan
//
//  Created by Константин on 11.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import MessageUI
import Alamofire


class PersonViewController: RotatableViewController {
    
    class func makeOne(with person: Person, isHead: Bool?) -> PersonViewController {
        let new = UIStoryboard(name: "Phonebook", bundle: nil).instantiateViewController(withIdentifier: "PersonViewController") as! PersonViewController
        new.person = person
        new.isHead = isHead
        return new
    }

    @IBOutlet weak var tableView: UITableView!
    
    var person: Person!
    var isHead: Bool?
    var currentMenuIndexPath: IndexPath?

    var items: [phonebookItem] {
        get {
            var itms: [phonebookItem] = []
            itms.append((icon: #imageLiteral(resourceName: "phonebook_person"), title: person.fio, action: nil ))
            if !person.post.isEmpty {
                itms.append((icon: #imageLiteral(resourceName: "phonebook_post"), title: person.post, action: nil))
            }
            if !person.phone.isEmpty {
                itms.append((icon: #imageLiteral(resourceName: "phone_icon"), title: person.phone, action: { self.call(to: self.person.phone) }))
            }
            if !person.phone2.isEmpty && person.phone2.rawPhone != person.phone.rawPhone {
                itms.append((icon: #imageLiteral(resourceName: "phone_icon"), title: person.phone2, action: { self.call(to: self.person.phone2) }))
            }
            if !person.fax.isEmpty {
                itms.append((icon: #imageLiteral(resourceName: "fax_icon"), title: person.fax, action: nil ))
            }
            if !person.email.isEmpty {
                itms.append((icon: #imageLiteral(resourceName: "mail_icon"), title: person.email, action: { self.email(to: self.person.email) }))
            }
            if !person.email2.isEmpty && person.email2.lowercased() != person.email.lowercased() {
                itms.append((icon: #imageLiteral(resourceName: "mail_icon"), title: person.email2, action: { self.email(to: self.person.email2) }))
            }
            
            if !person.schedule.isEmpty {
                itms.append((icon: #imageLiteral(resourceName: "search_recents_icon"), title: person.schedule, action: nil))
            }
            if !person.address.isEmpty {
                itms.append((icon: #imageLiteral(resourceName: "map_icon"), title: person.address, action: { self.openMap(to: self.person.address) }))
            }
            return itms
        }
    }
    
    override func viewDidLoad() {
        self.title = isHead == nil ? "" : isHead! ? "Руководство" : "Сотрудники"
    }
    
    func call(to: String) {
        let phone = to.rawPhone
        guard let url = URL(string: "tel://\(phone)") else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func email(to: String) {
        let compose = MFMailComposeViewController()
        compose.mailComposeDelegate = self
        compose.setToRecipients([to])
        compose.mailComposeDelegate = self
        self.present(compose, animated: true, completion: nil)
    }
    
    func openMap(to: String) {
        let escapedString = Alamofire.request("http://maps.apple.com/", method: .get, parameters: ["q":  to], encoding: URLEncoding.default, headers: nil).request?.url?.absoluteString
        let appleDeeplink = URL(string: escapedString ?? "http://maps.apple.com/")!
        UIApplication.shared.openURL(appleDeeplink)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMenu" {
            currentPath = IndexPath(row: 5, section: 0)
        }
    }
    
}



extension PersonViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}



extension PersonViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "info")!
        (cell.viewWithTag(1) as? UILabel)?.text = items[indexPath.row].title
        (cell.viewWithTag(2) as? UIImageView)?.image = items[indexPath.row].icon
        (cell.viewWithTag(1) as? UILabel)?.textColor = items[indexPath.row].action == nil ? .black : blueColor
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        items[indexPath.row].action?()
    }

}

extension PersonViewController { //menu actions
    func tableView(_ tableView: UITableView, shouldShowMenuForRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, canPerformAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return action == #selector(copy(_:))
    }
    
    func tableView(_ tableView: UITableView, performAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) {
        UIPasteboard.general.string = items[indexPath.row].title
    }

    
}
