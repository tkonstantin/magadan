//
//  PhonebookViewController.swift
//  Magadan
//
//  Created by Константин on 26.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit

class PhonebookViewController: SearchableViewController {
    
    class func makeOne() -> PhonebookViewController {
        let events = (UIStoryboard(name: "Phonebook", bundle: nil).instantiateInitialViewController() as! UINavigationController).viewControllers.first as! PhonebookViewController
        return events
    }
    
    
    var currentParent: Category?
    var currentItems: [Category] = []
    
    
    @IBOutlet weak var tableItems: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTable()
        if currentParent == nil {
            currentItems = PhoneBookParser.instance.allCategories
        } else {
            currentItems = currentParent?.categories ?? []
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMenu" {
            currentPath = IndexPath(row: 5, section: 0)
        }
    }
    
    
}

extension PhonebookViewController: UITableViewDelegate, UITableViewDataSource {
    
    fileprivate func configureTable() {
        tableItems.delegate = self
        tableItems.dataSource = self
        tableItems.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        (cell.viewWithTag(1) as? UILabel)?.text = currentItems[indexPath.row].title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentCategory = currentItems[indexPath.row]
        guard currentCategory.categories.count != 0 || currentCategory.head_persons.count != 0 || currentCategory.stuff_persons.count != 0 else { return }
        
        if currentCategory.phones.count == 0 && currentCategory.persons.keys.count == 0 {
            let new = PhonebookViewController.makeOne()
            new.currentParent = currentCategory
            new.navigationItem.setLeftBarButtonItems([], animated: false)
            new.navigationItem.title = currentCategory.title
            self.navigationController?.pushViewController(new, animated: true)
        } else {
            let new = DivisionViewController.makeOne()
            new.currentParent = currentCategory
            new.navigationItem.setLeftBarButtonItems([], animated: false)
            new.navigationItem.title = currentCategory.title
            self.navigationController?.pushViewController(new, animated: true)
        }
    }
    
}
