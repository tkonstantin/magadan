//
//  DivisionViewController.swift
//  Magadan
//
//  Created by Константин on 31.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import MessageUI
import Alamofire

typealias phonebookItem = (icon: UIImage, title: String, action: (()->Void)?)

extension String {
    var rawPhone: String {
        get {
            let allowed_characters = "1234567890"
            return String(self.filter({allowed_characters.contains($0)}))
        }
    }
}


class DivisionViewController: RotatableViewController {
    
    class func makeOne() -> DivisionViewController {
        let new = UIStoryboard(name: "Phonebook", bundle: nil).instantiateViewController(withIdentifier: "DivisionViewController") as! DivisionViewController
        return new
    }
    
    @IBOutlet weak var tableItems: UITableView!
    
    
    var currentParent: Category?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTable()

    }
    
    func call(to: String) {
        let phone = to.rawPhone
        guard let url = URL(string: "tel://\(phone)") else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func email(to: String) {
        let compose = MFMailComposeViewController()
        compose.mailComposeDelegate = self
        compose.setToRecipients([to])
        compose.mailComposeDelegate = self
        self.present(compose, animated: true, completion: nil)
    }
    
    func openMap(to: String) {
        
       let escapedString =  Alamofire.request("http://maps.apple.com/", method: .get, parameters: ["q":  to], encoding: URLEncoding.default, headers: nil).request?.url?.absoluteString
        let appleDeeplink = URL(string: escapedString ?? "http://maps.apple.com/")!
        UIApplication.shared.openURL(appleDeeplink)
    }
    
    
    
    lazy var phoneItems: [[phonebookItem]] = {
        var summ_result: [[phonebookItem]] = []
        for phone in currentParent?.phones ?? [] {
            var result: [phonebookItem] = []
            if !phone.phone.isEmpty {
                result.append((icon: #imageLiteral(resourceName: "phone_icon"), title: phone.phone, action: { self.call(to: phone.phone) }))
            }
            if !phone.phone2.isEmpty && phone.phone2.rawPhone != phone.phone.rawPhone {
                result.append((icon: #imageLiteral(resourceName: "phone_icon"), title: phone.phone2, action: { self.call(to: phone.phone2) }))
            }
            if !phone.fax.isEmpty {
                result.append((icon: #imageLiteral(resourceName: "fax_icon"), title: phone.fax, action: nil))
            }
            
            if !phone.email.isEmpty {
                result.append((icon: #imageLiteral(resourceName: "mail_icon"), title: phone.email, action: { self.email(to: phone.email) }))
            }
            
            if !phone.email.isEmpty && phone.email2.lowercased() != phone.email.lowercased() {
                result.append((icon: #imageLiteral(resourceName: "mail_icon"), title: phone.email2, action: { self.email(to: phone.email2) }))
            }
            
            if !phone.schedule.isEmpty {
                result.append((icon: #imageLiteral(resourceName: "clock_icon"), title: "График приёма:\n" + phone.schedule, action: nil))
            }
            
            if !phone.descript.isEmpty {
                result.append((icon: #imageLiteral(resourceName: "about_icon"), title: phone.descript, action: nil))
            }
            
            if !phone.address.isEmpty {
                result.append((icon: #imageLiteral(resourceName: "map_icon"), title: phone.address, action: { self.openMap(to: phone.address) }))
            }
            summ_result.append(result)
        }
        
        return summ_result
    }()
    
    lazy var subdivisionItems: [Category] = {
        return currentParent?.categories ?? []
    }()
    
    lazy var heads: [Person] = {
        return currentParent?.head_persons ?? []
    }()
    
    lazy var stuff: [Person] = {
        return currentParent?.stuff_persons ?? []
    }()
    
    lazy var subdivisionsCorrecter: Int =  {
        return phoneItems.count + min(heads.count, 1) + min(stuff.count, 1)
    }()
    
    
    enum sectionTypes {
        case phones
        case heads
        case stuff
        case subdivision
    }
    
    func title(type: sectionTypes, for section: Int) -> String {
        switch type {
        case .phones:
            return currentParent?.phones[section].title ?? ""
        case .heads:
            return "Руководство"
        case .stuff:
            return "Сотрудники"
        case .subdivision:
            return subdivisionItems[section - subdivisionsCorrecter].title
        }
    }
    
    struct RowContent {
        var title: String!
        var subtitle: String?
        var action: (()->Void)?
    }
    
    func rows(type: sectionTypes, for section: Int) -> [RowContent] {
        switch type {
        case .phones:
            return [RowContent(title: "", subtitle: nil, action: nil)] + phoneItems[section].map({RowContent(title: $0.title, subtitle: nil, action: $0.action)})
        case .heads:
            return [RowContent(title: "", subtitle: nil, action: nil)] + heads.map({RowContent(title: $0.fio, subtitle: $0.post, action: nil)})
        case .stuff:
            return [RowContent(title: "", subtitle: nil, action: nil)] + stuff.map({RowContent(title: $0.fio, subtitle: $0.post, action: nil)})
        case .subdivision:
            return [RowContent(title: subdivisionItems[section - subdivisionsCorrecter].title, subtitle: nil, action: nil)]
        }
    }
    
    
    func sectionType(for section: Int) -> sectionTypes {
        let phoneItemsCount = phoneItems.count
        
        guard section >= phoneItemsCount else { return .phones}
        
        switch section {
        case phoneItemsCount:
            if heads.count != 0 {
                return .heads
            } else if stuff.count != 0 {
                return .stuff
            } else {
                return .subdivision
            }
        case phoneItemsCount + (heads.count == 0 ? 0 : 1):

             if stuff.count != 0 {
                return .stuff
            } else {
                return .subdivision
            }
        default:
            return .subdivision
        }
        
    }
    
    
    var openedSections: [Int] = [0]
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMenu" {
            currentPath = IndexPath(row: 5, section: 0)
        }
    }
  
}


extension DivisionViewController: UITableViewDelegate, UITableViewDataSource {
    
    func configureTable() {
        tableItems.delegate = self
        tableItems.dataSource = self
        
        for section in 0..<self.tableItems.numberOfSections {
            if sectionType(for: section) == .heads {
                openedSections.append(section)
            }
        }
        self.tableItems.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return subdivisionsCorrecter + subdivisionItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard openedSections.contains(section) else { return 1 }
        return rows(type: sectionType(for: section), for: section).count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: indexPath.section >= subdivisionsCorrecter ? "division" : "header")!
            (cell.viewWithTag(1) as? UILabel)?.text = title(type: sectionType(for: indexPath.section), for: indexPath.section)
            (cell.viewWithTag(3) as? UIImageView)?.transform = CGAffineTransform(rotationAngle: self.openedSections.contains(indexPath.section) ? .pi : 0)
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: sectionType(for: indexPath.section) == .phones ? "info" : "usual")!
        
        let currentRow = rows(type: sectionType(for: indexPath.section), for: indexPath.section)[indexPath.row]
        (cell.viewWithTag(1) as? UILabel)?.text = currentRow.title
        (cell.viewWithTag(1) as? UILabel)?.textColor = currentRow.action == nil ? .black : blueColor
        (cell.viewWithTag(3) as? UILabel)?.text = currentRow.subtitle
        guard phoneItems.count > indexPath.section, phoneItems[indexPath.section].count > indexPath.row - 1 else { return cell }
        (cell.viewWithTag(2) as? UIImageView)?.image = (phoneItems[indexPath.section])[indexPath.row - 1].icon
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section >= subdivisionsCorrecter {
            let currentCategory = subdivisionItems[indexPath.section - subdivisionsCorrecter]
            guard subdivisionItems.count != 0 || heads.count != 0 || stuff.count != 0 else { return }
            
            
            if currentCategory.phones.count == 0 && currentCategory.persons.keys.count == 0 {
                let new = PhonebookViewController.makeOne()
                new.currentParent = currentCategory
                new.navigationItem.setLeftBarButtonItems([], animated: false)
                new.navigationItem.title = currentCategory.title
                self.navigationController?.pushViewController(new, animated: true)
            } else {
                let new = DivisionViewController.makeOne()
                new.currentParent = currentCategory
                new.navigationItem.setLeftBarButtonItems([], animated: false)
                new.navigationItem.title = currentCategory.title
                self.navigationController?.pushViewController(new, animated: true)
            }
            return
        }
        
        if indexPath.row == 0 {
            if let index = openedSections.index(of: indexPath.section) {
                openedSections.remove(at: index)
            } else {
                openedSections.append(indexPath.section)
            }
            self.tableItems.reloadSections([indexPath.section], with: .automatic)
        } else if sectionType(for: indexPath.section) == .phones {
            rows(type: sectionType(for: indexPath.section), for: indexPath.section)[indexPath.row].action?()
        } else if sectionType(for: indexPath.section) == .heads {
            self.navigationController?.pushViewController(PersonViewController.makeOne(with: heads[indexPath.row - 1], isHead: true), animated: true)
        } else if sectionType(for: indexPath.section) == .stuff {
            self.navigationController?.pushViewController(PersonViewController.makeOne(with: stuff[indexPath.row - 1], isHead: false), animated: true)
        }
        
        

    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == subdivisionsCorrecter {
            return 44
        } else {
            return 8
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == subdivisionsCorrecter {
            let header = UIView()
            header.backgroundColor = .clear
            let label = UILabel(frame: CGRect(x: 16, y: 8, width: self.view.bounds.width-32, height: 36))
            label.textAlignment = .left
            label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
            label.textColor = UIColor.lightGray
            label.text = "Подразделения".uppercased()
            header.addSubview(label)
            return header
        } else {
            return nil
        }
    }

}



extension DivisionViewController { //menu actions
    func tableView(_ tableView: UITableView, shouldShowMenuForRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, canPerformAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return action == #selector(copy(_:)) && indexPath.row != 0
    }
    
    func tableView(_ tableView: UITableView, performAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) {
        UIPasteboard.general.string = rows(type: sectionType(for: indexPath.section), for: indexPath.section)[indexPath.row].title
    }
    
    
}



func ==(f: Category, s: Category) -> Bool {
    return f.id == s.id
}

func ==(f: [Category], s: [Category]) -> Bool {
    return f.map({$0.id}) == s.map({$0.id})
}


extension DivisionViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}


