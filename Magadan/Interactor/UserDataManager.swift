//
//  UserDataManager.swift
//  Magadan
//
//  Created by Константин on 13.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import UIKit


class UserDataManager {
    static let shared = UserDataManager()
    
    var isAuthorized: Bool {
        get {
            return currentUser != nil
        }
    }
    
    private let currentUserKeychainKey = NSString(string: "129huaisldoil3esd")
    
    var currentUser: User! {
        get {
            if let data = KeychainService.loadData(for: currentUserKeychainKey) as Data? {
                return NSKeyedUnarchiver.unarchiveObject(with: data) as? User
            } else {
                return nil
            }
        } set {
            KeychainService.save(data: newValue == nil ? NSData() : NSData(data: NSKeyedArchiver.archivedData(withRootObject: newValue)), for: currentUserKeychainKey)
        }
    }
    
    var onboardingShowned: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "onboardingShowned_012")
        } set {
            UserDefaults.standard.set(newValue, forKey: "onboardingShowned_012")
            _ = UserDefaults.standard.synchronize()
        }
    }
    
    var cityImages: [TimeRelatedImage] = []
    
    var currentCityImage: TimeRelatedImage? {
        get {
            let currentHour = Date().currentHour()
            let currentMinute = Date().minute()
            
            var lastImage: TimeRelatedImage? = cityImages.last
            
            for image in cityImages.sorted(by: {$0.hour < $1.hour}) {
                if image.hour < currentHour || (image.hour == currentHour && image.minute < currentMinute) {
                    lastImage = image
                }
            }
            
            return lastImage ?? cityImages.first
        }
    }
    
    var cityImagePlaceholder: UIImage? {
        get {
            let fm = FileManager.default
            if let url = fm.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("placehlder_city_image.jpg") {
                return UIImage(contentsOfFile: url.path) ?? UIImage(named: "city_img")
            } else {
                return UIImage(named: "city_img")
            }
        } set {
            guard newValue != nil else { return }
            let fm = FileManager.default
            if let url = fm.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("placehlder_city_image.jpg") {
                try? UIImageJPEGRepresentation(newValue!, 1)?.write(to: url)
            }
        }
    }

}
