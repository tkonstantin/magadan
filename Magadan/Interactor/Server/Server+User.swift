//
//  Server+User.swift
//  Magadan
//
//  Created by Константин on 17.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import Alamofire

extension Server {
    
    
    func getCurrentUser(completion: (()->Void)?=nil) {
        guard let sid = self.sessionID else { completion?(); return }
        Alamofire.request(url_paths.base + url_paths.users + sid, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let code = response.response?.statusCode, code == 401 {
                completion?()
                self.logout()
                return;
            }
            if let json = response.result.value as? NSDictionary {
                UserDataManager.shared.currentUser = User(with: json)
            }
            completion?()
        }
    }
    
    func setCurrentUser(completion: (()->Void)?=nil) {
        guard let sid = self.sessionID else { return }
        
        Alamofire.request(url_paths.base + url_paths.users + sid, method: .post, parameters: UserDataManager.shared.currentUser.dictToSave, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion?()
        }
    }
    
    
    func getAvailableDistricts(completion: @escaping () -> Void) {
        guard availableProfileDistricts.count == 0 else { completion(); return }
        Alamofire.request(url_paths.base + url_paths.availableDistricts, headers: header).responseJSON { (response) in

            if let json = response.result.value as? [NSDictionary] {
                for dict in json {
                    self.availableProfileDistricts.append(District(with: dict))
                }
            }
            completion()
        }
    }
    
    func getRegionsList(completion: @escaping ()->Void) {
        Alamofire.request(url_paths.base + url_paths.regionsList, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var regions: [Region] = []
            if let json = response.result.value as? [NSDictionary] {
                for dict in json {
                    regions.append(Region(with: dict))
                }
            }
            regions.sort(by: {$0.title < $1.title})
            if let index = regions.index(where: {$0.id == Region.magadan_id}) {
                regions.insert(regions.remove(at: index), at: 0)
            }
            self.availableRegions = regions
            completion()
        }
    }
    
}

class Region: SimplePickerObject {
    
    static let magadan_id = "55"

    var isMagadan: Bool {
        get {
            return id == Region.magadan_id
        }
    }
    
}
