//
//  Server+Questions&Ideas.swift
//  Magadan
//
//  Created by Константин on 17.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import Alamofire

extension Server {
    
    //Ideas
    
    func getIdeas(page: Int, for theme: Theme, completion: @escaping ([Idea])->Void) {
        var ideas: [Idea] = []
        Alamofire.request(url_paths.base + url_paths.ideas + theme.id, method: .get, parameters: ["offset": page*10, "limit": 10], encoding: URLEncoding.default, headers: self.header).responseJSON { (response) in
            if let json = response.result.value as? [NSDictionary] {
                for dict in json {
                    ideas.append(Idea(with: dict))
                }
            }
            completion(ideas)
        }
    }
    
    func getMyIdeas(completion: @escaping ([Idea])->Void) {
        guard let sid = self.sessionID else { completion([]); return }
        var ideas: [Idea] = []
        Alamofire.request(url_paths.base + url_paths.users + sid + "/suggestions", method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let json = response.result.value as? [NSDictionary] {
                for dict in json {
                    ideas.append(Idea(with: dict))
                }
            }
            completion(ideas)
        }
    }
    
    
    
    func getFullIdea(_ idea: Idea, completion: @escaping (Idea?)->Void) {
        Alamofire.request(url_paths.base + url_paths.ideas + idea.theme_id + "/\(idea.id)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let json = response.result.value as? NSDictionary {
                completion(Idea(with: json))
            } else {
                completion(nil)
            }
        }
    }
    
    func getFullIdea(with id: String, theme_id: String, completion: @escaping (Idea?)->Void) {
        Alamofire.request(url_paths.base + url_paths.ideas + theme_id + "/\(id)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let json = response.result.value as? NSDictionary {
                completion(Idea(with: json))
            } else {
                completion(nil)
            }
        }
    }
    
    
    func setIdea(_ title: String, for theme: Theme?, completion: @escaping (String?)->Void) {
        guard theme != nil else { completion(nil); return }
        Alamofire.request(url_paths.base + url_paths.ideas + theme!.id, method: .post, parameters: ["title": title], encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if let json = response.result.value as? NSDictionary {
                completion(json["id"] as? String)
            }
        }
    }
    


    //Questions
    
    
    func getQuestions(page: Int, for theme: Theme, completion: @escaping ([Idea])->Void) {
        var ideas: [Idea] = []
        Alamofire.request(url_paths.base + url_paths.questions + theme.id, method: .get, parameters: ["offset": page*10, "limit": 10], encoding: URLEncoding.default, headers: self.header).responseJSON { (response) in
            if let json = response.result.value as? [NSDictionary] {
                for dict in json {
                    ideas.append(Idea(with: dict))
                }
            }
            completion(ideas)
        }
    }
    
    
    
    func getMyQuestions(completion: @escaping ([Idea])->Void) {
        guard let sid = self.sessionID else { completion([]); return }
        var ideas: [Idea] = []
        Alamofire.request(url_paths.base + url_paths.users + sid + "/questions", method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in

            if let json = response.result.value as? [NSDictionary] {
                for dict in json {
                    ideas.append(Idea(with: dict))
                }
            }
            completion(ideas)
        }
    }
    
    func getFullQuestion(_ idea: Idea, completion: @escaping (Idea?)->Void) {
        Alamofire.request(url_paths.base + url_paths.questions + idea.theme_id + "/\(idea.id)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let json = response.result.value as? NSDictionary {
                completion(Idea(with: json))
            } else {
                completion(nil)
            }
        }
    }
    
    
    func getFullQuestion(with id: String, theme_id: String, completion: @escaping (Idea?)->Void) {
        Alamofire.request(url_paths.base + url_paths.questions + theme_id + "/\(id)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let json = response.result.value as? NSDictionary {
                completion(Idea(with: json))
            } else {
                completion(nil)
            }
        }
    }
    
    func setQuestion(_ title: String, for theme: Theme?, completion: @escaping (String?)->Void) {
        guard theme != nil else { completion(nil); return }
        
        Alamofire.request(url_paths.base + url_paths.questions + theme!.id, method: .post, parameters: ["title": title], encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if let json = response.result.value as? NSDictionary {
                completion(json["id"] as? String)
            }
        }
    }
    
    
    func likeIdea(_ idea: Idea, completion: @escaping (Int, Bool, Bool)->Void) {
        completion(idea.likes_count + (idea.isLiked ? -1 : 1), !idea.isLiked, false)

        Alamofire.request(url_paths.base + url_paths.ideas + idea.theme_id + "/\(idea.id)/like", method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in

            if let json = response.result.value as? NSDictionary {
                completion(json["likes"] as? Int ?? Int(json["likes"] as? String ?? "0") ?? idea.likes_count, json["isLiked"] as? Bool ?? false, true)
            } else {
                completion(idea.likes_count, idea.isLiked, false)
            }
        }
    }
    
    func likeQuestion(_ question: Idea, completion: @escaping (Int, Bool, Bool)->Void) {
        completion(question.likes_count + (question.isLiked ? -1 : 1), !question.isLiked, false)

        Alamofire.request(url_paths.base + url_paths.questions + question.theme_id + "/\(question.id)/like", method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let json = response.result.value as? NSDictionary {
                completion(json["likes"] as? Int ?? Int(json["likes"] as? String ?? "0") ?? question.likes_count, json["isLiked"] as? Bool ?? false, true)
            } else {
                completion(question.likes_count, question.isLiked, false)
            }
        }
    }
    
    
    func loadQuestionsCount() {
        Alamofire.request(url_paths.base + url_paths.questionsCount, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let json = response.result.value as? [NSDictionary] {
                for dict in json {
                    self.quesionsCountForTheme[dict["id"] as? String ?? String(dict["id"] as? Int ?? 0)] = dict["count"] as? String ?? String(dict["count"] as? Int ?? 0)
                }
            }
        }
    }
    
    
    
    
}
