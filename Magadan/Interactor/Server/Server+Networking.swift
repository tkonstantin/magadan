//
//  Server+Netrorking.swift
//  Magadan
//
//  Created by Константин on 25.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Reachability

let networkNotification = NSNotification.Name("network_appeared")

class NetworkManager {
    
    static let shared = NetworkManager()
    
    fileprivate let reachability = Reachability()
    fileprivate var connectionIntterrupted = false
    
    func startMonitoringNetwork() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged(_:)), name: .reachabilityChanged, object: self.reachability)
        try? self.reachability?.startNotifier()
    }
    
    @objc private func reachabilityChanged(_ note: Notification) {
        let reachability = note.object as! Reachability
        
        if reachability.connection == .none {
            connectionIntterrupted = true
            UIApplication.shared.keyWindow?.rootViewController?.showOKAlert(title: "Нет сети. Проверьте соединение", message: nil)
        } else if connectionIntterrupted {
            connectionIntterrupted = false
            Server.makeRequest.initialLoading {
                asyncAfter {
                    NotificationCenter.default.post(name: networkNotification, object: nil)
                }
            }
        }
    }

    
}

