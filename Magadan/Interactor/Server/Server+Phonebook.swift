//
//  Server+Phonebook.swift
//  Magadan
//
//  Created by Константин on 17.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import Alamofire

extension Server {
    
    func getAllPhonebookItems(completion: @escaping ()->Void) {
        Alamofire.request(url_paths.base + url_paths.phonebook + "/offline", method: .get, parameters: ["v": Int64(PhoneBookParser.instance.previousDownloadDate.timeIntervalSince1970)], encoding: URLEncoding.default, headers: self.header).responseJSON { (response) in
            PhoneBookParser.instance.previousDownloadDate = Date()
            PhoneBookParser.instance.update(with: response.result.value as? NSDictionary)
            asyncAfter {
                completion()
            }
        }
    }
    
}
