//
//  Server+Wheather.swift
//  Magadan
//
//  Created by Константин on 17.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import Alamofire
import Solar
import CoreLocation


extension Server {
    func getWheathers( completion: @escaping ([(key: String, degrees: Int, icon: String)])->Void) {
        
        Alamofire.request("https://49gov.ru/common/api/2/weather/", method: .get, parameters: [:], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            var wheaters: [(key: String, degrees: Int, icon: String)] = []
            if let json = response.result.value as? [NSDictionary] {
                for w in json {
                    if let key = w.allKeys.first as? String {
                        let current_observation = (w[key] as? NSDictionary)?["current_observation"] as? NSDictionary
                        let degrees = current_observation?["temp_c"] as? Double ?? 300
                        var icon = current_observation?["icon"] as? String ?? "clear"
                        if icon.isEmpty {
                            icon = "clear"
                        }
                        if (icon == "clear" || icon == "partlycloudy") && !self.isInSunlight() {
                            icon += "_night"
                        }
                        wheaters.append((key: key, degrees: Int(degrees), icon: icon))
                    }
                }
            }
            completion(wheaters)
        }
    }
    
    func getCurrentWeatherHourly( completion: @escaping ()->Void) {
        let apiKey = "4e8e64b1a97ef0cb"//"c7add0cdd6fb72f3"
        
        guard currentDegrees == 300 else { completion(); return }
        
        Alamofire.request("http://api.wunderground.com/api/\(apiKey)/hourly/geolookup/conditions/q/\(currentLat),\(currentLon).json").responseJSON { (response) in
            print("wheather response = \(response)")
            
            if let json = response.result.value as? NSDictionary, let hourly = json["hourly_forecast"] as? [NSDictionary], let current_observation = json["current_observation"] as? NSDictionary {
                                
                
                self.currentDegrees = Int(current_observation["temp_c"] as? Double ?? 300)
                self.currentIcon = current_observation["icon"] as? String ?? "clear"
    
                
                if (self.currentIcon == "clear" || self.currentIcon == "partlycloudy") && !self.isInSunlight() {
                    self.currentIcon += "_night"
                }
                
                
                let morningDict = hourly.first(where: { dict in
                    let hour = (dict["FCTTIME"] as? NSDictionary)?["hour"] as? Int ?? Int((dict["FCTTIME"] as? NSDictionary)?["hour"] as? String ?? "0") ?? 0
                    return (hour >= 6 && hour < 12) && hour >= Date().hour()
                })
                
                let morningIcon = morningDict?["icon"] as? String ?? "no"
                let morningDegrees = (morningDict?["temp"] as? NSDictionary)?["metric"] as? String ?? "\(self.currentDegrees)"
                
                
                let dayDict = hourly.first(where: { dict in
                    let hour = (dict["FCTTIME"] as? NSDictionary)?["hour"] as? Int ?? Int((dict["FCTTIME"] as? NSDictionary)?["hour"] as? String ?? "0") ?? 0
                    return (hour >= 12 && hour < 18) && hour >= Date().hour()
                })
                
                let dayIcon = dayDict?["icon"] as? String ?? "no"
                let dayDegrees = (dayDict?["temp"] as? NSDictionary)?["metric"] as? String ?? "\(self.currentDegrees)"
                
                
                
                let eveningDict = hourly.first(where: { dict in
                    let hour = (dict["FCTTIME"] as? NSDictionary)?["hour"] as? Int ?? Int((dict["FCTTIME"] as? NSDictionary)?["hour"] as? String ?? "0") ?? 0
                    return (hour >= 18 && hour < 23) && hour >= Date().hour()
                })
                
                var eveningIcon = eveningDict?["icon"] as? String ?? "no"
                if (eveningIcon == "clear" || eveningIcon == "partlycloudy") {
                    eveningIcon += "_night"
                }
                
                let eveningDegrees = (eveningDict?["temp"] as? NSDictionary)?["metric"] as? String ?? "\(self.currentDegrees)"
                
                
                
                let nightDict = hourly.first(where: { dict in
                    let hour = (dict["FCTTIME"] as? NSDictionary)?["hour"] as? Int ?? Int((dict["FCTTIME"] as? NSDictionary)?["hour"] as? String ?? "0") ?? 0
                    return (hour >= 23 || hour < 6) && hour >= Date().hour()
                })
                
                var nightIcon = nightDict?["icon"] as? String ?? "no"
                if (nightIcon == "clear" || nightIcon == "partlycloudy") {
                    nightIcon += "_night"
                }
                let nightDegrees = (nightDict?["temp"] as? NSDictionary)?["metric"] as? String ?? "\(self.currentDegrees)"
                
                
                self.currentWheathers = [
                    (dayPart: .morning, degrees: morningDegrees, iconName: morningIcon),
                    (dayPart: .day, degrees: dayDegrees, iconName: dayIcon),
                    (dayPart: .evening, degrees: eveningDegrees, iconName: eveningIcon),
                    (dayPart: .night, degrees: nightDegrees, iconName: nightIcon),
                ]
                
                completion()
            } else {
                completion()
            }
        }
        
    }
    
    
    
    func setCurrentCoordinates(lat: Double, lon: Double) {
        var correctedLat = lat
        var correntedLon = lon
        
        if correctedLat < 58.8 || correctedLat > 66.4 || correntedLon < 144.7 || correntedLon > 163.2 {
            correctedLat = 59.560665
            correntedLon = 150.811228
        }
        
        currentLat = correctedLat
        currentLon = correntedLon
        
    }
    
    
    
    var currentDegrees: Int {
        get {
            if Date().timeIntervalSince1970 - currentDegreesTuple.date.timeIntervalSince1970 < 3600 {
                return currentDegreesTuple.value
            } else {
                return 300
            }
        } set {
            currentDegreesTuple = (date: Date(), value: newValue)
        }
    }
    
    
    func isInSunlight() -> Bool {
        let solar = Solar(for: Date(), coordinate: CLLocationCoordinate2D(latitude: currentLat, longitude: currentLon))
        return solar?.isDaytime ?? true
    }
    
    func loadCityImages(completion: (([TimeRelatedImage])->Void)?=nil) {
        var result: [TimeRelatedImage?] = []
        Alamofire.request(url_paths.base + url_paths.cityImages, method: .get, headers: header).responseJSON { (response) in
            for dict in (response.result.value as? [NSDictionary] ?? []) {
                result.append(TimeRelatedImage(with: dict))
            }
            
            let savedResult = result.compactMap({$0})
            UserDataManager.shared.cityImages = savedResult
            completion?(savedResult)
        }
    }
    
    
}



class TimeRelatedImage {
    var link: String!
    var startTime: String!
    
    init?(with json: NSDictionary) {
        link = json["image"] as? String
        startTime = json["time"] as? String
        if link == nil || startTime == nil {
            return nil
        }
    }
    
    
    var comps: [String] {
        get {
            return startTime.components(separatedBy: ":")
        }
    }
    var hour: Int {
        get {
            return Int(comps.first ?? "0") ?? 0
        }
    }
    var minute: Int {
        get {
            return Int(comps.last ?? "0") ?? 0
        }
    }
}
