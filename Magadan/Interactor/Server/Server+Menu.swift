//
//  Server+Menu.swift
//  Magadan
//
//  Created by Константин on 18.01.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import Alamofire


extension Server {
    
    func loadMenuItems(completion: @escaping ()->Void) {
        
        Alamofire.request(url_paths.base + url_paths.menu, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var items: [MenuItem] = []
            
            if let json = response.result.value as? [NSDictionary] {
                for dict in json {
                    items.append(MenuItem(with: dict))
                }
            }
            
            menuItems[0] += items            
        }
    }
    
    func loadSubmenuItems(item: MenuItem, completion: @escaping ([SubMenuItem])->Void) {
        
        Alamofire.request(url_paths.base + url_paths.menu + "/" + item.id!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var subItems: [SubMenuItem] = []
            
            if let json = response.result.value as? [NSDictionary] {
                for dict in json {
                    subItems.append(SubMenuItem(with: dict))
                }
            }
            
            if subItems.count == 1 {
                subItems[0].isSingle = true
            } else {
                subItems.forEach({$0.isSingle = false})
            }
            
            completion(subItems)
        }
    }
    
    func checkTranslations(completion: @escaping (Bool)->Void) {
        Alamofire.request(url_paths.base + url_paths.checkTranslations, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            completion((response.result.value as? NSDictionary)?["live"] as? Bool ?? false)
        }
    }
}
