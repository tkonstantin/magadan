//
//  Server+Pushes.swift
//  Magadan
//
//  Created by Константин on 24.01.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Alamofire
import Foundation

class PushSettings: NSObject, NSCoding {
    
    enum states: Int {
        case all = 2
        case important = 1
        case never = 0
        
        var label: String {
            get {
                switch self {
                case .all: return "Все"
                case .important: return "Важные"
                case .never: return "Никакие"
                    
                }
            }
        }
        
    }
    var isEnabled = true
    
    var news: states = .important
    var events: states = .important
    var live: states = .important
    var vote: states = .important
    
    
    
    override init() {
        news = .important
        events = .important
        live = .important
        vote = .important
        isEnabled = true
    }
    
    
    init(with json: NSDictionary) {
        self.news = states(rawValue: PushSettings.int(from: json["news"])) ?? .important
        self.events = states(rawValue: PushSettings.int(from: json["events"])) ?? .important
        self.live = states(rawValue: PushSettings.int(from: json["live"])) ?? .important
        self.vote = states(rawValue: PushSettings.int(from: json["vote"])) ?? .important
        self.isEnabled = json["isEnabled"] as? Bool ?? true

    }
    
     private static func int(from any: Any?) -> Int {
        if let intValue = any as? Int {
            return intValue
        } else if let stringValue = any as? String {
            return Int(stringValue) ?? 0
        }
        return 0
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.news.rawValue, forKey: "news")
        aCoder.encode(self.events.rawValue, forKey: "events")
        aCoder.encode(self.live.rawValue, forKey: "live")
        aCoder.encode(self.vote.rawValue, forKey: "vote")
        aCoder.encode(self.isEnabled, forKey: "isEnabled")

    }
    
    required init?(coder aDecoder: NSCoder) {
        self.news = states(rawValue: aDecoder.decodeInteger(forKey: "news")) ?? .important
        self.events = states(rawValue: aDecoder.decodeInteger(forKey: "events")) ?? .important
        self.live = states(rawValue: aDecoder.decodeInteger(forKey: "live")) ?? .important
        self.vote = states(rawValue: aDecoder.decodeInteger(forKey: "vote")) ?? .important
        self.isEnabled = aDecoder.decodeObject(forKey: "isEnabled") as? Bool ?? true
    }
    
    var params: [String: Any] {
        get {
            return [
                "news": self.news.rawValue,
                "events": self.events.rawValue,
                "live": self.live.rawValue,
                "vote": self.vote.rawValue,
                "isEnabled": self.isEnabled,
            ]
        }
    }

}

extension Server {
    
    private static let kPushSettings = NSString(string: "PushSettings")
    
    static var pushSettings: PushSettings {
        get {
            
            if let data = KeychainService.loadData(for: kPushSettings) as Data? {
                return NSKeyedUnarchiver.unarchiveObject(with: data) as? PushSettings ?? PushSettings()
            }
            return PushSettings()
        } set {
            let data = NSKeyedArchiver.archivedData(withRootObject: newValue) as NSData
            KeychainService.save(data: data, for: kPushSettings)
        }
    }
    
    func sendPushToken(token: String) {
        
        Alamofire.request(url_paths.base + url_paths.pushes, method: .post, parameters: ["token": token], encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
        }
    }
    
    func setPushSettings(completion: @escaping (Bool)->Void) {
        Alamofire.request(url_paths.base + url_paths.updatePushes, method: .post, parameters: Server.pushSettings.params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(true)
        }
    }
    
    func getPushSettings(completion: @escaping ()->Void) {
        Alamofire.request(url_paths.base + url_paths.pushes, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let json = response.result.value as? NSDictionary {
                Server.pushSettings = PushSettings(with: json)
            }
            completion()

        }
    }
    
    
}
