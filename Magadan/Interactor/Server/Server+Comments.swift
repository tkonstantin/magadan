//
//  Server+Comments.swift
//  Magadan
//
//  Created by Константин on 08/10/2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import Alamofire

extension Server {
    
    func getComments(_ news: News?=nil, event: Event?=nil, idea: Idea?=nil, page _page: Int = 0, laterThan lt: String!=nil, earlierThan et: String!=nil, completion: @escaping ([Comment], Int)->Void) {
        var page = _page
        var currentPageSize = pageSize
        var params: [String: Any] = [:]
        if lt != nil, !lt.isEmpty {
            params["lt"] = lt!
        }
        if et != nil, !et.isEmpty {
            params["et"] = et!
            page = 0
            currentPageSize = 0
        }
        params["offset"] = page*currentPageSize
        params["limit"] = currentPageSize
        
        var id: String!
        var typePath: String!
        
        if news != nil || event != nil {
            typePath = url_paths.news
        } else if idea != nil {
            typePath = String(url_paths.ideas.dropLast())
        }
        
        if news != nil {
            id = news!.idString
        } else if event != nil {
            id = event!.idString
        } else if idea != nil {
            id = String(idea!.id)
        }
        
        guard id != nil && typePath != nil else { completion([], 1); return }
        
        var path = String(format: url_paths.base + "%@/%@/comments", typePath!, id!)
        if let theme = idea?.theme_id {
            path = String(format: url_paths.base + "%@/%@/%@/comments", typePath!, theme, id!)
        }
        
        
        Alamofire.request(path, method: .get, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            
            var result: [Comment?] = []
            
            if let json = response.result.value as? NSDictionary {
                for dict in (json["comments"] as? [NSDictionary] ?? []) {
                    result.append(Comment(with: dict))
                }
                
                completion(result.compactMap({$0}), json["pages"] as? Int ?? 1)

            } else if let jsons = response.result.value as? [NSDictionary] {
                for dict in jsons {
                    result.append(Comment(with: dict))
                }
                
                completion(result.compactMap({$0}), 1)
            } else {
                completion([], 1)
            }
           
        }
    }
    
    
    func leaveComment(_ news: News?=nil, event: Event?=nil, idea: Idea?=nil, text: String, completion: @escaping (Comment?)->Void) {
        let params = ["message": text]
        
        var id: String!
        var typePath: String!
        
        if news != nil || event != nil {
            typePath = url_paths.news
        } else if idea != nil {
            typePath = String(url_paths.ideas.dropLast())
        }
        
        if news != nil {
            id = news!.idString
        } else if event != nil {
            id = event!.idString
        } else if idea != nil {
            id = String(idea!.id)
        }
        
        
        
        guard id != nil && typePath != nil && !text.isEmpty else { completion(nil); return }
        
        var path = String(format: url_paths.base + "%@/%@/comments", typePath!, id!)
        if let theme = idea?.theme_id {
            path = String(format: url_paths.base + "%@/%@/%@/comments", typePath!, theme, id!)
        }
        
        Alamofire.request(path, method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            completion(Comment(with: response.result.value as? NSDictionary))
        }
    }
}
