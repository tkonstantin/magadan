//
//  Server+News.swift
//  Magadan
//
//  Created by Константин on 17.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import Alamofire

extension Server {
    
    
    func getNewsCategories(completion: @escaping ([NewsCategory])->Void) {
        var categories: [NewsCategory] = []
        Alamofire.request(url_paths.base + url_paths.newsCategories, method: .get, parameters: nil, encoding: URLEncoding.default, headers: self.header).responseJSON { (response) in
            if let json = response.result.value as? [NSDictionary] {
                for dict in json {
                    categories.append(NewsCategory(with: dict))
                }
            }
            
            let allCategory = NewsCategory(with: [:])
            allCategory.title = "ВСЕ"
            allCategory.id = 0
            completion([allCategory] + categories)
        }
    }
    
    
    func getNews(page: Int = 0, cat_id: Int, completion: @escaping ([News])->Void) {
        var params: [String: Any] = [:]
        params["offset"] = page*pageSize
        params["limit"] = pageSize
        
        if cat_id != 0 {
            params["category"] = cat_id
        }
        
        Alamofire.request(url_paths.base + url_paths.news, method: .get, parameters: params, encoding: URLEncoding.default, headers: self.header).responseJSON { (response) in
            var news: [News] = []
            if let json = response.result.value as? [NSDictionary] {
                for item in json {
                    news.append(News(with: item))
                }
            }
            completion(news)
        }
    }
    
    
    func getPinnedNews(completion: @escaping ([News])->Void) {
        var pinned: [News] = []
        Alamofire.request(url_paths.base + url_paths.pinnedNews, method: .get, parameters: nil, encoding: URLEncoding.default, headers: self.header).responseJSON { (response) in
            
            if let json = response.result.value as? [NSDictionary] {
                for item in json {
                    let new = News(with: item)
                    new.isBanner = true
                    pinned.append(new)
                }
            } else if let item = response.result.value as? NSDictionary {
                let new = News(with: item)
                new.isBanner = true
                pinned.append(new)
            }
            completion(pinned)
        }
    }
    
    func getNewsMore(_ news: News?=nil, id: String?=nil, completion: @escaping (News?)->Void) {
        Alamofire.request(url_paths.base + url_paths.news + "/\(news?.idString ?? id!)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: self.header).responseJSON { (response) in
            if let json = response.result.value as? NSDictionary {
                completion(News(with: json))
            } else {
                completion(news)
            }
        }
    }
    
    
    func likeNews(_ news: News, completion: @escaping (Int, Bool, Bool)->Void) {
        completion(news.likesCount + (news.isLiked ? -1 : 1), !news.isLiked, false)
        
        Alamofire.request(url_paths.base + url_paths.news + "/\(news.idString)" + "/like", method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let json = response.result.value as? NSDictionary {
                let likes = json["likes"] as? Int ?? Int(json["likes"] as? String ?? "0")
                completion(likes ?? news.likesCount, json["isLiked"] as? Bool ?? false, true)
            } else {
                completion(news.likesCount, news.isLiked, false)
            }
        }
    }
    
    
}
