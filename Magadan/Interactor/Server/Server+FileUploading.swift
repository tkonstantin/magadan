//
//  Server+FileUploading.swift
//  Magadan
//
//  Created by Константин on 18.01.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import Foundation
import Alamofire
import MobileCoreServices


extension Server {
    
    enum upload_types {
        case questions
        case ideas
        case reception
        
        func urlPath(with theme_id: String?=nil) -> String {
                switch self {
                case .questions: return url_paths.base + url_paths.questions + theme_id!
                case .ideas: return url_paths.base + url_paths.ideas + theme_id!
                case .reception: return url_paths.base + url_paths.receptionRequest
                }
        }
    }
    
    private static let fileSizeLimit: Int = 10*1024*1024
    
    ///Returns successfulness (true/false)
    func upload(images: [UIImage], files: [URL], for type: upload_types, id: String, theme_id: String?=nil, completion: @escaping (Bool)->Void) {
        
        Alamofire.upload(multipartFormData: { (formdata) in
            for index in 0..<images.count {
                let image = images[index]
                var compressionQuality: CGFloat = 1.0
                if let rawImgData = UIImageJPEGRepresentation(image, 1) {
                    if rawImgData.count > Server.fileSizeLimit {
                        compressionQuality = CGFloat(Server.fileSizeLimit)/CGFloat(rawImgData.count)
                    }
                }
                
                if let compressedData = UIImageJPEGRepresentation(image, compressionQuality) {
                    formdata.append(compressedData, withName: "file\(index+1)", fileName: "file\(index+1).jpg", mimeType: "image/jpeg")
                }
            }
            for index in 0..<files.count {
                let file = files[index]
                if let data = FileManager.default.contents(atPath: file.path) {
                    formdata.append(data, withName: file.deletingPathExtension().lastPathComponent, fileName: file.lastPathComponent, mimeType: file.mimeType)
                }
            }
        }, to: type.urlPath(with: theme_id) + "?upload=1&id=\(id)", method: .post, headers: header) { (res) in
            switch res {
            case .success(let request, _, _):

                request.responseJSON(completionHandler: { (response) in
                    if let json = response.result.value as? NSDictionary {
                        completion(json["success"] as? Bool ?? false)
                    } else {
                        completion(false)
                    }
                })
            case .failure(let error):
                print("upload file encoding failed with error: \(error)")
                completion(false);
                return
            }
        }
    }
}


extension URL {
    var mimeType: String {
        get {
            let url = NSURL(fileURLWithPath: self.path)
            let pathExtension = url.pathExtension
            
            if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
                if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                    return mimetype as String
                }
            }
            return "application/octet-stream"
        }
    }
}


