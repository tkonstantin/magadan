//
//  Server+URLPaths.swift
//  Magadan
//
//  Created by Константин on 17.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation

extension Server {
    
    enum url_paths {
        static let base = "https://49gov.ru/common/api/2"
        static let events = "/events"
        static let news = "/news"
        static let newsCategories = "/news/categories"
        static let pinnedNews = "/news/pinned"
        static let live = "/live"
        static let imagesPathPreffix = "https://49gov.ru"
        static let phonebook = "/phonebook"
        static let polls = "/vote"
        static let pollByID = "/vote/%@"
        static let questionsCount = "/feedback"
        static let ideas = "/feedback/suggestions/"
        static let questions = "/feedback/questions/"
        static let search = "/search"
        static let receptionRequest = "/feedback/desk/"
        static let receptionRecipients = "/feedback/authorities/"
        static let users = "/user/"
        static let userTreatments = "/treatments/count"
        static let availableDistricts = "/user/districts/"
        static let regionsList = "/user/regions/"
        static let menu = "/menu"
        static let pushes = "/push"
        static let updatePushes = "/push/update"
        static let checkTranslations = "/live/ongoing"
        static let cityImages = "/menu/images"
    }
    
}
