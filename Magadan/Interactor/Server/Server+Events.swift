//
//  Server+Events.swift
//  Magadan
//
//  Created by Константин on 17.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import Alamofire

extension Server {
    func getEvents(page: Int = 0, completion: @escaping ([Event])->Void) {
        var params: [String: Any] = [:]
        params["month"] = currentMonth
        params["year"] = currentYear
        params["full"] = true
//        params["limit"] = pageSize
//        params["offset"] = page*pageSize
        
        Alamofire.request(url_paths.base + url_paths.events, method: .get, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            var events: [Event] = []
            
            if let json = response.result.value as? [NSDictionary] {
                for item in json {
                    events.append(Event(with: item))
                }
            }
            completion(events)
        }
    }
    
    
    
    func getLive(completion: @escaping ([Event])->Void) {
        var lives: [Event] = []
        
        Alamofire.request(url_paths.base + url_paths.live, method: .get, parameters: nil, encoding: URLEncoding.default, headers: self.header).responseJSON { (response) in
            if let json = response.result.value as? [NSDictionary] {
                for item in json {
                    let new = Event(withLive: item)
                    lives.append(new)
                }
            }
            completion(lives)
        }
    }
    
    
    
    
    func getEventMore(_ event: Event?=nil, id: String?=nil, completion: @escaping (Event?)->Void) {
        Alamofire.request(url_paths.base + url_paths.events + "/\(event?.idString ?? id!)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: self.header).responseJSON { (response) in
            if let json = response.result.value as? NSDictionary {
                completion(Event(with: json))
            } else {
                completion(event)
            }
        }
    }
}
