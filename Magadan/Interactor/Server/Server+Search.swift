//
//  Server+Search.swift
//  Magadan
//
//  Created by Константин on 07.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import Alamofire

class SearchItem: NSObject, NSCoding {
    
    enum types {
        case news
        case events
        case persons
        case photo
        case video
        case questions
        case ideas
        case undefined
        case history
        
        init(rawValue: String) {
            switch rawValue {
            case "EVENTS": self = .events
            case "NEWS": self = .news
            case "PERSONS", "SPRAV_OGV": self = .persons
            case "PMO_PHOTO_GALLERY": self = .photo
            case "PMO_VIDEO_GALLERY": self = .video
            case "QUESTION": self = .questions
            case "SUGGESTION": self = .ideas
            case "HISTORY": self = .history
            default: self = .undefined
            }
        }
        
        var intValue: Int {
            get {
                switch self {
                case .news: return 1
                case .events: return 4
                case .persons: return 6
                case .photo: return 8
                case .video: return 7
                case .questions: return 9
                case .ideas: return 10
                default: return 0
                }
            }
        }
        
        var displayString: String {
            get {
                switch self {
                case .events: return "Мероприятия"
                case .news: return "Новости"
                case .persons: return "Справочник"
                case .photo: return "Фото"
                case .video: return "Видео"
                case .questions: return "Вопросы"
                case .ideas: return "Идеи"
                case .history: return "История поиска"
                default: return "Все"
                }
            }
        }
        
        var displayIcon: UIImage {
            get {
                switch self {
                case .events: return #imageLiteral(resourceName: "events_icon")
                case .news: return #imageLiteral(resourceName: "search_news_icon")
                case .persons: return #imageLiteral(resourceName: "phonebook_icon")
                case .photo: return #imageLiteral(resourceName: "search_photo_icon")
                case .video: return #imageLiteral(resourceName: "search_video_icon")
                case .questions: return #imageLiteral(resourceName: "ideas_menu_icon")
                case .ideas: return #imageLiteral(resourceName: "ideas_menu_icon")
                case .history: return #imageLiteral(resourceName: "search_recents_icon")
                default: return #imageLiteral(resourceName: "settings_help")
                }
            }
        }

    }

    var title = ""
    var rawType = ""
    var type: types = .undefined
    var date = Date()
    var id = ""
    var links: [String] = []
    var theme_id = ""
    var theme_title = ""
    
    init(with dict: NSDictionary) {
        id = dict["id"] as? String ?? ""
        title = dict["title"] as? String ?? ""
        date = Date(dateFromVeryStrangeString: dict["date"] as? String ?? "00000000000000")
        rawType = dict["type"] as? String ?? ""
        type = types(rawValue: rawType)
        theme_id = dict["theme_id"] as? String ?? String(dict["theme_id"] as? Int ?? 0)
        theme_title = dict["theme_title"] as? String ?? ""

        if let items = dict["photos"] as? [String] {
            self.links = items.map({Server.url_paths.imagesPathPreffix + $0})
        } else if let item = dict["src"] as? String {
            self.links = [item]
        }

    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.date, forKey: "date")
        aCoder.encode(self.rawType, forKey: "rawType")
        aCoder.encode(self.theme_id, forKey: "theme_id")
        aCoder.encode(self.theme_title, forKey: "theme_title")
        aCoder.encode(self.links, forKey: "links")

    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? String ?? ""
        self.title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
        self.date = aDecoder.decodeObject(forKey: "date") as? Date ?? Date()
        self.rawType = aDecoder.decodeObject(forKey: "rawType") as? String ?? ""
        type = types(rawValue: rawType)
        self.theme_id = aDecoder.decodeObject(forKey: "theme_id") as? String ?? ""
        self.theme_title = aDecoder.decodeObject(forKey: "theme_title") as? String ?? ""
        self.links = aDecoder.decodeObject(forKey: "links") as? [String] ?? []

    }
}

extension Server {
    
    func search_all(query: String, completion: @escaping ([SearchItem])->Void) {
        var items: [SearchItem] = []
        Alamofire.request(url_paths.base + url_paths.search, method: .get, parameters: ["q": query], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if let json = response.result.value as? NSDictionary {
                for dicts in json.allValues.flatMap({$0 as? [NSDictionary]}) {
                    for itemDict in dicts {
                        items.append(SearchItem(with: itemDict))
                    }
                }
            }
            completion(items)
        }
    }
    
    func searchBy(type: SearchItem.types, query: String, pageNumber: Int, completion: @escaping ([SearchItem])->Void) {
        guard type != .undefined else { completion([]); return }
        
        var items: [SearchItem] = []
        Alamofire.request(url_paths.base + url_paths.search + "/" + String(type.intValue), method: .get, parameters: ["q": query, "from": String(pageNumber + 2), "limit": "5"], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            if let dicts = response.result.value as? [NSDictionary] {
                for dict in dicts {
                    items.append(SearchItem(with: dict))
                }
            }
            completion(items)
        }
    }
}
