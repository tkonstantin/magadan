//
//  Server.swift
//  Magadan
//
//  Created by Константин on 19.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class Server {
    
    static let makeRequest = Server()
    
    var header: HTTPHeaders? {
        get {
            if let sid = self.currentAuthorizationSession?.session_id {
                return ["X-Auth-Token": sid, "udid": udid]
            } else {
                return ["udid": udid]
            }
        }
    }
    
    var sessionID: String? {
        get {
            return self.currentAuthorizationSession?.session_id
        }
    }
    
    var udid: String {
        get {
            if let stored = UserDefaults.standard.string(forKey: "udid") {
                return stored
            } else {
                UserDefaults.standard.set(UIDevice.current.identifierForVendor?.uuidString ?? "", forKey: "udid")
                _ = UserDefaults.standard.synchronize()
                return UIDevice.current.identifierForVendor?.uuidString ?? ""
            }
        } set {
            UserDefaults.standard.set(newValue, forKey: "udid")
            _ = UserDefaults.standard.synchronize()
        }
    }

    var isAuthorized: Bool {
        get {
            return currentAuthorizationSession != nil
        }
    }
    
    var isTermConfirmed: Bool {
        get {
            return termsConfirmedValue
        } set {
            termsConfirmedValue = newValue
        }
    }
    
    
    
    
    let pageSize = 30
    var currentMonth: Int = 13
    var currentYear: Int = Date().dateToComponents().year
    var availableProfileDistricts: [District] = []
    var availableRegions: [Region] = []
    var newsCategories: [NewsCategory] = []
    var currentCategoryId: Int = 0
    var receptionRecipients: [Recipient] = []
    var currentLat: Double = 0
    var currentLon: Double = 0
    var currentDegreesTuple = (date: Date(timeIntervalSince1970: 1), value: 300)
    var currentIcon: String = "clear"
    var currentWheathers: [wheather] = []
    var quesionsCountForTheme: [String: String] = [:]

    
    var savedNewsForCategory: [Int: [News]] = [:]
    

    private var termsConfirmedValue: Bool {
        get {
            return UserDefaults.standard.value(forKey: "termsConfirmedValue") as? Bool ?? false
        } set {
            UserDefaults.standard.set(newValue, forKey: "termsConfirmedValue")
            _ = UserDefaults.standard.synchronize()
        }
    }
    
   private var currentAuthorizationSession: AuthorizationSession? {
        get {
            if let data = KeychainService.loadData(for: "currentAuthorizationSessionKey0123456") as Data? {
                let session = NSKeyedUnarchiver.unarchiveObject(with: data) as? AuthorizationSession
                return session
            }
            return nil
        } set {
            let data = newValue == nil ? NSData() : NSKeyedArchiver.archivedData(withRootObject: newValue!) as NSData
            KeychainService.save(data: data, for: "currentAuthorizationSessionKey0123456")
        }
    }
    
    

    
    func updateAuthSession(with json: NSDictionary?) {
        currentAuthorizationSession = AuthorizationSession(with: json)
    }

    
    func logout() {
        Server.makeRequest.currentAuthorizationSession = nil
        UserDataManager.shared.currentUser = nil
        NotificationCenter.default.post(name: NSNotification.Name("logout"), object: nil)
        asyncAfter(milliseconds: 300) {
            UIApplication.shared.keyWindow?.rootViewController = AuthViewController.makeOne()
        }
    }

    
    func initialLoading(completion: @escaping ()->Void) {
        if Server.makeRequest.isAuthorized {
            Server.makeRequest.getNewsCategories(completion: { (result) in
                Server.makeRequest.newsCategories = result
                Server.makeRequest.loadQuestionsCount()
                Server.makeRequest.getAvailableDistricts {}
                Server.makeRequest.getRegionsList {}
                Server.makeRequest.loadMenuItems {}
                Server.makeRequest.getCurrentUser()
                Server.makeRequest.getAllPhonebookItems {}
                completion()
            })
        } else {
            Server.makeRequest.getNewsCategories(completion: { (result) in
                Server.makeRequest.newsCategories = result
                Server.makeRequest.getAvailableDistricts {}
                Server.makeRequest.getRegionsList {}
                Server.makeRequest.loadMenuItems {}
                Server.makeRequest.getAllPhonebookItems {}
                completion()
            })
        }
    }

    

    
}
