//
//  Server+Reception.swift
//  Magadan
//
//  Created by Константин on 17.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import Alamofire

extension Server {
    
    func getReceptionRecipients(completion: @escaping ()->Void) {
        guard receptionRecipients.count == 0 else { completion(); return }
        
        Alamofire.request(url_paths.base + url_paths.receptionRecipients, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let json = response.result.value as? [NSDictionary] {
                for dict in json {
                    self.receptionRecipients.append(Recipient(with: dict))
                }
            }
            completion()
        }
    }
    
    /// returns id of created request, id == nil when creation failed
    func leaveReceptionRequest(_ req: ReceptionRequest, theme: Theme, completion: @escaping (String?)->Void) {
        var fields = req.custom_fields
        fields["theme"] = theme.id
        
        Alamofire.request(url_paths.base + url_paths.receptionRequest, method: .post, parameters: fields, encoding: JSONEncoding.default, headers: self.header).responseJSON { (response) in
            completion((response.result.value as? NSDictionary)?["id"] as? String)
        }
    }
    
    
    func getMyTreatments(completion: @escaping ([Something])->Void) {
        guard let sid = self.sessionID else { completion([]); return }
        var items: [Something] = []
        Alamofire.request(url_paths.base + url_paths.users + sid + "/treatments", method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let json = response.result.value as? [NSDictionary] {
                for dict in json {
                    items.append(Something(with: dict))
                }
            }
            completion(items)
        }
    }
    
}

