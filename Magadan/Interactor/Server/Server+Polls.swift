//
//  Server+Polls.swift
//  Magadan
//
//  Created by Константин on 17.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation
import Alamofire

extension Server {
    
    static var pollsLimit: Int {
        get {
            return 20
        }
    }
    func getPolls(page: Int, completion: @escaping ([Poll], Bool)->Void) {
        Alamofire.request(url_paths.base + url_paths.polls, method: .get, parameters: ["limit": Server.pollsLimit, "offset": page*Server.pollsLimit], encoding: URLEncoding.default, headers: self.header).responseJSON { (response) in
            print("get pollls request = \(response.request?.url)")
            var result: [Poll] = []
            if let items = response.result.value as? [NSDictionary] {
                for item in items {
                    result.append(Poll(with: item))
                }
            }
            completion(result, result.count == Server.pollsLimit)
        }
    }
    
    func getPoll(by id: String, completion: @escaping (Poll?)->Void) {
        Alamofire.request(url_paths.base + String(format: url_paths.pollByID, arguments: [id]), method: .get, parameters: nil, encoding: URLEncoding.default, headers: self.header).responseJSON { (response) in
            
            print("url = \(url_paths.base + String(format: url_paths.pollByID, arguments: [id]))")
            print("id = \(id) && response = \(response)")
            
            var result: Poll? = nil
            
            if let item = response.result.value as? NSDictionary {
                result = Poll(with: item)
            }
            
            completion(result)
        }
    }
    
    
    func sendPollAnswers(_ poll: Poll, completion: @escaping ()->Void) {
        var params: [String: AnyObject] = [:]
        params["session_id"] = self.sessionID as AnyObject
        var qa = [[String: String]]()
        var answers: [Answer] = []
        for q in poll.questions {
            
            if q.isText {
                if q.text != nil {
                    qa.append(["VOTE_ANSWER_ID": "0",
                               "QUESTION_ID": q.id,
                               "QUESTION_VOTE_TYPE": "text",
                               "text": q.text!])
                }
            } else {
                for _index in q.selectedAnswerIndexes {
                    let index = _index - 1
                    if index >= 0 && q.answers.count > index {
                        answers.append(q.answers[index])
                    }
                }
            }
        }
        
        answers.forEach({
            qa.append(["VOTE_ANSWER_ID": $0.id,
                       "QUESTION_ID": $0.question_id,
                       "QUESTION_VOTE_TYPE": $0.type])
            return;
        })
        
        params["QA"] = qa as AnyObject
        
        print("sendPollAnswers params = \(params)")
        
        Alamofire.request(url_paths.base + url_paths.polls + "/" + poll.id, method: .post, parameters: params, encoding: JSONEncoding.default, headers: self.header).responseJSON { (response) in
            print("sendPollAnswers response = \(response)")
            completion()
        }
    }
    
    func getMyVotes(completion: @escaping ([Poll])->Void) {
        guard let sid = self.sessionID else { completion([]); return }
        var items: [Poll] = []
        Alamofire.request(url_paths.base + url_paths.users + sid + "/votes", method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            if let json = response.result.value as? [NSDictionary] {
                for dict in json {
                    items.append(Poll(with: dict))
                }
            }
            completion(items)
        }
    }
    
}
