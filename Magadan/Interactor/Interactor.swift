//
//  Interactor.swift
//  Magadan
//
//  Created by Константин Черкашин on 19.10.17.
//  Copyright © 2016 Константин Черкашин. All rights reserved.
//

import UIKit
import Alamofire
import Foundation

class Interactor {
    static let instance = Interactor()
}

extension UIViewController {
    
}

func asyncAfter(milliseconds: Int=0, _ block: @escaping (()->Void)) {
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.milliseconds(milliseconds), execute: block)
}


extension NSAttributedString {
    func requiredHeightForTextView(_ font: UIFont, width: CGFloat) -> CGFloat {
        let textView: UITextView = UITextView(frame: CGRect(x: 0, y: 0, width: width, height: 0))
        textView.font = font
        textView.attributedText = self
        textView.isScrollEnabled = false
        textView.sizeToFit()
        return textView.contentSize.height + textView.contentInset.top + textView.contentInset.bottom
    }
}

extension String { // MARK: - Calculating preffered content sizes for String
    
    
    func requiredHeight(_ size: CGFloat, font: UIFont?, width: CGFloat, lineSpacing lnspace: CGFloat?) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = .byClipping
        
        if font == nil {
            label.font = UIFont.systemFont(ofSize: size)
        } else {
            label.font = font
        }
        label.text = self
        
        label.sizeToFit()
        
        return label.frame.height
    }
    
    func requiredHeightForTextView(_ font: UIFont, width: CGFloat) -> CGFloat {
        let textView: UITextView = UITextView(frame: CGRect(x: 0, y: 0, width: width, height: 0))
        textView.font = font
        textView.text = self
        textView.isScrollEnabled = false
        textView.sizeToFit()
        return textView.contentSize.height + textView.contentInset.top + textView.contentInset.bottom
    }
    
    
    
    func requiredWidth(_ size: CGFloat, font: UIFont?, height: CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: height))
        label.numberOfLines = 1
        
        if font == nil {
            label.font = UIFont.systemFont(ofSize: size)
        } else {
            label.font = font
        }
        label.text = self
        
        label.sizeToFit()
        
        return label.frame.width
    }
    
    
    var isValidEmail: Bool {
        get {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: self)
        }
    }
    
}


//MARK: - Date's logic -> }
extension String {
    
    
    func stringToDate(format: String="dd-MM-yyyy HH:mm:ss") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        if date != nil {
            return date!
        } else {
            return Date()
        }
    }
    
    func noSecondsStringToDate() -> Date {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        return df.date(from: self) ?? Date()
    }
    
    
    func htmlToString() -> String {
        guard let data = self.data(using: .unicode, allowLossyConversion: true) else { return self }
        let attrStr = try? NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue, NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        return attrStr?.string ?? self
    }
    
    
    func htmlToModifiedString() -> String {
        var finalHTMLString = self
        var stringWithImages = self

        while let srcStart = stringWithImages.range(of: "src=\"")?.upperBound {
            stringWithImages = stringWithImages.substring(from: srcStart)
            guard let srcEnd = stringWithImages.range(of: "\"")?.lowerBound else { break; }
            let source = stringWithImages.substring(to: srcEnd)

            if let souceRange = self.range(of: source) {
                let originalRange = self.lineRange(for: souceRange)
                let absoluteSource = "https://www.49gov.ru" + source
                finalHTMLString = self.replacingCharacters(in: originalRange, with: "<a><img alt src=\"\(absoluteSource)\" style='width: \(UIScreen.main.bounds.width*0.9); object-fit: scale-down'></img></a>")
            }
        }
        return finalHTMLString
    }
    
     func htmlToAttributedString() -> NSAttributedString {
        
        let modified = self.htmlToModifiedString()
        let modifiedFont = NSString(format:"<span style=\"font-family: 'Roboto-Regular'; color: #20221F; font-size: 16px; line-height: 24px;\">%@</span>" as NSString, modified) as String
        guard let data = modifiedFont.data(using: .unicode, allowLossyConversion: true) else { return NSAttributedString(string: modified); }
        let attrStr = try? NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue, NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)

        return attrStr ?? NSAttributedString(string: self)
    }
    
}

extension Int {
    var monthName: String {
        get {
            switch self {
            case 1: return "Январь"
            case 2: return "Февраль"
            case 3: return "Март"
            case 4: return "Апрель"
            case 5: return "Май"
            case 6: return "Июнь"
            case 7: return "Июль"
            case 8: return "Август"
            case 9: return "Сентябрь"
            case 10: return "Октябрь"
            case 11: return "Ноябрь"
            case 12: return "Декабрь"
            case 13: return "Все"
            default: return ""
            }
        }
    }
}



extension Date {
    
    init(dateFromVeryStrangeString veryStrangeString: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        self = dateFormatter.date(from: veryStrangeString) ?? Date()
    }
    
    func dateToComponents() -> (year: Int, month: Int, day: Int, weekDay: String, monthName: String) {
        let calendar = Calendar.current
        let comps = (calendar as NSCalendar).components([.year, .month, .day, .weekday], from: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EE"
        let dayString = dateFormatter.string(from: self).capitalized(with: Locale(identifier: "ru_RU"))
        dateFormatter.dateFormat = "MMMM"
        
        let monthString = dateFormatter.string(from: self).capitalized(with: Locale(identifier: "ru_RU"))
        return (year: comps.year!, month: comps.month!, day: comps.day!, weekDay: dayString, monthName: monthString)
    }
    
    
    func hour() -> Int {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.init(secondsFromGMT: 11*3600)!
        let comps = (calendar as NSCalendar).components([.hour], from: self)
        return comps.hour ?? 0
    }
    
    func currentHour() -> Int {
        var calendar = Calendar.current
        let comps = (calendar as NSCalendar).components([.hour], from: self)
        return comps.hour ?? 0
    }
    
    func minute() -> Int {
        var calendar = Calendar.current
        let comps = (calendar as NSCalendar).components([.minute], from: self)
        return comps.minute ?? 0
    }
        
    
    func dateToMonth() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        return dateFormatter.string(from: self).capitalized(with: Locale(identifier: "ru_RU"))
        
        
    }
    
    func parseTime() -> String {
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([.hour, .minute], from: self)
        var hour = String(describing: components.hour!)
        if hour.characters.count == 1 {
            hour = "0" + hour
        }
        var minute = String(describing: components.minute!)
        if minute.characters.count == 1 {
            minute = "0" + minute
        }
        return hour + ":" + minute
    }
    
    func parseDate() -> String {
        let dateFormatter = DateFormatter()
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([.month, .day, .weekday], from: self)
        let day = components.day!
        dateFormatter.dateFormat = "EE"
        let dayString = dateFormatter.string(from: self).capitalized(with: Locale(identifier: "ru_RU"))
        return String(describing: day) + " " + dayString.lowercased()
    }
    
    func parseDateShort(dots: Bool = true) -> String {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.month, .day, .year], from: self)
        let day = components.day! < 10 ? "0\(components.day!)" : "\(components.day!)"
        let month = components.month! < 10 ? "0\(components.month!)" : "\(components.month!)"
        let year = String(components.year!)
        return [day, month, year].joined(separator: dots ? "." : "-")
        
    }
    
    
    var dateToZero: Date {
        return Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: self)!
    }
    
    var dateToZeroPlus3: Date {
        return Calendar.current.date(bySettingHour: 3, minute: 0, second: 0, of: self)!
    }
    
    func dateToString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return dateFormatter.string(from: self)
    }
    
    func dateToNoSecondsString() -> String {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        return df.string(from: self)
    }
    
    func dateToInt() -> Int {
        return Int(self.timeIntervalSince1970 * 1000)
    }
    
    func scrollDatetoSevenOClock() -> Date {
        let newDate = self
        let calendar = Calendar.current
        var components = (calendar as NSCalendar).components([.hour, .minute], from: newDate)
        components.hour = 7
        components.minute = 0
        components.second = 0
        
        return newDate
    }
    
    func startOfWeek() -> Date? {
        var cal = Calendar.current
        var comp: DateComponents = cal.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)
        comp.to12pm()
        cal.firstWeekday = 2
        return cal.date(from: comp) == nil ? nil : cal.date(from: comp)!
    }
    
    
    func endOfWeek() -> Date? {
        let cal: Calendar = Calendar.current
        var comp: DateComponents = (cal as NSCalendar).components([ .weekOfYear, .day], from: self)
        comp.weekOfYear = 1
        comp.day = -1
        if let date = (cal as NSCalendar).date(byAdding: comp, to: self.startOfWeek()!, options: []) {
            return date
        } else {
            return nil
        }
    }
    
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    
    func nextWeek() -> Date? {
        let cal = Calendar.current
        var comp: DateComponents = (cal as NSCalendar).components([.weekOfYear], from: self)
        comp.weekOfYear = 1
        if let date = (cal as NSCalendar).date(byAdding: comp, to: self.startOfWeek()!, options: []) {
            return date.dateToStartOfDay()
        } else {
            return nil
        }
    }
    func previousWeek() -> Date? {
        let cal = Calendar.current
        var comp: DateComponents = (cal as NSCalendar).components([.weekOfYear], from: self)
        comp.weekOfYear = -1
        if let date = (cal as NSCalendar).date(byAdding: comp, to: self.startOfWeek()!, options: []) {
            return date.dateToStartOfDay()
        } else {
            return nil
        }
    }
    
    func isInToday() -> Bool {
        return Calendar.current.isDateInToday(self)
    }
    
    func isInTomorrow() -> Bool {
        return Calendar.current.isDateInTomorrow(self)
        
    }
    
    func isInThisWeek() -> Bool {
        if let end = Date().endOfWeek()?.dateToEndOfDay() {
            return self > Date().dateToEndOfDay() && !self.isInTomorrow() && self < end
        }
        return false
    }
    
    func isInNextWeek() -> Bool {
        if let plusWeek = Date().nextWeek() {
            
            let startOfNextWeek = plusWeek.startOfWeek()
            let endOfNextWeek = plusWeek.endOfWeek()
            if startOfNextWeek != nil && endOfNextWeek != nil {
                return self > startOfNextWeek! && self < endOfNextWeek!
            }
        }
        return false
        
    }
    
    func isLateThanNextWeek() -> Bool {
        if let plusWeek = Date().nextWeek() {
            if let endOfNextWeek = plusWeek.endOfWeek() {
                return self > endOfNextWeek
            }
        }
        return false
    }
    
    
    
    func setMonthFromDate(_ date: Date) -> Date {
        let cal = Calendar.current
        var comps1 = (cal as NSCalendar).components([.month], from: self)
        let comps2 = (cal as NSCalendar).components([.month], from: date)
        comps1.month = comps2.month
        if let resultDate = cal.date(from: comps1) {
            return resultDate
        } else {
            return self
        }
    }
    func setDayFromDate(_ date: Date) -> Date {
        let cal = Calendar.current
        var comps1 = (cal as NSCalendar).components([.day], from: self)
        let comps2 = (cal as NSCalendar).components([.day], from: date)
        comps1.day = comps2.day
        if let resultDate = cal.date(from: comps1) {
            return resultDate
        } else {
            return self
        }
    }
    func setYearFromDate(_ date: Date) -> Date {
        let cal = Calendar.current
        var comps1 = (cal as NSCalendar).components([.year], from: self)
        let comps2 = (cal as NSCalendar).components([.year], from: date)
        comps1.year = comps2.year
        if let resultDate = cal.date(from: comps1) {
            return resultDate
        } else {
            return self
        }
    }
    
    func setTimeFromDate(_ date: Date) -> Date {
        let comps = (Calendar.current as NSCalendar).components([.hour, .minute, .second], from: date)
        if let resultDate = Calendar.current.date(bySettingHour: comps.hour!, minute: comps.minute!, second: comps.second!, of: self) {
            return resultDate
        } else {
            return self
        }
        
    }
    
    static func >(fdate: Date, sdate: Date) -> Bool {
        if fdate.compare(sdate) == .orderedAscending || fdate.compare(sdate) == .orderedSame {
            return false
        } else{
            return true
        }
    }
    static func <(fdate: Date, sdate: Date) -> Bool {
        if sdate.compare(fdate) == .orderedAscending || sdate.compare(fdate) == .orderedSame {
            return false
        } else{
            return true
        }
    }
    
    func dateToStartOfDay() -> Date {
        let cal = Calendar.current
        return (cal as NSCalendar).date(bySettingHour: 0, minute: 0, second: 0, of: self, options: [])!
    }
    
    func dateToEndOfDay() -> Date {
        let cal = Calendar.current
        let zeroDay = self.dateToStartOfDay()
        return cal.date(byAdding: .day, value: 1, to: zeroDay)!
    }
}


internal extension DateComponents {
    mutating func to12pm() {
        self.hour = 12
        self.minute = 0
        self.second = 0
    }
}


extension Data {
    func mimeType() -> String {
        var c: __uint8_t = 0
        self.copyBytes(to: &c, count: 1)
        switch c {
        case 0xFF:
            return "image/jpeg"
        case 0x89:
            return "image/png"
        case 0x47:
            return "image/gif"
        case 0x4D:
            return "image/tiff"
        case 0x25:
            return "application/pdf"
        case 0xD0:
            return "application/vnd"
        case 0x46:
            return "text/plain"
        default:
            return "application/octet-stream"
        }
    }
}

extension Int {
    func byteSize() -> String {
        if self < 1000 {
            return "\(self) Б"
        } else if self < 1000000 {
            return "\((Double(self)/1000.0).roundTo(x: 2)) КБ"
        } else if self < 1000000000 {
            return "\((Double(self)/1000000.0).roundTo(x: 2)) МБ"
        } else {
            return "\((Double(self)/1000000000.0).roundTo(x: 2)) ГБ"
        }
    }
}

extension Double {
    func roundTo(x:Int) -> Double {
        let divisor = pow(10.0, Double(x))
        return (self * divisor).rounded() / divisor
    }
}

extension UIImage {
    func imageWithColor(_ tintColor: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        
        let context = UIGraphicsGetCurrentContext()! as CGContext
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height) as CGRect
        context.clip(to: rect, mask: self.cgImage!)
        tintColor.setFill()
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()! as UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
}



//extension UIImage {
//    func imgToBase64() -> String {
//        let imageData = UIImageJPEGRepresentation(self , 0.2)
//        let imageDataBase64 = imageData!.base64EncodedString(options: [])
//        return imageDataBase64
//    }
//
//}


