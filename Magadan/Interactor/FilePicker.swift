//
//  FilePicker.swift
//  Magadan
//
//  Created by Константин on 18.01.2018.
//  Copyright © 2018 Константин. All rights reserved.
//

import UIKit
import MobileCoreServices

class FilePicker: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    fileprivate var picker: UIImagePickerController!
    fileprivate var onImagePicked: ((UIImage?)->Void)?
    fileprivate var onFilePicked: ((URL?)->Void)?
    
    func showPicker(source: UIView, from: UIViewController, onImagePicked imageHandler: @escaping ((UIImage?)->Void), onFilePicked fileHandler: @escaping ((URL?)->Void)) {
        onImagePicked = imageHandler
        
        picker = UIImagePickerController()
        picker.delegate = self
        
        let alert = UIAlertController(title: "Выберите источник", message: nil, preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = source
        
        alert.addAction(UIAlertAction(title: "Камера", style: .default, handler: { (_) in
            self.picker.sourceType = .camera
            from.present(self.picker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Альбомы", style: .default, handler: { (_) in
            self.picker.sourceType = .photoLibrary
            from.present(self.picker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Документы", style: .default, handler: { (_) in
            let documentPicker = DocumentPicker.initWithAnyType()
            documentPicker.onPickedDocument(completion: { (documentURL) in
                fileHandler(documentURL)
            })
            from.present(documentPicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        from.present(alert, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let img = info[UIImagePickerControllerOriginalImage] as? UIImage {
            onImagePicked?(img)
        }
        picker.dismiss(animated: true, completion: nil)
    }
}



class DocumentPicker: UIDocumentPickerViewController, UIDocumentPickerDelegate {
    
    class func initWithAnyType() -> DocumentPicker {
        let new = DocumentPicker(documentTypes: [
            kUTTypePDF,
            kUTTypeJPEG,
            kUTTypeRTF,
            kUTTypeRTFD,
            kUTTypeMP3,
            kUTTypePNG,
            kUTTypeMPEG4,
            kUTTypeMovie,
            kUTTypeAVIMovie,
            kUTTypeJPEG2000
            ].map({String($0)}) + ["org.openxmlformats.wordprocessingml.document",
                                   "com.microsoft.word.doc"], in: .import)
        new.delegate = new
        return new
    }
    
    private var completionBlock: ((URL?)->Void)?
    
    func onPickedDocument(completion: @escaping (URL?)->Void) {
        completionBlock = completion
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        sendCompletion(with: nil)
        controller.dismiss(animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        var tempDirectory: URL!
        
        if #available(iOS 10.0, *) {
            tempDirectory = FileManager.default.temporaryDirectory
        } else {
            tempDirectory = URL(fileURLWithPath: NSTemporaryDirectory())
        }
        
        var tempURL = tempDirectory.appendingPathComponent(url.lastPathComponent)
        var index = 0
        while (try? FileManager.default.contentsOfDirectory(atPath: tempDirectory.path).contains(tempURL.lastPathComponent)) == true {
            index += 1
            tempURL = tempURL.deletingLastPathComponent().appendingPathComponent(url.deletingPathExtension().lastPathComponent + "\(index)").appendingPathExtension(url.pathExtension)
        }
        try? FileManager.default.copyItem(at: url, to: tempURL)
        sendCompletion(with: tempURL)
        controller.dismiss(animated: true, completion: nil)
        
    }
    
    private func sendCompletion(with _url: URL?) {
        
        guard let url = _url else { completionBlock?(nil); return }
        
        if isFileSizeValid(path: url.path) {
            completionBlock?(url)
        } else {
            showSizeAlert()
        }
    }
    
    private func isFileSizeValid(path: String) -> Bool {
        let size = FileManager.default.contents(atPath: path)?.count ?? 0
        return size < 10*1024*1024
    }
    
    private func showSizeAlert() {
        let alert = UIAlertController(title: "Ошибка", message: "Максимальный размер файла - 10Мб", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
}
