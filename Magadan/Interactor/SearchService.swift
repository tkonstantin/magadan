//
//  SearchService.swift
//  Magadan
//
//  Created by Константин on 07.12.2017.
//  Copyright © 2017 Константин. All rights reserved.
//
import Foundation

class SearchService: NSObject {
    
    fileprivate var currentType: SearchItem.types = .undefined
    
    func changeType(to newtype: SearchItem.types) {
        currentType = newtype
        textChanged(currentSearchText, needsForceUpdate: true)
    }
    
    
    let requestsIntervalSeconds: TimeInterval = 2
    let typingDelay: TimeInterval = 0.7
    
    public func textChanged(_ newText: String, needsForceUpdate: Bool=false) {
        lastSymbolTyped = Date().timeIntervalSince1970
        if currentSearchText == newText && needsForceUpdate {
            currentSearchText = newText
            tryToRequest()
        } else {
            currentSearchText = newText
        }
    }
    
    public func subscribeForResults(completion: @escaping (([SearchItem])->Void)) {
        resultsSubscription = completion
    }
    
    private var resultsSubscription:(([SearchItem])->Void)?
    
    
    var currentSearchText = "" {
        didSet {
            if currentSearchText != oldValue {
                tryToRequest()
            }
        }
    }
    
    private var lastRequestTimestamp: TimeInterval = 0
    private var lastSymbolTyped: TimeInterval = 0
    
    
    @objc private func tryToRequest() {
        if Date().timeIntervalSince1970 - lastRequestTimestamp > requestsIntervalSeconds && Date().timeIntervalSince1970 - lastSymbolTyped > typingDelay {
            NSObject.cancelPreviousPerformRequests(withTarget: self)
            request()
        } else {
            self.perform(#selector(self.tryToRequest), with: nil, afterDelay: requestsIntervalSeconds/2)
        }
    }
    
    private func request() {
        guard currentSearchText.count != 0 else { self.resultsSubscription?([]); return }
        
        self.lastRequestTimestamp = Date().timeIntervalSince1970
        if currentType == .undefined {
            Server.makeRequest.search_all(query: currentSearchText) { (items) in
                self.resultsSubscription?(items)
            }
        } else {
            Server.makeRequest.searchBy(type: self.currentType, query: currentSearchText, pageNumber: -1, completion: { (items) in
                self.resultsSubscription?(items)
            })
        }
        
        
    }
    
    class var searchHistory: [SearchItem] {
        get {
            if let data = UserDefaults.standard.value(forKey: "searchHistory_01") as? Data {
                return NSKeyedUnarchiver.unarchiveObject(with: data) as? [SearchItem] ?? []
            }
            return []
        } set {
            var valuesToWrite = newValue
            while valuesToWrite.count > 5 {
                valuesToWrite.removeFirst()
            }
            let data = NSKeyedArchiver.archivedData(withRootObject: valuesToWrite)
            UserDefaults.standard.setValue(data, forKey: "searchHistory_01")
            _ = UserDefaults.standard.synchronize()
        }
    }
}

