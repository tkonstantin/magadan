//
//  PhoneBookParser.swift
//  Magadan
//
//  Created by Константин on 01.11.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import Foundation


class PhoneBookParser {
    static let instance = PhoneBookParser()
    
    var previousDownloadDate: Date {
        get {
            if let date = UserDefaults.standard.value(forKey: "previousPhonebookDownloadDate") as? Date {
                return date
            }
            return Date(timeIntervalSince1970: 1)
        } set {
            UserDefaults.standard.set(newValue, forKey: "previousPhonebookDownloadDate")
            _ = UserDefaults.standard.synchronize()
        }
    }
    
    private var _allCategories: [Category] = []
    var allCategories: [Category] {
        get {
            if _allCategories.count == 0 {
                if let data = UserDefaults.standard.value(forKey: "allcategoriesUDSaved") as? Data {
                    _allCategories = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Category] ?? []
                }
            }
            return _allCategories
        } set {
            _allCategories = newValue
            UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: newValue), forKey: "allcategoriesUDSaved")
            _ = UserDefaults.standard.synchronize()
        }
    }
    
    func findItem(with id: String) -> Any? {
        var founded: Any?
        for category in self.allCategories {
            if let cat = findItem(with: id, in: category) {
                founded = cat
            }
        }
        return founded
    }
    
    private func findItem(with id: String, in category: Category) -> Any? {
        var founded: Any?
        guard category.id != id else { return (category, false) }
        for subcategory in category.categories {
            if let cat = findItem(with: id, in: subcategory) {
                founded = cat
                return founded
            }
        }
        
        for person in (category.head_persons + category.stuff_persons) {
            if person.id == id {
                founded = (person)
                return founded
            }
        }
        
        return founded
    }
    
    func update(with o_json: NSDictionary?) {
        var newCats: [Category] = []
        guard let json = o_json else { return }
        for key in json.allKeys {
            if let cat = Category(with: json[key] as? NSDictionary) {
                newCats.append(cat)
            }
        }
        allCategories = newCats
    }
    
    
}

class Category: NSObject, NSCoding {
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.categories, forKey: "categories")
        aCoder.encode(self.phones, forKey: "phones")
        aCoder.encode(self.persons, forKey: "persons")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? String ?? ""
        self.title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
        self.categories = aDecoder.decodeObject(forKey: "categories") as? [Category] ?? []
        self.phones = aDecoder.decodeObject(forKey: "phones") as? [Phone] ?? []
        self.persons = aDecoder.decodeObject(forKey: "persons") as? [String: [Person]] ?? [:]
    }
    
    var id = ""
    var title = ""
    var categories: [Category] = []
    var phones: [Phone] = []
    var persons: [String: [Person]] = [:]
    var parent_id = ""
    var address = ""
    
    var head_persons: [Person] {
        get {
            return persons["1"] ?? []
        }
    }
    
    var stuff_persons: [Person] {
        get {
            return persons["0"] ?? persons["2"] ?? persons["3"] ?? []
        }
    }
    
    init?(with o_json: NSDictionary?) {
        guard let json = o_json else { return nil }
        
        self.id = json["id"] as? String ?? ""
        self.title = json["title"] as? String ?? ""
        self.address = json["address"] as? String ?? ""
        
        if let data = json["data"] as? NSDictionary {
            
            if let cats = data["categories"] as? [NSDictionary] {
                for cat in cats {
                    let subCategory = Category(with: cat)!
                    subCategory.parent_id = self.id
                    self.categories.append(subCategory)
                }
            }
            
            if let info = data["info"] as? NSDictionary {
                if let phoneDicts = info["phones"] as? [NSDictionary] {
                    for phoneDict in phoneDicts {
                        phones.append(Phone(with: phoneDict))
                    }
                }
                if let personsDict = info["persons"] as? NSDictionary {
                    for personKey in personsDict.allKeys {
                        guard let personKeyString = personKey as? String else { return }
                        if let personsDictArr = personsDict[personKeyString] as? [NSDictionary] {
                            for pd in personsDictArr {
                                let person = Person(with: pd)
                                if let _ = persons[personKeyString] {
                                    persons[personKeyString]?.append(person)
                                } else {
                                    persons[personKeyString] = [person]
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
}


class Phone: Person {
    
}

class Person: NSObject, NSCoding  {
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: "id")
        aCoder.encode(self.fio, forKey: "fio")
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.descript, forKey: "descript")
        aCoder.encode(self.schedule, forKey: "schedule")
        aCoder.encode(self.email, forKey: "email")
        aCoder.encode(self.fax, forKey: "fax")
        aCoder.encode(self.phone, forKey: "phone")
        aCoder.encode(self.url, forKey: "url")
        aCoder.encode(self.ogv, forKey: "ogv")
        aCoder.encode(self.contact_type, forKey: "contact_type")
        aCoder.encode(self.head, forKey: "head")
        aCoder.encode(self.parent, forKey: "parent")
        aCoder.encode(self.news_exist, forKey: "news_exist")
        aCoder.encode(self.podrazdel, forKey: "podrazdel")
        aCoder.encode(self.post, forKey: "post")
        aCoder.encode(self.phone2, forKey: "phone2")
        aCoder.encode(self.email2, forKey: "email2")
        aCoder.encode(self.address, forKey: "address")


    }
    
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? String ?? ""
        self.fio = aDecoder.decodeObject(forKey: "fio") as? String ?? ""
        self.title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
        self.descript = aDecoder.decodeObject(forKey: "descript") as? String ?? ""
        self.schedule = aDecoder.decodeObject(forKey: "schedule") as? String ?? ""
        self.email = aDecoder.decodeObject(forKey: "email") as? String ?? ""
        self.fax = aDecoder.decodeObject(forKey: "fax") as? String ?? ""
        self.phone = aDecoder.decodeObject(forKey: "phone") as? String ?? ""
        self.url = aDecoder.decodeObject(forKey: "url") as? String ?? ""
        self.ogv = aDecoder.decodeObject(forKey: "ogv") as? String ?? ""
        self.contact_type = aDecoder.decodeObject(forKey: "contact_type") as? String ?? ""
        self.head = aDecoder.decodeObject(forKey: "head") as? String ?? ""
        self.parent = aDecoder.decodeObject(forKey: "parent") as? String ?? ""
        self.news_exist = aDecoder.decodeObject(forKey: "news_exist") as? String ?? ""
        self.podrazdel = aDecoder.decodeObject(forKey: "podrazdel") as? String ?? ""
        self.post = aDecoder.decodeObject(forKey: "post") as? String ?? ""
        self.phone2 = aDecoder.decodeObject(forKey: "phone2") as? String ?? ""
        self.email2 = aDecoder.decodeObject(forKey: "email2") as? String ?? ""
        self.address = aDecoder.decodeObject(forKey: "address") as? String ?? ""


    }
    
    var id = ""
    var fio = ""
    var title = ""
    var descript = ""
    var schedule = ""
    var email = ""
    var fax = ""
    var phone = ""
    var url = ""
    var ogv = ""
    var contact_type = ""
    var head = ""
    var parent = ""
    var news_exist = ""
    var podrazdel = ""
    var post = ""
    var phone2 = ""
    var email2 = ""
    var address = ""
    
    init(with json: NSDictionary) {
        self.id = json["id"] as? String ?? ""
        self.fio = json["fio"] as? String ?? ""
        self.title = json["title"] as? String ?? ""
        self.descript = json["description"] as? String ?? ""
        self.schedule = json["schedule"] as? String ?? ""
        self.email = json["email"] as? String ?? ""
        self.fax = json["fax"] as? String ?? ""
        self.phone = json["phone"] as? String ?? ""
        self.url = json["url"] as? String ?? ""
        self.ogv = json["ogv"] as? String ?? ""
        self.contact_type = json["contact_type"] as? String ?? ""
        self.head = json["head"] as? String ?? ""
        self.parent = json["parent"] as? String ?? ""
        self.news_exist = json["news_exist"] as? String ?? ""
        self.podrazdel = json["podrazdel"] as? String ?? ""
        self.post = json["post"] as? String ?? ""
        self.phone2 = json["phone2"] as? String ?? ""
        self.email2 = json["email2"] as? String ?? ""
        self.address = json["address"] as? String ?? ""
        
        
    }
    
}
