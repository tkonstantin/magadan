//
//  AppDelegate.swift
//  Magadan
//
//  Created by Константин on 17.10.2017.
//  Copyright © 2017 Константин. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import Firebase
import UserNotifications
import SVProgressHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var waitingShortCutAction: (()->Void)?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        Fabric.with([Crashlytics.self])
        FirebaseApp.configure()
        NetworkManager.shared.startMonitoringNetwork()
        addShourcuts(for: application)
        prepareNotifications(for: application)
        
        Server.makeRequest.loadCityImages()
        if let notifData = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable: Any] {
            self.handleNotificationData(data: notifData)
        }
        
        if #available(iOS 9.0, *) {
            if let shortcutItem = launchOptions?[UIApplicationLaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
                shortCuts(shortcutItem.type)?.action()
                return false
            }
        }
        
        return true
    }
    
    func prepareNotifications(for app: UIApplication) {
        app.applicationIconBadgeNumber = 0
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: { _, _ in
                
            })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            app.registerUserNotificationSettings(settings)
        }
        
        Messaging.messaging().delegate = self
        
        app.registerForRemoteNotifications()
        
        if let token = Messaging.messaging().fcmToken, !token.isEmpty {
            Server.makeRequest.sendPushToken(token: token)
        }
    }
    
}


extension AppDelegate: UNUserNotificationCenterDelegate, MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        Server.makeRequest.sendPushToken(token: fcmToken)
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        handleNotificationData(data: remoteMessage.appData)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        handleNotificationData(data: userInfo)
    }
    
    enum notificationContenTypes: String {
        case news = "news"
        case events = "events"
        case live = "live"
        case vote = "vote"
        
        
    }
    fileprivate func handleNotificationData(data: [AnyHashable: Any]) {
        guard UIApplication.shared.applicationState != .active else { return }
        
        if let raw_type = data["contentType"] as? String, let type = notificationContenTypes(rawValue: raw_type) {
            let id = data["id"] as? String ?? String(data["id"] as? Int ?? 0)
            let link = URL(string: data["ios"] as? String ?? "")
            
            if id != "0" || link != nil  {
                
                let currentAction: (()->Void) = {
                    switch type {
                    case .news: return {
                        Server.makeRequest.getNewsMore(nil, id: id, completion: { (news) in
                            asyncAfter {
                                root?.mainNav?.viewControllers = [MainViewController.makeOne()]
                                root?.mainNav?.pushViewController(NewsViewController.makeOne(news: news, openedFromSearch: false), animated: true)
                                SVProgressHUD.dismiss()
                            }
                        })
                    }

                    case .events: return {
                        Server.makeRequest.getEventMore(nil, id: id, completion: { (event) in
                            asyncAfter {
                                root?.mainNav?.viewControllers = [EventsViewController.makeOne()]
                                root?.mainNav?.pushViewController(NewsViewController.makeOne(event: event, openedFromSearch: false), animated: true)
                                SVProgressHUD.dismiss()
                            }
                        })
                    }
                        
                    case .live: return {
                        root?.mainNav?.viewControllers = [EventsViewController.makeOne()]
                        if let browser = BrowserViewController.makeOne(title: "Прямая трансляция", with: link?.absoluteString ?? "", shareLink: data["share_link"] as? String ?? "https://www.49gov.ru/press/broadcast/?id=\(id)") {
                            root?.mainNav?.pushViewController(browser, animated: true)
                        }
                        SVProgressHUD.dismiss()
                    }
                        
                    case .vote: return {
                        Server.makeRequest.getPoll(by: id, completion: { (poll) in
                            SVProgressHUD.dismiss()

                            if poll != nil {
                                let poll = PollViewController.makeOne(with: poll!)
                                root?.mainNav?.viewControllers = [PollsListViewController.makeOne(), poll]
                            } else {
                                root?.mainNav?.viewControllers = [PollsListViewController.makeOne()]
                            }
                        })
                        }
                    }
                }()
                
                if root?.mainNav == nil {
                    (UIApplication.shared.delegate as? AppDelegate)?.waitingShortCutAction = currentAction
                } else {
                    SVProgressHUD.show()
                    currentAction()
                }
            }
        }
    }
    
    
    
}

extension UIApplication {
    class func openSettings() {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }
    }
    
    class var isIpad: Bool {
        get {
            return UIDevice.current.userInterfaceIdiom == .pad
        }
    }
    
}



